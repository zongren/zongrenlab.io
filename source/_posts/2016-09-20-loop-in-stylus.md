---
title: 在stylus中使用循环语句
date: 2016-09-20 16:17:41
category: miscellaneous
tags: [stylus,css]
---

在优化nojs主题的标签页时，使用到了`stylus`的循环功能，记录一下需要注意的几点，首先看以下代码
```
min-font-size=12px
font-size-step=1
min-color=lighten(primary-color,60%)
color-step=50%
.tags
  background-color #fff
  padding-top content-padding
  padding-left content-padding 
  padding-bottom content-padding 
  padding-right content-padding
  span
    display block
  
  for $num in (1..10)
    a.level-{$num}
      color darken(min-color,color-step * $num)
      font-size min-font-size + font-size-step * $num

  a
    margin-top 10px
    margin-right 10px
    display inline-block
    &:hover
      color #fff
```

* 变量的前面最好加上`$`符号，例如`$num`，用来跟正常的字符串区分开
* 想要拼接变量跟字符串，就必须使用大括号`{}`，例如`a.level-{$num}`
* 循环的范围可以用两个小数点`..`表示，如`(1..10)`就是从1到10，并且包括1和10