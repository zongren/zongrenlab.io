---
title: 博客移到gitlab
date: 2016-07-15 15:57:55
category: miscellaneous
tags: [web,blog]
excerpt: 本文介绍了为什么把博客从github pages和coding pages迁到gitlab pages上。
---
今天将博客内容移到`gitlab pages`服务器上，主要考虑到以下几点
* gitlab还没有屏蔽百度爬虫。
* gitlab支持服务器端运行`Hexo`，再也不用本地生成然后部署了，在多个电脑上撰写博客更方便。
* gitlab支持`Let’s Encrypt`证书，添加`https`支持非常方便。