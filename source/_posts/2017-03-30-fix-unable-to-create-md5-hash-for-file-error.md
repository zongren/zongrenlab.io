---
title: 解决“Failed to create MD5 hash for file ”的问题
category: miscellaneous
tags: [build,gradle]
date: 2017-03-30 14:41:03
excerpt: 解决“Failed to create MD5 hash for file ”的问题
---
在升级一个老项目的gradle版本后，打包出现以下错误
```
Error:Execution failed for task ':app:compileDebugJavaWithJavac'.
> Failed to create MD5 hash for file 'C:\Users\zongren\AndroidStudioProjects\android\app\libs\commons-cli-1.0.jar'.
```

检查发现app module下的build.gradle文件编译了不存在的文件：`commons-cli-1.0.jar`

```
dependencies {
    compile files('libs/commons-cli-1.0.jar')
    ...
}
```
将其移除后，成功解决问题，另外一般按照以下方式，将所有`libs`目录下的`jar`文件加入到项目中

```
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    ...
}
```