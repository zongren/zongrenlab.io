---
title: 编辑OSX系统的hosts文件
date: 2016-05-31 23:33:39
category: miscellaneous
tags: [osx,hosts]
---

在项目开发过程中，使用`hosts`文件配置代替直接使用ip的方式，下面简单介绍一下如何更改OSX下的hosts文件。首先打开终端`terminal`，输入以下代码
```
sudo vim /private/etc/hosts
```
这个操作需要输入管理员账号的密码。按回车打开hosts文件后就可以更改了。更改操作之后，有可能需要刷新一下DNS缓存，请输入以下代码：
```
//10.9之前的系统
sudo killall -HUP mDNSResponder
//10.9及之后的系统
dscacheutil -flushcache; sudo killall -HUP mDNSResponder
```