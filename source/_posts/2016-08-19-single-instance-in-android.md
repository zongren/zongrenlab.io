---
title: Android单例模式
date: 2016-08-19 14:40:17
category: android
tags: [singleton]
---
Android下的单例模式非常简单，代码如下
```
public class Singleton {
    private static Singleton ourInstance = new Singleton();

    public static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton() {
    }
}
```
如果想采用懒加载的方式，请使用以下代码
```
public class Singleton {
    private static Singleton ourInstance = null;

    public static Singleton getInstance() {
        if(ourInstance == null){
            ourInstance = new Singleton();
        }
        return ourInstance;
    }

    private Singleton() {
    }
}
```
将默认构造函数设置为私有方法可以避免创建多个实例。