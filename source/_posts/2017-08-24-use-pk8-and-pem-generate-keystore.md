---
title: 使用`.pk8`和`.pem`文件生成`.keystore`文件
category: android
tags: [android-studio]
excerpt: 使用`.pk8`和`.pem`文件生成`.keystore`文件，从而方便`Android Studio`使用。
date: 2017-08-24 14:34:19
---

本文介绍了如何在`Linux`环境下，使用`.pk8`文件和`.pem`文件生成`.keystore`文件，从而方便`Android Studio`使用

首先确认安装了`JRE`（或者`OpenSDK-JRE`）
```
java -version

```
如果安装了的话，会显示以下内容
```
java version "1.7.0_151"
OpenJDK Runtime Environment (IcedTea 2.6.11) (7u151-2.6.11-0ubuntu1.14.04.1)
OpenJDK 64-Bit Server VM (build 24.151-b01, mixed mode)
```

没有安装的话使用以下命令安装默认`JRE`（或者`OpenSDK-JRE`），现在`Ubuntu`默认的是`OpenSDK-JRE`

```
sudo apt-get update
sudo apt-get install default-jre
```

如果想要安装指定`JDK`
```
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
```

指定安装`OpenSDK-JRE`
```
sudo apt-get install openjdk-7-jre
```

`clone`生成工具
```
git clone https://github.com/getfatday/keytool-importkeypair.git ~/Downloads/keytool-importkeypair
```

```
cd ~/Downloads/keytool-importkeypair
./keytool-importkeypair -k android.keystore -p android -pk8 key.pk8 -cert key.x509.pem -alias android
```
其中`-k`指定导出的文件名，`-p`指定`storePassword`和`keyPassword`，`-alias`指定`keyAlias`

配置`Android Studio`中的`Build.gradle`文件
```
android {
    signingConfigs {
        release {
            storeFile file('C:/Users/zongren/andriod.keystore')
            keyAlias "android"
            keyPassword "android"
            storePassword "android"
        }
    }

    buildTypes {
        release {
            signingConfig signingConfigs.release
        }
    }
}
```