---
title: 使用sysimg.zip文件创建Android虚拟机
category: android
tags: [virtual-device]
excerpt: 使用sysimg.zip文件创建Android虚拟机
date: 2018-05-13 23:24:00
---

首先下载```sysimg_armv7a-14_r02.zip```文件

解压缩到```C:\Users\zongren\AppData\Local\Android\Sdk\system-images\```目录下

```
C:\Users\zongren\AppData\Local\Android\Sdk\system-images\android-14\armeabi-v7a
```

执行avdmanager命令
```
avdmanager.bat create avd -n test -k "system-images;android-14;default;armeabi-v7a" -b armeabi-v7a -c 100M -d 7 -f
```