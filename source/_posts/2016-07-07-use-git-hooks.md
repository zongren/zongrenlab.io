---
title: 使用Git钩子
date: 2016-07-07 16:52:57
category: development
tags: [git]
---
Git钩子和所有其他系统提供的钩子一样，让你能在某些特定的时刻进行一些任务，如代码检查，代码规范性处理等等。使用方法也很简单，例如我想在commit之前对项目文件的顺序以及项目文件进行处理，那么就添加一个`pre-commit`的钩子，在终端输入以下代码
```
{ echo '#!/bin/sh'; echo 'export PATH=/usr/local/bin:$PATH'; echo 'synx ~/XcodeProjects/App/App.xcodeproj';  echo 'xunique ~/XcodeProjects/App/App.xcodeproj'; echo 'xunique ~/XcodeProjects/App/Pods/Pods.xcodeproj/'; } > ~/XcodeProjects/App/.git/hooks/pre-commit
```
然后添加可执行权限
```
chmod 755 ~/XcodeProjects/App/.git/hooks/pre-commit
```
更多类型的钩子以及使用方法请参考官网介绍：[Git 钩子](https://git-scm.com/book/zh/v2/%E8%87%AA%E5%AE%9A%E4%B9%89-Git-Git-%E9%92%A9%E5%AD%90)