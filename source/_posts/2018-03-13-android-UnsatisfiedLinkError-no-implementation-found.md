---
title: 解决"java.lang.UnsatisfiedLinkError No implementation found"
category: android
tags: [jni]
excerpt: 解决"java.lang.UnsatisfiedLinkError No implementation found"
date: 2018-03-13 16:49:56
---

一句话，native代码所在的包或文件（java类）名称不对
例如
```
private static native FileDescriptor open(String var0, int var1, int var2);
```
这个native代码需要在指定的包和指定的类下，才能正常工作