---
title: IEEE浮点数
tags:
  - development
  - computer-science
category: development
date: 2016-09-19 11:51:12
---

维基百科链接：[IEEE floating point](https://en.wikipedia.org/wiki/IEEE_floating_point)

IEEE浮点数运算标准（IEE 754）是一项为浮点数运算设定的技术标准，由电气和电子工程师协会（Institute of Electrical and Electronics Engineers，IEEE）在1985年提出，大多数硬件的浮点单元都遵守IEE 754标准。

这个标准定义了以下内容

* ***格式***：二进制和十进制数据的表示方式，包括有限小数（包括-0,不正规数），无限小数和NaN（非值）
* ***交换格式***：一种高效、紧凑的格式，用来交换浮点数
* ***四舍五入规则***
* ***运算规则***
* ***异常处理***：例如除以0，溢出等