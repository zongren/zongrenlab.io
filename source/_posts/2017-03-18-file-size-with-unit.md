---
title: 计算文件大小
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 计算文件大小，包括单位
date: 2017-03-18 14:10:31
---
来自[StackOverflow](http://stackoverflow.com/a/3758880)上的解决方法
```
public static String humanReadableByteCount(long bytes, boolean si) {
    int unit = si ? 1000 : 1024;
    if (bytes < unit) return bytes + " B";
    int exp = (int) (Math.log(bytes) / Math.log(unit));
    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
}
```
