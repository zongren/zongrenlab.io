---
title: 块级元素
date: 2016-09-14 14:00:03
category: miscellaneous
tags: [css,block-level,element]
---

块级元素会占用父元素的所有空间，因此产生了一`块`区域，浏览器通常会在块级元素的开始和结束的地方插入换行符，下面的例子说明块级元素的影响

```
<p>这个段落是一个块级元素，其背景已被着色以显示块级元素的的影响。</p>
```

p标签的样式如下：
```
p { background-color: #222;color:#fff; }
```
{% raw %}
<p class="highlight" style="background-color:#222;color:#fff;text-align:center;">这个段落是一个块级元素，其背景已被着色以显示块级元素的的影响。</p>
{% endraw %}

以下元素均为块级元素
* address, article`HTML5`, aside`HTML5`, blockquote, canvas`HTML5`
* dd, div, dl, fieldset, figcaption`HTML5`, figure`HTML5`, footer`HTML5`
* form, h1, h2, h3, h4, h5, h6, header`HTML5`, hgroup`HTML5`
* hr, li, main, nav, noscript, ol, output`HTML5`, p, pre
* section`HTML5`, table, tfoot, ul, video`HTML5`

请参考{% post_link inline-elements 内联元素 %}，查看两个的区别。