---
title: 内联元素
date: 2016-09-14 14:06:35
category: miscellaneous
tags: [css,block-level,element]
---

内联元素只占通过定义内联元素标签包围的空间。下面的例子演示了内联元素的影响：
```
<p>这个<span>标签</span>是一个内联元素，其背景已被着色以显示内联元素的的影响。</p>
```

span标签的样式如下：
```
span { background-color: #222;color:#fff; }
```

{% raw %}
<p>这个<span style="background-color:#222;color:#fff;">标签</span>是一个内联元素，其背景已被着色以显示内联元素的的影响。</p>
{% endraw %}
下面的所有元素均为内联元素
* b, big, i, small, tt
* abbr, acronym, cite, code, dfn, em, kbd, strong, samp, time, var
* a, bdo, br, img, map, object, q, script, span, sub, sup
* button, input, label, select, textarea

内联元素和{% post_link block-level-elements 块级元素 %}的区别如下

  | 块级元素 | 内联元素
-- | ----- | -------
内容 | 块级元素一般包含内联元素或块级元素 | 内联元素包含具体数据（data）或其它内联元素
格式 | 以新行开头，新行结尾 | 不以新行开头、结尾，能够出现在任意位置
