---
title: 使用Javascript截取中文
category: miscellaneous
tags: [javascript]
date: 2017-03-31 15:54:22
excerpt: 使用Javascript截取中文，或中英混合
---
```
var sub=function(str,n){
  var r=/[^\x00-\xff]/g;
  if(str.replace(r,"mm").length<=n){return str;}
  var m=Math.floor(n/2);
  for(var i=m;i<str.length;i++){
      if(str.substr(0,i).replace(r,"mm").length>=n){
          return str.substr(0,i)+"...";
      }
  }
  return str;
}
```
