---
title: 重复运行junit测试
category: android
tags: [junit]
excerpt: 重复运行junit测试
date: 2019-02-21 13:43:23
---

有时候测试多线程或者多个接口的实现时，会用到重复执行相同junit测试的功能，这个功能junit本身是支持的，也就是Parameterized

首先添加@RunWith
```
@RunWith(Parameterized.class)
public class PrintTaskExecutorTest {
}
```

如果只是简单重复测试，不需要自定义参数，可以按以下代码实现
```
@Parameterized.Parameters
public static Object[][] data() {
    return new Object[50][0];
}

```

所有的test会执行50次

如果需要提供特殊参数，按以下方式
```
  public HttpTest(String httpName) {
    this.httpName = httpName;
  }

  @Parameterized.Parameters
  public static String[] httpNames() {
    return new String[]{"OkHttp", "HttpUrlConnection", "HttpClient", "HttpClientApi23"};
  }

```

