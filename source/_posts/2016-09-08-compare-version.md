---
title: 比较版本号字符串
date: 2016-09-08 16:50:32
category: development
tags: [version,compare]
---

如何在iOS中比较版本号字符串呢，代码如下
```
if ([latestVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
    //You may need to update the app.
} 
```

在Android下的代码
```
if (latestVersion.compareTo(currentVersion) > 0) {
    //You may need to update the app.
}
```

