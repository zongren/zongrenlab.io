---
title: Other linker flags到底是什么
category: miscellaneous
tags: [xcode,compile]
date: 2017-01-05 00:00:48
excerpt: Other linker flags到底是什么
---
简单来说 Other linker flags 就是 ld 命令的options参数，首先看一下ld 命令是用来干什么的
```
man ld
ld(1)                     BSD General Commands Manual                    ld(1)

NAME
     ld -- linker

SYNOPSIS
     ld files...  [options] [-o outputfile]

DESCRIPTION
     The ld command combines several object files and libraries, resolves ref-
     erences, and produces an ouput file.  ld can produce a final linked image
     (executable, dylib, or bundle), or with the -r option, produce another
     object file.  If the -o option is not used, the output file produced is
     named "a.out".

...
```
粗略翻译一下就是说，ld命令把若干文件和库文件整合起来，生成单个文件。那-ObjC参数是什么意思呢
```
 -ObjC       Loads all members of static archive libraries that implement
                 an Objective-C class or category.
```
也就是加载所有包括Objective-C类或者Category的库文件