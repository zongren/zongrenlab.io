---
title: 将Activity提到最前
date: 2016-08-02 16:32:14
category: android
tags: [tip,note]
---

开发时经常遇到这种需求，返回到应用主界面（也就是将主界面提到最前方），下面说一下如何实现这种需求。首先看一下什么是`launchMode`

## 什么是`android:launchMode`
`android:launchMode`就是`activity`的启动方式，配合`FLAG_ACTIVITY_*`一起使用，可以精确更改Activity的打开方式。`android:launchMode`包括以下四种：
* standard 
* singleTop 
* singleTask 
* singleInstance
默认为`standard`。

## 四种`launchMode`的区别

启动方式 | 使用场景  |  能否打开多个实例 |   注释
--------- | -------------- | -------------------- | ------------
standard |  大部分情况下使用这个 |  可以打开多实例 | 无论什么情况下，都会创建一个新的Activity实例，并将它提到最前 
singleTop  | 同上  | 根据flag决定  | 如果目标已经在最前，不会创建新的实例，而会调用目标Activity的onNewIntent方法，其他情况下跟`standard`一样
singleTask |  极特殊情况，一般不使用 | 不会打开多实例  | 如果该Activity的实例不存在，将创建新的`task`和心的Activity实例，并把它提到最前。如果该Activity的实例已经存在，将直接把它提到最前。
singleInstance | 同上  |  不会打开多实例 | 和`singleTask`一样，但是该Activity所在的task不会存在其它Activity 的实例。

## 正确返回到主界面的办法
所以想要返回到主界面，需要将RootActivity的launcheMode设置为singleTask或者singleInstance，然而singleInstance的动画效果比较奇怪，所以最好设置为singleTask。打开方式如下
```
Intent intent = new Intent(this,RootActivity.class);
startActivity(intent);
```
打开后会调用RootActivity的`onNewIntent`方法

##  其他解决办法
除了设置launchMode外，还可以使用flag解决这个需求
`FLAG_ACTIVITY_REORDER_TO_FRONT`，即
```
Intent intent = new Intent(this,RootActivity.class);
intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
startActivity(intent);
```
这种办法也可以使RootActivity提到最前面，不过不会清除历史纪录，所以使用返回键还可以返回到上一个界面，打开后同样会调用RootActivity的`onNewIntent`方法。