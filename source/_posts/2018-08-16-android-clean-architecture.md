---
title: Clean架构学习及应用（序）
category: android
tags: [clean-architecture,architecture]
excerpt: 在企业项目中应用Clean架构
date: 2018-08-16 10:22:17
---

这是一篇系列

从以下几个角度分析Clean架构的作用


1、开发（模块化/分工）

2、测试（单元测试/集成测试）

3、维护（Bug/新需求/定制化）
