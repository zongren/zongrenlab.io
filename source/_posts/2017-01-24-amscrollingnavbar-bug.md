---
title: 使用AMScrollingNavbar时遇到的一个Bug
category: miscellaneous
tags: [ios]
date: 2017-01-24 11:43:07
excerpt: 使用AMScrollingNavbar时遇到的一个Bug
---
请看下图
{% raw %}
<div style="overflow-x:auto;overflow-y:hidden;width:100%;"><a href="/images/scrolling-view-bug.png" target="_blank"><img src="/images/scrolling-view-bug.png" alt="scrolling-view-bug" style="max-width:unset;width:500px;"/></a></div>
{% endraw %}

由于CollectionView已经超出了UIViewController.view，所以点击事件无法继续传播，导致蓝色区域无法响应点击事件。
经过测试，发现是因为最下层的UIViewController使用的是UITabBarController，蓝色区域就是UITabBar的高度，解决方法很麻烦，每次打开新的页面之后要将UITabBar的高度设置为0，关闭页面的时候再恢复高度，其中用到了Method Swizzling，代码如下

```

+(void)swizzlingClassMethod:(SEL)originalSelector toMethod:(SEL)swizzledSelector{

    Class class = object_getClass((id)self);

    Method originalMethod = class_getClassMethod(class, originalSelector);
    Method swizzledMethod = class_getClassMethod(class, swizzledSelector);
    
    BOOL didAddMethod =
    class_addMethod(class,
                    originalSelector,
                    method_getImplementation(swizzledMethod),
                    method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }

}

+(void)swizzlingInstanceMethod:(SEL)originalSelector toMethod:(SEL)swizzledSelector{
    Class class = [self class];
    
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod =
    class_addMethod(class,
                    originalSelector,
                    method_getImplementation(swizzledMethod),
                    method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}
```