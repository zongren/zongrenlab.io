---
title: 管理包含第三方aar和jar的android库
category: android
tags: [gradle,maven,nexus,library]
excerpt: 如果你创建的android库依赖第三方aar和jar，应该如何处理
date: 2018-08-15 16:52:23
---

由于目前我仍然使用就版本的maven gradle插件，所以library包含aar和jar的时候，需要进行特殊处理，不清楚最新的maven-publish插件需不需要

## aar

第一种方式是使用自定义task将aar里的资源文件复制出来，并且不能跟library本身的代码或其它aar里的代码冲突

第二种方式是将aar作为一个库单独上传到maven仓库

## jar
jar里如果包含assets文件，需要做特殊处理

第一种方法是使用gradle task将jar里的assets文件全部复制到打包目录

第二种就是人工复制jar里的assets文件，到library本身的assets文件夹里