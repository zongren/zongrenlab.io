---
title: 修复git无法提交的问题
date: 2016-10-17 10:18:25
category: miscellaneous
tags: [git,fix]
---
有时提交git的时候会提示以下错误
```
fatal: Unable to create '/Users/zongren/xcodeprojects/yishop/.git/index.lock': File exists.

Another git process seems to be running in this repository, e.g.
an editor opened by 'git commit'. Please make sure all processes
are terminated then try again. If it still fails, a git process
may have crashed in this repository earlier:
remove the file manually to continue.
```
解决办法很简单，删除`index.lock`文件即可
```
cd .git
rm index.lock
```
