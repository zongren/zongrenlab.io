---
title: 在Objective-C中使用block
date: 2016-07-29 15:46:41
category: ios
tags: [tip,note]
---

### block作为局部变量使用
```
returnType (^blockName)(parameterTypes) = ^returnType(parameters) {...};
```

### 声明一个block属性
```
@property (nonatomic, copy, nullability) returnType (^blockName)(parameterTypes);
```

### 作为方法参数（形参）
```
- (void)someMethodThatTakesABlock:(returnType (^nullability)(parameterTypes))blockName;
```

### 作为方法参数（实参）
```
[someObject someMethodThatTakesABlock:^returnType (parameters) {...}];
```

### 将block声明为变量类型
```
typedef returnType (^TypeName)(parameterTypes);
TypeName blockName = ^returnType(parameters) {...};
```