---
title: 增强nojs主题的评论功能
category: development
tags: [nojs,comment]
date: 2017-02-28 08:27:22
excerpt: 本文介绍了nojs主题的评论功能在哪些方面进行了改动。
---

nojs主题的评论功能是使用[staticman](https://github.com/eduardoboucas/staticman)实现的，一开始只能给文章添加评论，现在添加了以下几个功能：

* 回复某个评论，自动@用户，并支持跳转到该评论
* 增加编辑和删除链接，方便跳转到github对应的页面
* 首页增加最新评论功能，并支持跳转到相应的评论或文章页面

## 更新
准备使用LeanCloud做一个动态评论系统，这样就不需要重新打包部署了