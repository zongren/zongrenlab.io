---
title: 使用tcpdump在Android设备上抓包
category: android
tags: [network-analysis,captuer-packet]
excerpt: 使用tcpdump在Android设备上抓包
date: 2018-11-29 17:27:31
---
使用[tcpdump](http://www.tcpdump.org/)可以捕获所有tcp和udp网络请求，不仅适用与linux系统，在Android系统下也可以使用（需要root）
首先下载tcpdump可执行文件，并且推送到Android系统内
```
adb push tcpdump /data/local/tcpdump
```
添加可执行权限
```
chmod 777 /data/local/tcpdump
```

开始抓包
```
/data/local/tcpdump  -i any -p -s 0 -w /sdcard/a.pcap
```
将a.pcap导出，并使用相关的工具打开分析（例如Wireshark）
```
adb pull /sdcard/a.pcap
```

{% img /images/wireshark.png wireshark %}