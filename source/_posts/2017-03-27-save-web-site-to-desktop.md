---
title: 网站桌面图标
category: development
tags: [web,html,icon,desktop]
date: 2017-03-27 14:04:01
excerpt: 网站桌面图标
---

## Android
Android Chrome图标规范：[官方文档](https://developer.chrome.com/multidevice/android/installtohomescreen)

## Apple
苹果设备图标规范：[官方文档](https://developer.apple.com/library/content/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html)

## Windows
Windows设备图标规范：[官方文档](https://msdn.microsoft.com/en-us/library/dn255024.aspx?f=255&MSPPError=-2147217396)

官方制作工具：[官方地址](http://www.buildmypinnedsite.com/)

示例：
```
<meta name="application-name" content="宗仁的博客"/>
<meta name="msapplication-square70x70logo" content="/images/small.jpg"/>
<meta name="msapplication-square150x150logo" content="/images/medium.jpg"/>
<meta name="msapplication-wide310x150logo" content="/images/wide.jpg"/>
<meta name="msapplication-square310x310logo" content="/images/large.jpg"/>
<meta name="msapplication-TileColor" content="#000000"/>
<meta name="msapplication-notification" content="frequency=30;polling-uri=http://notifications.buildmypinnedsite.com/?feed=https://zongren.me/atom.xml&amp;id=1;polling-uri2=http://notifications.buildmypinnedsite.com/?feed=https://zongren.me/atom.xml&amp;id=2;polling-uri3=http://notifications.buildmypinnedsite.com/?feed=https://zongren.me/atom.xml&amp;id=3;polling-uri4=http://notifications.buildmypinnedsite.com/?feed=https://zongren.me/atom.xml&amp;id=4;polling-uri5=http://notifications.buildmypinnedsite.com/?feed=https://zongren.me/atom.xml&amp;id=5; cycle=1"/>
```