---
title: Windows10和Linux子系统之间共享文件
category: miscellaneous
tags: [windows10,linux]
excerpt: Windows10和Linux子系统之间共享文件
date: 2017-08-24 14:37:13
---

简单来说，Windows下的驱动盘已经被挂载到Linux子系统的mnt节点下，使用命令进入mnt并查看文件内容

```
cd /mnt
ls
```
{% img /images/linux-mnt.png linux-mnt-content %}
____________________________________


这里的c盘就是Windows系统下的C盘，我们可以使用cp命令进行双向复制和粘贴

{% img /images/linux-cp-cat.png linux-copy-file %}

在Linux子系统中直接修改Windows下的文件也是可以的

{% img /images/linux-edit-windows-file.png linux-edit-windows-file %}