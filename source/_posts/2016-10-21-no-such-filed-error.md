---
title: 使用其它库里的fragment或者activity导致崩溃
category: android
tags: [fix,library]
date: 2016-10-21 15:46:43
---

我现在进行的项目有一个通用页面（Activity和Fragment），把它放在了`common`里，`common`是一个`com.android.library`。在使用这个通用页面的时候，出现了以下问题
```
java.lang.NoSuchFieldError: No field ... of type I in class
```
报错代码为
```
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_region,container,false);
    mCloseImageView = (ImageView) view.findViewById(R.id.fragment_region_closeImageView);//this line crashes.
    ...
}
```
经过检查发现，在使用这个通用页面的项目里，有一个重复的布局文件`fragment_region`，将它删除后一切正常。
