---
title: 更改安卓hosts文件
date: 2016-05-24 10:47:17
category: android
tags: [hosts,tip,note]
---

如何更改Android手机或者模拟器中的hosts文件呢，首先需要获取root权限，所以推荐使用模拟器，很多模拟器都可以开启root权限，不用担心安全问题，我用的就是Genymotion模拟器，默认开启了root权限。
```
adb root
adb remount
adb pull /system/etc/hosts ~/documents/hosts
adb push ~/documents/hosts /system/etc/hosts
```