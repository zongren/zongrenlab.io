---
title: Computer Networking读书笔记（3）
category: reading
tags:
  - book
  - network
  - computer
date: 2016-10-09 14:41:24
---
## 打卡日期
2016年10月9日，以下内容为`Computer Networking`的读书笔记之第`3`篇。距离上一次打卡日期已经过去了N长时间，shame one me。这个章节主要介绍了分组交换网络的延迟、丢包和吞吐量。

## 书籍链接
附上书籍链接：[Computer Networking](https://book.douban.com/subject/10573157/)

## 书籍章节
第1.4.1章，35页-39页

## 读书笔记

### 吞吐量（throughput）
吞吐量是指网络每秒能够传输的数据量。

### 延迟（delay）
当一个数据包从一个节点（主机、路由器）传输到下一个节点（主机、路由器）时，受到以下几种延迟的影响：节点`处理延迟`（`nodal processing delay`）、`队列延迟`（`queuing delay`）、`传输延迟`（`transmission delay`）、`传播延迟`（`propagation delay`），加在一起叫做`总节点延迟`（`total nodal delay`）。

#### 处理延迟（processing delay）
处理延迟包括了检测数据包头部信息，决定下一个节点位置，检测数据准确性等。处理延迟通常在微秒级或者更低。

#### 队列延迟（queuing delay）
如果队列中不存在任何其他数据包，那么队列延迟为0。队列延迟的程度取决于队列中的数据包数量以及线路的拥堵情况。实际应用中，这个延迟通常在微秒到毫秒级。

#### 传输延迟（transmission delay）
大部分分组交换网络都按照`先到先服务`的方式传输数据。对于10M以太网来说，它的传输速率为10 Mbps，对于100M以太网来说，它的传输速率为100Mbps，假设一条线路的传输速率为R bits/sec，一个数据包的长度为L bits，那么传输延迟为L/R seconds。实际应用中，这个延迟通常在微秒到毫秒级。

#### 传播延迟（propagation delay）
一旦一位（one bit）数据传输到线路中，它就会传播到下一个节点，所花费的时间叫做传播延迟。传播延迟的程度取决于线路的传播介质（光纤、铜线等）。传播延迟的通常在以下区间内
![computer-networking-three](/images/computer-networking-three.png)
传播延迟小于等于光速。在广域网络中，传播延迟一般在毫秒级。

### 对比传输延迟和传播延迟
传输延迟是路由器将一个数据包推出到线路中所花费的时间，传播延迟是从节点A传播到节点B所花费的时间。传输延迟取决于数据包的长度，线路的传输速率，和线路的长度无关。传播延迟取决于线路的长度。
话说还是不太理解为什么会出现传输延迟，还有所谓的传输速率是什么玩意儿，有网友举例说，如果有人每秒敲10次鼓，0.1秒后你听到鼓声，那么传输速率为10次/sec，传输时间为1s，传播速率就是声音传播的速率，传播时间为0.1s。