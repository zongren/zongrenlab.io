---
title: Android路由设计
category: android
tags: [router]
date: 2016-10-25 15:44:24
---
首先需要说明的是远程调用（外部调用）是本地调用（内部调用）的子集，也就是说远程调用是通过本地调用完成的。
参考文章列表
{% blockquote %}
* [ActivityRouter路由框架：通过注解实现URL打开Activity](https://joyrun.github.io/2016/08/01/ActivityRouter/)
* [Android路由框架设计与实现](http://www.sixwolf.net/blog/2016/03/23/Android%E8%B7%AF%E7%94%B1%E6%A1%86%E6%9E%B6%E8%AE%BE%E8%AE%A1/)
{% endblockquote %}