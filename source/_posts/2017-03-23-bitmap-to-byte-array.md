---
title: 将bitmap转为字节
category: miscellaneous
tags: [io]
date: 2017-03-23 17:29:16
excerpt: 将bitmap转为字节
---
```
Bitmap bitmap = intent.getExtras().get("data");
ByteArrayOutputStream stream = new ByteArrayOutputStream();
bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
byte[] byteArray = stream.toByteArray();

```