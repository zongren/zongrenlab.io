---
title: iOS团队协作开发
category: ios
tags: [team,dev-op]
excerpt: iOS团队协作开发
date: 2020-05-06 17:05:36
---

## 背景知识

### gem版本
使用[rvm](https://rvm.io/)工具管理多个版本的gem

### cocoapods版本
之前的版本可以通过```_x.y.z```的方式指定pod版本
```
pod _1.0.0_ install
```
最新版此命令不生效，仍然使用本地最新版本的pod

### cocoapods镜像
参考[清华镜像说明](https://mirrors.tuna.tsinghua.edu.cn/help/CocoaPods/)

### github代理
添加git全局配置
```
http.https://github.com.proxy=socks5://127.0.0.1:1080
```

## 忽略文件