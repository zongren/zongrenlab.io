---
title: 解决adb push权限问题
category: android
tags: [adb,permission]
excerpt: 解决adb push权限问题
date: 2018-11-29 17:27:59
---
如果想adb push一个文件到Android系统目录，例如system文件夹，没有权限的话，会提示
```
adb: error: failed to copy 'Videos\desktop.ini' to '/system/Videos/desktop.ini': remote Read-only file system
```

一般来说，通过以下方式既可以获取root权限，从而修改系统文件
```
adb root
adb remount
```

但有些设备可能需要通过以下方式
```
mount -o remount,rw /system
```