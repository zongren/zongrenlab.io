---
title: 更有效的枚举类型
date: 2016-05-31 11:49:25
category: android
tags: [java,enum]
---

```
private enum ITEM_CODE{
    ONE_COLUMN_AD("m_ad_one"),
    THREE_COLUMN_AD("m_ad_three"),
    NAVIGATOR("m_nav"),
    ;

    private static Map<Integer,ITEM_CODE> mMap = new HashMap<>();
    private static List<String> mList = new ArrayList<>();

    static {
        for (ITEM_CODE itemCode : ITEM_CODE.values()) {
            mList.add(itemCode.getValue());
            mMap.put(itemCode.ordinal(),itemCode);
        }
    }
    public static List<String> getList(){
        return mList;
    }

    public static ITEM_CODE getItemCode(String value){
        for (ITEM_CODE itemCode : ITEM_CODE.values()) {
            if(itemCode.mValue.equals(value)){
                return itemCode;
            }
        }
        throw new IllegalArgumentException();
    }

    public static ITEM_CODE valueOf(int ordinal){
        return mMap.get(ordinal);
    }
    private String mValue;

    ITEM_CODE(final String value){
        mValue = value;
    }

    public String getValue(){
        return mValue;
    }
}
```