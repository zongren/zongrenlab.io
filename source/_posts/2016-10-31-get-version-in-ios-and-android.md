---
title: iOS和Android获取版本信息
category: development
tags: [ios,android,version]
date: 2016-10-31 15:09:15
---
首先看一下iOS中如何获取当前的版本名称（Version）和版本号（Build），以下代码是使用宏的方式获取
```
#define VERSION [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]
#define BUILD [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey]
```
Android获取版本号有两种方法，第一种是使用`getPackageManager()`，代码如下
```
/**
 * Get app`s version code.
 *
 * @param context
 * @return
*/
public static int getVersionCode(Context context) {
    int code = 1;
    try {
        code = context.getPackageManager().getPackageInfo(context.getPackageName(),
                0).versionCode;

    } catch (PackageManager.NameNotFoundException e) {
        Log.e(TAG, "Failed to get app version name.");
    }
    return code;
}

/**
 * Get app`s version name.
 *
 * @param context
 * @return
 */
public static String getVersionName(Context context) {
    String version = "1.0.0";
    try {
        version = context.getPackageManager().getPackageInfo(context.getPackageName(),
                0).versionName;

    } catch (PackageManager.NameNotFoundException e) {
        Log.e(TAG, "Failed to get app version name.");
    }
    return version;
}
```

第二种只适用于`Android Studio`+`Gradle`的项目，不过应该算是目前Android项目的标配了
```
String versionName = BuildConfig.VERSION_NAME;
int versionCode = BuildConfig.VERSION_CODE;
```
需要注意导入正确的BuildConfig文件。