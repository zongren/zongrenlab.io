---
title: 整理你的Xcode项目（二）
date: 2016-07-07 15:52:59
category: ios
tags: [xcode]
---
## 介绍
之前的{% post_link make-your-xcode-project-neat 整理你的Xcode项目（一） %}讲了如何使用`Synx`整理项目文件，这篇文章讲一下如何使用[xUnique](https://github.com/truebit/xUnique)减少`xcodeproj`文件的冲突。
`xUnique`用于解决相同文件在不同机器上UUID不同的"BUG"，

## 安装
* 使用pip安装
```
pip install xUnique
```
* 从github下载然后安装
```
python setup.py install
```

## 使用
```
xunique [options] path/to/your/project.xcodeproj
```
更多参数请参考github项目主页