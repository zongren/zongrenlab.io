---
title: 安卓线程管理
date: 2016-05-31 23:53:36
category: android
tags: [thread]
---

## 跟UI主线程交互
首先需要声明一个`Handler`对象
```
mHandler = new Handler(Looper.getMainLooper()){
    public void handleMessage(Message msg) {
        switch (msg.what){
            case MESSAGE_WHAT_LOAD_IMAGE:
                handleImageLoadMessage(msg.getData());
                break;
        }
    }
};
```
向这个`Handler`发送消息
```
Message message = new Message();
Bundle data = new Bundle();
data.putInt(BUNDLE_KEY_IMAGE_WIDTH,imageWidth);
data.putInt(BUNDLE_KEY_IMAGE_HEIGHT,imageHeight);
data.putString(BUNDLE_KEY_IMAGE_URI,imageUri);
message.what = MESSAGE_WHAT_LOAD_IMAGE;
message.setData(data);
mHandler.sendMessage(message);
```