---
title: Android中的sp和dp
category: android
tags: [unit]
excerpt: 简单记录一下sp和dp的不同
date: 2017-07-06 09:42:48
---
## dp(Density-independent Pixels)和sp(Scale-independent Pixels)对比

{% img /images/3_7_480_800_252_hdpi.png 3_7_480_800_252_hdpi %}
{% img /images/3_7_720_1280_397_xhdpi.png 3_7_720_1280_397_xhdpi %}
{% img /images/zoom.png zoomed-image %}
{% img /images/full.png full-size-image %}

可以看到，一般情况，这两种单位是相同的

## 屏幕尺寸、分辨率、ppi（dpi）和density bucket
`density bucket`是指Android设备对应的像素密度。由于Android设备的尺寸和分辨率太多，开发中不可能针对每种情况进行适配，所以官方提供了集中`density bucket`，开发人员只要针对这几种情况进行适配即可，节省了大量的时间，常见的几种Density Bucket有以下几种

| Density Bucket | Screen Density | Physical Size | Pixel Size                    | 
| ---- | ----- | ----- | ------ |
| ldpi           | 120 dpi        | 0.5 x 0.5 in  | 0.5 in * 120 dpi = 60x60 px   | 
| mdpi           | 160 dpi        | 0.5 x 0.5 in  | 0.5 in * 160 dpi = 80x80 px   | 
| hdpi           | 240 dpi        | 0.5 x 0.5 in  | 0.5 in * 240 dpi = 120x120 px | 
| xhdpi          | 320 dpi        | 0.5 x 0.5 in  | 0.5 in * 320 dpi = 160x160 px | 
| xxhdpi         | 480 dpi        | 0.5 x 0.5 in  | 0.5 in * 480 dpi = 240x240 px | 
| xxxhdpi        | 640 dpi        | 0.5 x 0.5 in  | 0.5 in * 640 dpi = 320x320 px | 

## Font Scale
我们知道Android设备是可以设置字体大小的，那么在不同的设置下，`dp`和`sp`两种单位的文本会发生什么变化呢
下图展示了最小字体和最大字体的对比
{% img /images/font-scale-compare.png font-scale-compare %}