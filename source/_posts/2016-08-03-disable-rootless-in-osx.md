---
title: 关闭System Integrity Protection
date: 2016-08-03 10:24:04
category: miscellaneous
tags: [osx,sip,rootless]
---

## 进入恢复模式
开机听到响声时按下`Command`+`R`
## 打开终端
在工具(OS X Utilities)下打开终端(Terminal)
## 输入代码
```
csrutil disable; reboot
```
## 检测是否关闭成功
```
csrutil status
```
如果关闭成功，回显示以下代码
```
System Integrity Protection status: disabled.
```