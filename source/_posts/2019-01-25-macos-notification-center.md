---
title: 禁用（启用）macOS通知中心
category: miscellaneous
tags: [macOS]
excerpt: 禁用（启用）macOS通知中心
date: 2019-01-25 14:53:22
---

## 禁用
终端执行以下命令
```
launchctl unload -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist
killall NotificationCenter
```

## 启用
```
launchctl load -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist
open /System/Library/CoreServices/NotificationCenter.app/
```