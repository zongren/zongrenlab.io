---
title: SVN版本回退
category: miscellaneous
tags: [svn]
date: 2017-04-05 17:56:26
excerpt: 将指定的文件回退到指定的版本上
---

如果不小心把错误的代码提交到了服务器端，可以使用`svn merge`将其回退，例如
```
svn merge -r 22746:22739 SomeFile.java

```
这条命令的意思就是将`SomeFile.java`文件从`22746`版本回退到`22739`版本