---
title: 在sed命令中使用变量
category: development
tags: [terminal]
date: 2017-03-30 10:44:00
excerpt: 在sed命令中使用变量
---

本文介绍了如何在sed命令中使用变量，并替换包含`/`的字符

{% raw %}
<div style="overflow-x:auto;overflow-y:hidden;width:100%;"><a href="/images/sed-variable.png" target="_blank"><img src="/images/sed-variable.png" alt="sed-variable.png" style="max-width:unset;width:500px;"/></a></div>
{% endraw %}