---
title: Ransom Note
date: 2016-08-31 09:20:49
category: leetcode
tags: [algorithm]
---
## 问题链接
[Ransom Note | LeetCode OJ](https://leetcode.com/problems/ransom-note/)

## 问题描述
给定任意`勒索`字符串，同时有一个`杂志`字符串，如果`勒索`字符串的所有字符都可以从`杂志`字符串找到，那么返回true，否则，它将返回false。在该杂志的字符串每个字母只能在你的赎金笔记使用一次。你可以假设两个字符串仅包含小写字母。例如：
```
canConstruct("a", "b") -> false
canConstruct("aa", "ab") -> false
canConstruct("aa", "aab") -> true
```


## 解决办法
以下为我的解决方法，语言为Java
```
public class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        while(ransomNote.length() > 0){
            if(ransomNote.length() == 1){
                return magazine.contains(ransomNote);
            }
            else{
                String a = ransomNote.substring(0,1);
                ransomNote = ransomNote.substring(1);
                int position = magazine.indexOf(a);
                if(position >= 0){
                    magazine = remove(magazine,position);
                }
                else{
                    return false;
                }
            }
        }
        return true;
    }
    
    public String remove(String s,int position){
        int originalLength = s.length();
        if (position == 0){
            s = s.substring(1);
        }
        else if(position == originalLength){
            s = s.substring(0,position);
        }
        else{
            s = s.substring(0,position)+s.substring(position+1,originalLength);
        }
        return s;
    }
}
```
