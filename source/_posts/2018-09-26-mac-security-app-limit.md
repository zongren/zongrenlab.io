---
title: macOS“任何来源”选项的显示和隐藏
category: miscellaneous
tags: [macOS]
excerpt: 如何控制macOS“任何来源”选项的显示和隐藏。。。
date: 2018-09-26 15:17:02
---

显示“任何来源“选项，使用终端输入以下命令
```
sudo spctl --master-disable
```

隐藏“任何来源”选项，使用终端输入以下命令
```
sudo spctl --master-enable
```