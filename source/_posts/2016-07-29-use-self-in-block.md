---
title: 在block中使用self
date: 2016-07-29 14:06:04
category: ios
tags: [block,self]
---
在block直接使用self会使编译器报以下错误
```
//Capture 'self' strongly in this block is likely to lead to a retain circle
```
解决办法是在block中使用```self```的弱引用(weak reference)
```
__unsafe_unretained typeof(self) weakSelf = self;
self.refreshCallback = ^void(id data) {
    [weakSelf.goodsCollectionView.mj_header endRefreshing];
    ...
};
```
