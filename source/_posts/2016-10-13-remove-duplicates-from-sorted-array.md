---
title: remove-duplicates-from-sorted-array
category: leetcode
tags:
  - algorithm
date: 2016-10-13 08:42:35
---

## 问题链接
[Remove Duplicates from Sorted Array | LeetCode OJ](https://leetcode.com/problems/remove-duplicates-from-sorted-array/)

## 问题描述
给出一个有序数组，移除重复的元素，并且返回移除后的数组长度`l`，可以忽略`l-1`之后的元素。


## 解决办法
```
public class Solution {
    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int previousNumber = nums[0];
        int length = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != previousNumber) {
                previousNumber = nums[i];
                nums[length] = nums[i];
                length++;
            }
        }
        return length;
    }
}
```
