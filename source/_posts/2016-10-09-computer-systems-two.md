---
title: Computer Systems读书笔记（2）
category: reading
tags:
  - book
  - computer
date: 2016-10-09 14:39:13
---
## 打卡日期
2016年10月9日，以下内容为`Computer Systems`的读书笔记之第`2`篇。

## 书籍链接
附上书籍链接：[Computer Systems](https://book.douban.com/subject/3023631/)

## 书籍章节
第1.5、1.6章，12页-16页

## 读书笔记

### 缓存
大容量的存储设备一般比小容量的存储设备慢。处理器从寄存器读取数据比从内存读取数据快100倍，为了解决这个`处理器内存差距`（***`processor-memory gap`***），系统设计者加入了`高速缓存存储器`（`cache memories`），由`静态随机存取存储器`（***`static random access memory`***，SRAM）技术实现。当今的计算机设备包括了多级缓存，常见的有`L1`，`L2`，`L3`三级缓存。