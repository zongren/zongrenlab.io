---
title: 修复“Couldn't load ** from loader dalvik.system.PathClassLoader”的错误
category: miscellaneous
tags: []
excerpt: 修复“Couldn't load ** from loader dalvik.system.PathClassLoader”的错误
date: 2017-04-08 11:09:21
---

在gradle文件中添加以下配置

```
android {
    ...
    defaultConfig {
        ...
        ndk {
            abiFilters "armeabi"
        }
    }
```