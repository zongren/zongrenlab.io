---
title: 创建macOS安装U盘
category: miscellaneous
tags: [macos]
date: 2016-11-23 21:03:10
---
使用一条命令创建`macOS Sierra`的安装U盘
```
sudo /Applications/Install\ macOS\ Sierra.app/Contents/Resources/createinstallmedia --volume /Volumes/macOS --applicationpath /Applications/Install\ macOS\ Sierra.app --nointeraction
```
其中`/Volumes/macOS`是U盘的路径