---
title: iOS下实现父视图高度自适应
date: 2016-08-23 16:37:47
category: ios
tags: [autolayout,height]
---
如何在iOS实现父视图高度或者宽度自适应呢，简单来说是通过添加约束来完成的，但是必须满足以下条件，以高度自适应为例
* 子视图之间按从上到下的顺序添加约束
* 最上方的子视图和父视图添加约束
* 最下方的子视图和父视图添加约束 

例如父视图fartherView有三个子视图childA，childB，childC，fartherView想高度自适应，则按照如下方式添加约束（所有约束通过Masonry添加
```
[childA mas_makeConstraints:^(MASConstraintMaker* make) {
    make.top.equalTo (fartherView);
}];
[childB mas_makeConstraints:^(MASConstraintMaker* make) {
    make.top.equalTo (childA.mas_bottom);
}];
[childC mas_makeConstraints:^(MASConstraintMaker* make) {
    make.top.equalTo (childB.mas_bottom);
    make.bottom.equalTo(fartherView);
}];
```
