---
title: Fragment中的菜单
category: android
tags: [fragment,menu]
date: 2016-11-08 09:18:01
excerpt: Fragment中的菜单
---
你知道可以在Fragment中直接使用menu吗
```
public class BusinessHourFragment extends CommonFragment {
    ...
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.business_hour, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.business_hour_save:
                confirm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
```
