---
title: 获取apk包名
category: android
tags: [aapt]
excerpt: 获取apk包名
date: 2018-04-17 15:24:23
---
linux下的命令
```
aapt dump badging <path-to-apk> | grep package:\ name
```

windows下的命令
```
aapt dump badging <path-to-apk> | findstr -i "package:name"
```

appt一般在```Android/Sdk/build-tools/{version}```/目录下