---
title: 遍历Bundle中的变量
date: 2016-05-23 11:37:16
category: android
tags: [iteration,bundle]
---
Here's what I used to get information on an undocumented (3rd-party) intent:
```
for (String key : bundle.keySet()) {
    Object value = bundle.get(key);
    Log.d(TAG, String.format("%s %s (%s)", key,  
        value.toString(), value.getClass().getName()));
}
```
(Make sure to check if bundle is null before the loop)