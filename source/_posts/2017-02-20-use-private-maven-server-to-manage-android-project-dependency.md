---
title: 使用私有maven服务器管理Android项目依赖
category: android
tags: [dependency,project]
date: 2017-02-20 20:12:51
excerpt: 如何使用私有maven服务器管理Android项目依赖。
---

之前的{% post_link external-and-library-dependency 一篇文章 %}记录了如何使用`git submodule`（或者`svn external`）功能和`gradle`进行依赖管理，但是实际操作的时候发现一个问题，即Android库和Android项目包名部分重复的时候，修改包名就变得非常麻烦，举例说明

项目A依赖于基础库B，A的包名为`me.zongren.projectA`，B的包名为`me.zongren.libraryB`，此时我需要修改A的包名，我们知道使用Android Studio修改包名还是比较方便的，它会自动修改`import`语句，减少工作量，但是当我们把A的包名修改为`com.other.projectA`的时候，B的包名也会跟着改为`com.other.libraryB`。更糟糕的是，如果我不小心将这些改动提交到版本控制中，就会对其他所有的项目造成影响。

所以这种方式并不是很适合多个项目同时依赖一个基础库，并且每个项目都有可能更改包名的情况，针对这种情况，必须建立私有maven服务器。

建立私有maven服务器，可以同时满足方便修改包名、保护私有库文件、享受gradle打包的便利，同时也能满足嵌套依赖（例如B依赖基础库C）。

## 安装maven服务器
我使用的是[nexus](https://www.sonatype.com/download-oss-sonatype)，按照[官方教程](http://books.sonatype.com/nexus-book/reference3/index.html)安装，没有什么需要特殊说明的地方。

## 上传库文件
maven服务器创建之后就可以上传库文件了，上传工具同样是gradle，具体流程

配置library下的gradle.properties文件
```
NEXUS_VERSION=1.0
NEXUS_GROUP_ID=me.zongren
NEXUS_ARTIFACT_ID=libraryB

NEXUS_RELEASE_URL=http://192.168.1.2:8081/repository/maven-release/
NEXUS_SNAPSHOT_URL=http://192.168.1.2:8081/repository/maven-snapshot/

NEXUS_USERNAME=admin
NEXUS_PASSWORD=admin123
```

为了保证build.gradle的简洁性，我们新建一个gradle文件，nexus.gradle
```
apply plugin: 'maven'
apply plugin: 'signing'

configurations {
    deployerJars
}

repositories {
    mavenCentral()
}

// 判断版本是Release or Snapshots
def isReleaseBuild() {
    return !VERSION.contains("SNAPSHOT");
}

// 获取仓库url
def getRepositoryUrl() {
    return isReleaseBuild() ? NEXUS_RELEASE_URL : NEXUS_SNAPSHOT_URL;
}

uploadArchives {
    repositories {
        mavenDeployer {
            beforeDeployment {
                MavenDeployment deployment -> signing.signPom(deployment)
            }

            pom.version = NEXUS_VERSION
            pom.artifactId = NEXUS_ARTIFACT_ID
            pom.groupId = NEXUS_GROUP_ID

            repository(url: getRepositoryUrl()) {
                authentication(userName: NEXUS_USERNAME, password: NEXUS_PASSWORD)
            }
        }
    }
}

// 进行数字签名
signing {
    // 当 发布版本 & 存在"uploadArchives"任务时，才执行
    required { isReleaseBuild() && gradle.taskGraph.hasTask("uploadArchives") }
    sign configurations.archives
}

// type显示指定任务类型或任务, 这里指定要执行Javadoc这个task,这个task在gradle中已经定义
task androidJavadocs(type: Javadoc) {
    // 设置源码所在的位置
    source = android.sourceSets.main.java.sourceFiles
}

// 生成javadoc.jar
task androidJavadocsJar(type: Jar) {
    // 指定文档名称
    classifier = 'javadoc'
    from androidJavadocs.destinationDir
}

// 生成sources.jar
task androidSourcesJar(type: Jar) {
    classifier = 'sources'
    from android.sourceSets.main.java.sourceFiles
}

// 产生相关配置文件的任务
artifacts {
    archives androidSourcesJar
    archives androidJavadocsJar
}
```

然后在build.gradle文件中调用nexus.gradle
```
apply from: 'nexus.gradle'
```
## 使用库文件
使用私有maven服务器上的库文件很简单，只需设置好服务器地址，然后就在gradle中添加依赖即可。
修改根目录build.gradle文件
```
allprojects {
    repositories {
        maven { url "http://192.168.1.2:8081/repository/maven-snapshot/" }
    }
}
```

在module下的build.gradle文件中添加依赖
```
dependencies {
    ...

    compile 'me.zongren:libraryB:1.0-SNAPSHOT'
}
```
除了上述这些原因促使我更改基础库的依赖方式和管理工具，还有一个非常重要的因素就是一致性，即统一了公司iOS项目和Android项目依赖管理方式，作为技术负责人来说我感到非常舒心。
从去年年底，我就渐渐把公司项目的项目依赖管理工具从[CocoaPods](https://cocoapods.org/)改为[Carthage](https://github.com/Carthage/Carthage)了，可以看一下{% post_link carthage-with-nested-dependencies 这篇文章 %}，简单了解一下。