---
title: 给nojs添加切换代码高亮风格的功能
date: 2016-08-31 11:19:19
category: miscellaneous
tags: [nojs,theme]
---
Hexo默认使用highlight.js生成高亮代码，而高亮风格需要主题来实现，现在给nojs主题添加切换高亮代码风格的功能。简单来说，这个功能就是通过修改`nojs/_config.yml`里的一行代码，自动更换代码高亮的风格。例如使用highlight.js官方的rainbow主题，只需在主题配置文件中修改
```
highlight_theme: rainbow
```