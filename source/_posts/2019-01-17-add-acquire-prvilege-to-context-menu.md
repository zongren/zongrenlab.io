---
title: 添加”管理员获取所有权“右键菜单
category: miscellaneous
tags: [windows,registor]
excerpt: 添加”管理员获取所有权“右键菜单
date: 2019-01-17 10:12:11
---

将下面的内容保存成```privilege.reg```，双击运行
```
Windows Registry Editor Version 5.00

 [HKEY_CLASSES_ROOT\*\shell\runas]
 @="管理员取得所有权"
 "NoWorkingDirectory"=""

 [HKEY_CLASSES_ROOT\*\shell\runas\command]
 @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
 "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"

 [HKEY_CLASSES_ROOT\exefile\shell\runas2]
 @="管理员取得所有权"
 "NoWorkingDirectory"=""

 [HKEY_CLASSES_ROOT\exefile\shell\runas2\command]
 @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
 "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"

 [HKEY_CLASSES_ROOT\Directory\shell\runas]
 @="管理员取得所有权"
 "NoWorkingDirectory"=""

 [HKEY_CLASSES_ROOT\Directory\shell\runas\command]
 @="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t"
 "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t"
```