---
title: 使用JUnit测试HttpClient库
category: android
tags: [http,junit]
excerpt: 使用JUnit测试HttpClient库
date: 2019-02-21 13:43:12
---

Android本身继承了一个老版本的apache httpclient，如下所示
```
android{
    useLibrary  'org.apache.http.legacy'
}
```

如果你想通过junit测试这个库，会提示以下错误
```
java.lang.RuntimeException: Stub!
```

这是因为这个库只在Android系统中，所以相关测试必须在Android模拟器或真机上运行，这样效率会非常低，那么有没有办法解决这个问题呢

解决思路是看下Android系统使用的什么版本的httpclient和httpcore，然后通过gradle依赖相同版本的jar即可，通过一番搜索，发现Google并没有使用某个指定版本的库
只能找最接近的库，详见[stackoverflow](https://stackoverflow.com/questions/2618573/what-version-of-apache-http-client-is-bundled-in-android-1-6/4818714#4818714)
```
android{
    //useLibrary  'org.apache.http.legacy'
}

dependencies{
    implementation 'org.apache.httpcomponents:httpclient:4.0-beta1'
    implementation 'org.apache.httpcomponents:httpcore:4.0-beta2'
}

```

测试完成后，将依赖改回Android
```
android{
    useLibrary  'org.apache.http.legacy'
}
dependencies{
    //implementation 'org.apache.httpcomponents:httpclient:4.0-beta1'
    //implementation 'org.apache.httpcomponents:httpcore:4.0-beta2'
}
```