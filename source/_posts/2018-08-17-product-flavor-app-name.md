---
title: 多版本打包使用不同的应用名称
category: android
tags: [gradle,build]
excerpt: 多版本打包使用不同的应用名称
date: 2018-08-17 16:07:54
---
首先需要删除默认string.xml的app_name资源，然后在defaultConfig添加默认配置

```
defaultConfig {
    resValue "string", "app_name", "AppName"
}
```

然后根据不同的flavor设置应用包名

```
productFlavors {
    flavor1{
        dimension "dimension1"
        resValue "string", "app_name", "AppFlavor1"
    }

    flavor2{
        dimension "dimension1"
        resValue "string", "app_name", "AppFlavor2"
    }
}
```