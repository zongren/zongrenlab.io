---
title: 自动验证letsencrypt
date: 2016-10-13 14:01:25
category: miscellaneous
tags: [gitlab-pages,letsencrypt]
---

## 更新
请参考{% post_link use-gitlab-letsencrypt-to-auto-renew-gitlab-page-https 新版教程 %}，此插件不再维护

在之前的一篇{% post_link add-tls-to-gitlab-pages 文章 %}介绍过使用`let's encrypt`给`gitlab pages`添加`tls`支持，操作起来还是比较麻烦的，又加上`let's encrypt`的证书90天后过期，而gitlab又不支持自动更新`let's encrypt`证书，所以最好的办法就是使用[gitlab-letsencrypt](https://github.com/rolodato/gitlab-letsencrypt)插件自动验证并创建证书。然而对于使用hexo的情况并不适用，所以对原插件稍加修改（大概加了几行代码），使其适用于`hexo`，详细使用说明请参考[github](https://github.com/zongren/gitlab-letsencrypt-hexo)。
需要注意的是，`gitlab-letsencrypt-hexo`需要配合使用[hexo-processor-static](https://github.com/lakenen/hexo-processor-static)插件才可以。