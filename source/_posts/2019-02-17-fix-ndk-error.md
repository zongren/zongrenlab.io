---
title: 解决老版本项目NDK报错
category: android
tags: [ndk]
excerpt: 解决老版本项目NDK报错
date: 2019-02-17 18:02:16
---

使用新版本Android Studio（新版本NDK）打开一些比较老的项目（老的gradle版本），有可能提示以下错误
```
No toolchains found in the NDK toolchains folder for ABI with prefix mips64el-linux-android
```
或者
```
No toolchains found in the NDK toolchains folder for ABI with prefix mipsel-linux-android
```
解决方案是将```android gradle plugin```升级到```3.1```或更高的版本

如果不能升级grale版本，那么可以通过以下方式临时解决，仅针对macOS
```
cd  ~/Library/Android/sdk/ndk-bundle/toolchains
ln -s aarch64-linux-android-4.9 mips64el-linux-android
ln -s arm-linux-androideabi-4.9 mipsel-linux-android
```