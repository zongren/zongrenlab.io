---
title: 修复“is waiting for the debugger to attach”的错误
category: android
tags: [android-studio]
date: 2017-04-05 10:37:15
excerpt: 修复“is waiting for the debugger to attach”的错误
---

方法1："Run"->"Attach debugger to Android process"
{% img /images/attach-debugger-to-process.png attach-debugger-to-process.png %}

方法2： 直接点击"Attach debugger to Android process"
{% img /images/attach-debugger-to-process-2.png attach-debugger-to-process-2.png %}

P.S. 上面两个方法有瑕疵，每次`debug`的时候都需要点击一次

方法3： 在Android设备中，点击"开发者选项"->"选择调试应用"

P.S. 不知道为什么，上面的方法在我的设备上不起作用