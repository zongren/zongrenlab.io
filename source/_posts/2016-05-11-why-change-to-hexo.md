---
title: 为什么使用Hexo
date: 2016-05-11 21:09:23
category: miscellaneous
tags: [hexo,jekyll,blog]
excerpt: 介绍为什么使用hexo作为本博客的博客生成系统。
---
我的博客一开始使用`Jekyll`作为构建系统，后来尝试使用`hexo`，发现`hexo`更合我的心意，主要是`hexo`基于`nodejs`，`jekyll`基于`ruby`，个人对于`js`更加熟悉，遇到一些问题可以自己解决，并且`hexo`生态环境更加成熟，插件更加丰富。