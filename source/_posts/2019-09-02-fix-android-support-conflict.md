---
title: 解决support库冲突
category: android
tags: [dependencies]
excerpt: 解决support库冲突
date: 2019-09-02 14:23:35
---
在项目根目录build.gralde中添加以下代码，指定support库所使用的版本，注意multidex和其它support库不同
```
subprojects {
    project.configurations.all {
        resolutionStrategy.eachDependency { detail ->
            if (detail.requested.group == 'com.android.support'
                    && !detail.requested.name.contains('multidex') ) {
                detail.useVersion 'x.y.z'
            }
        }
    }
}
```