---
title: AdHoc安装失败
date: 2016-08-10 10:38:54
category: ios
tags: [archive,install,adhoc]
---
最近使用[fir.im](http://fir.im)测试应用，发布之后，发现安装不了，使用官方提供的工具[Log Guru](http://fir.im/tools/log_guru)检测之后发现提示以下错误
```
10:45:59 installd➜ SecTrustEvaluate  [leaf IssuerCommonName SubjectCommonName]
10:45:59 installd➜0x16e1bb000 +[MICodeSigningVerifier _validateSignatureAndCopyInfoForURL:withOptions:error:]: 142: Failed to verify code signature of /private/var/installd/Library/Caches/com.apple.mobile.installd.staging/temp.2m3w05/extracted/Payload/App.app/Frameworks/AlipaySDK.framework : 0xe8008019 (The application does not have a valid signature.)
```
经过检查发现，在`Build Phases`下的Link Binary With Libraries和Embed Frameworks各有一个AlipaySDK.framework，删掉Embed Frameworks下的AlipaySDK.framework，重新打包上传，问题解决。