---
title: 百度站内搜索文件生成插件
date: 2016-08-31 11:25:59
category: miscellaneous
tags: [blog,hexo,plugin]
---
随着博客内容越来越多，想要找出一篇之前写的文章变得非常不易，所以想使用百度站内搜索方便查找，在github上找到了[hexo-generator-baidu-sitemap](https://github.com/coneycode/hexo-generator-baidu-sitemap)插件，不过使用之后发现，这个插件生成的xml文件已经不符合百度的要求，所以fork修改了一下，源码已经放在了[github](https://github.com/zongren/hexo-generator-baidu-search)上，并且已经上传到了npm，直接使用以下代码安装即可
```
npm install hexo-generator-baidu-search --save
```
经测试一切正常，不过发现百度的两个坑，一是搜索内容只包括已经收录的页面，二是百度本身的搜索链接不支持https，所以最后也只得放弃了百度站内搜索。