---
title: JSONModel的使用方法（二）
date: 2016-07-08 16:05:05
category: ios
tags: [tip,note]
---

之前的文章{% post_link basic-use-of-jsonmodel JSONModel的使用方法（一） %}介绍了如何使用JSONModel解析，这篇文章介绍如何解析更加复杂的模型。首先看一下服务器端返回的模型
```
{
    "goods":{
        "1":{
            "name":"xxx",
            "id":"xxx",
            ...
        }
    }
}
```
解析方法如下
```
//ResponseModelAppIndexGoodsModel.h
@interface ResponseModelAppIndexGoodsModel : JSONModel
@property (nonatomic, strong) NSArray<ResponseModelAppIndexGoodsItemModel>* goods;
@end
//ResponseModelAppIndexGoodsModel.m
@implementation ResponseModelAppIndexGoodsModel
+ (JSONKeyMapper*)keyMapper {
    return [[JSONKeyMapper alloc] initWithDictionary:@{
        @"goods.1": @"goods"
    }];
}
@end
```

