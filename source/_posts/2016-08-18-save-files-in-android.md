---
title: 安卓下保存文件（一）
date: 2016-08-18 15:08:11
category: android
tags: [study,note]
---

## 选择内部存储或外部存储（Choose Internal or External Storage） 
首先要考虑的问题是将文件存储到哪里，这时有两种存储空间供你选择，内部存储和外部存储。内部存储通常是设备内置的存储空间，外部存储不一定为可移除的外部存储介质（如SD卡），也有可能是从内置存储空间划分出的一部分。下面的表格总结了这两种存储空间的区别。

| 内部存储（Internal Storage） | 外部存储（External Storage） |
| -------------------------- | -------------------------- | 
| 总是可用的                   | 不一定可用 |
| 只有你的应用可以访问           | 可以被其他应用访问 | 
| 系统卸载应用时，会同时删除该存储空间上的文件  | 系统卸载应用时，会删除getExternalFilesDir()目录下的文件，不会删除其它的文件 |


## 获取外部存储读写权限（Obtain Permissions for External Storage）
内部存储默认是可读写的，所以不需要申请权限，而外部存储需要申请权限后才可以读写。如果应用需要向外部存储空间写入文件，就必须在`manifest`文件申请写入权限
### 获取写入权限
```
<manifest ...>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    ...
</manifest>
```
应用申请写入权限后，也就隐式得拥有读取权限了。需要注意的是，目前所有应用不需要任何权限申请就可以读取外部存储，但是这种情况以后会发生变化，所以如果你的应用需要读取权限（并且不需要写入权限），最好在`manifest`中申请读取权限
### 获取读取权限
```
<manifest ...>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    ...
</manifest>
```

## 将文件保存到内部存储（Save a File on Internal Storage）
使用File构造方法创建新的文件
```
File file = new File(context.getFilesDir(), filename);
```
也可以使用`getCacheDir()`获取缓存目录，缓存目录下的文件有可能会被系统删掉。
将内容写入文件
```
String filename = "myfile";
String string = "Hello world!";
FileOutputStream outputStream;

try {
  outputStream = new FileOutputStream(file);
  outputStream.write(string.getBytes());
  outputStream.close();
} catch (Exception e) {
  e.printStackTrace();
}
```
你可以可使用`openFileOutput()`方法获取`FileOutputStream`，从而将内容写入文件
```
String filename = "myfile";
String string = "Hello world!";
FileOutputStream outputStream;

try {
  outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
  outputStream.write(string.getBytes());
  outputStream.close();
} catch (Exception e) {
  e.printStackTrace();
}
```

## 将文件保存到外部存储（Save a File on External Storage）
首先需要判断外部存储是否可用，官方代码如下
```
//判断外部存储是否可写
public boolean isExternalStorageWritable() {
    String state = Environment.getExternalStorageState();
    if (Environment.MEDIA_MOUNTED.equals(state)) {
        return true;
    }
    return false;
}

//判断外部存储是否可读
public boolean isExternalStorageReadable() {
    String state = Environment.getExternalStorageState();
    if (Environment.MEDIA_MOUNTED.equals(state) ||
        Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
        return true;
    }
    return false;
}
```
其次还要决定存储方式，外部存储分为公共和私有，区别为系统卸载应用时，不会被删除公共文件，但是会删除私有文件。使用 getExternalStoragePublicDirectory() 方法获取公共文件，getExternalFilesDir()获取私有文件。

## 查询可用空间（Query Free Space）
存储文件之前，可以先使用`getFreeSpace()`或者`getTotalSpace()`方法查询可用空间是否足够大，也可以不查询，直接在`IOException`中处理。

## 删除文件（Delete A File）
你可以直接调用`File`的`delete()`方法，也可以调用`Context`的`deleteFile()`方法，代码如下：
```
myFile.delete();
myContext.deleteFile(fileName);
```