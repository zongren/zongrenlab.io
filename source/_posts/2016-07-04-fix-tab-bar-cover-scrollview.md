---
title: 解决UIScrollView被UITabBar遮挡
date: 2016-07-04 10:46:03
category: ios
tags: [tip,note]
---

有时候UITabBar会遮挡UIScrollView，UITableView或者UICollectionView，解决办法是设置UIViewController的edgesForExtendedLayout属性为none，代码如下
```
self.edgesForExtendedLayout = UIRectEdgeNone;
```