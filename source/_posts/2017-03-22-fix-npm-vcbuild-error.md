---
title: 修复npm安装时报VCBuild不存在的错误
category: miscellaneous
tags: [npm]
date: 2017-03-22 09:59:05
excerpt: 修复npm安装时报VCBuild不存在的错误
---
在安装某个node组件时，提示以下错误
```
MSBUILD : error MSB3428: 未能加载 Visual C++ 组件“VCBuild.exe”。要解决此问题，1) 安装 .NET Framework 2.0 SDK；2) 安装 Microsoft Visual Stu
dio 2005；或 3) 如果将该组件安装到了其他位置，请将其位置添加到系统路径中。 [C:\Users\zongren\zongren.gitlab.io\node_modules\microtime\build\binding.sl
n]
```
从[StackOverflow](http://stackoverflow.com/a/39235952)上找到一个非常简单的解决方法，那就是使用npm安装windows build tools，代码如下
```
npm install --global --production windows-build-tools
```