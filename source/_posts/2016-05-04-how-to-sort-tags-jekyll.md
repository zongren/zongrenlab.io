---
title: 对Jekyll标签进行排序
date: 2016-05-04 21:33:28
category: miscellaneous
tags: [blog,liquid,jekyll]
excerpt: 本文简单介绍了在Jekyll博客系统中，如何对文章标签和分类进行排序。
---
我们知道`github pages`是一个非常好用的免费静态博客系统，但是它有很多限制，例如不支持自定义域名的加密链接（`https`），不支持自定义插件等，所以想要创建标签页和分类页的同学就只能自己动手了。创建标签页的方法很简单，在此就不做介绍了。

首先看一下，如何把标签按字母顺序进行排序
```
{% capture tags %}
  {% for tag in site.tags %}
    {{ tag | downcase | replace:' ','-' }}
  {% endfor %}
{% endcapture %}
{% assign sorted_post_tags = tags | split:' ' | sort %}
{% for sorted_tag in sorted_post_tags %}
  {% for tag in site.tags %}
    {% assign downcase_tag = tag | downcase | replace:' ','-' %}
    {% if downcase_tag == sorted_tag %}
      &lt;a href="{{ page.url }}#{{ tag[0] }}"&gt;{{ tag[0] }}&lt;/a&gt;
    {% endif %}
  {% endfor %}
{% endfor %}
```
______________________________

还可以根据标签下的文章数量进行排序
```
{% assign max_tag_count = 0 %}
  {% for tag in site.tags %}
    {% if tag[1].size > max_tag_count %}
      {% assign max_tag_count = tag[1].size %}
    {% endif %}
 {% endfor %}
 {% for i in (1..max_tag_count) reversed %}
    {% for tag in site.tags %}
      {% if tag[1].size == i %}
       &lt;a href="{{ page.url }}#{{ tag[0] }}"&gt;
        {{ tag[0] }}&lt;sup&gt;{{ i }}&lt;/sup&gt;
      &lt;/a&gt;
    {% endif %}
  {% endfor %}
{% endfor %}
```