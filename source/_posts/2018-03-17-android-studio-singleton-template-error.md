---
title: Android Studio提示singleton template错误
category: android
tags: [android-studio]
excerpt: Android Studio提示singleton template错误
date: 2018-03-14 09:41:26
---

右键创建singleton时，Android Studio提示以下错误
```
Unable to parse template "Singleton"
Error message: This template did not produce a Java class or an interface
```
解决方法是在对应的配置文件中添加以下代码，即studio64.exe.vmoptions或者studio.exe.vmoptions
```
-Djdk.util.zip.ensureTrailingSlash=false
```