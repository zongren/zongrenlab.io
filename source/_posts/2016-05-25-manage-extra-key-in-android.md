---
title: 键值管理
date: 2016-05-25 16:15:11
category: android
tags: [tip,enum,convention,standard,best-practice]
---

```
/**
 * Created by zongren on 2016/3/23.
 * Used in putExtra and getExtra functions.
 */
public enum EXTRA_KEY {
    PARENT_CATEGORY_ID,
    URL,
    BREAD_CRUMB,
    HTML,
    CATEGORY,
    LIST_VIEW_DATA,
    GOODS_ID,
    ORDER_ID;

    @Override
    public String toString(){
        return "me.zongren.android.EXTRA_KEY_" + super.toString();
    }
    
    public String getValue() {
        return toString();
    }

    public EXTRA_KEY getExtraKey(String value){
        for(EXTRA_KEY extraKey:values()){
            if(extraKey.toString().equals(value)){
                return extraKey;
            }
        }
        throw new IllegalArgumentException();
    }
}
```