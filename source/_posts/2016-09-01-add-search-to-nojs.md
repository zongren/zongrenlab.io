---
title: 给nojs主题加入搜索功能
date: 2016-09-01 15:55:10
category: miscellaneous
tags: [nojs,search]
---

网站内容多了之后，想要找到之前写的文章就非常困难，所以给博客加入了搜索功能，nojs主题提供了两种搜索工具

* [algolia](https://www.algolia.com/)
* [tapirgo](https://www.tapirgo.com/)

## 使用algolia
在**主题配置**文件中添加以下配置即可
```
algolia:
  appId: "ZBAGTKWCAI"
  apiKey: "ccb9d9123sdgfs31eaa45b89c59e2ba"
  indexName: "blog"
```
推荐使用[hexo-algoliasearch](https://github.com/LouisBarranqueiro/hexo-algoliasearch)插件自动上传文章记录，该插件的详细配置见其项目主页。需要注意的是，如果你使用了`hexo-algoliasearch`插件，那么就不要在主题配置文件再次添加`algolia`配置了（主题配置会自动加载站点配置文件的内容并且覆盖重复的字段）。

## 使用tapirgo
在**主题配置**文件中添加以下配置即可
```
tapir_token: "57c123asd4a14ad6664da6cc68b"
```
上传文章记录的方法见[官方网站](https://www.tapirgo.com/)。

algolia的搜索结果和搜索速度都比较快，并且支持分页，推荐使用algolia，缺点是由于是免费版，限制搜索次数。Tapir的优点是申请简单，操作简单，不限制搜索次数，缺点是不支持中文搜索，不支持手动添加文章记录。