---
title: 使用反射读取私有静态变量
category: miscellaneous
tags: [reflect,java]
date: 2017-04-05 10:29:34
excerpt: 使用反射读取私有静态变量，调用私有方法
---

关键在于使用`getDeclared**`方法

## 获取私有静态变量

```
Field field = object.getClass().getDeclaredField("mStaticField");
field.setAccessible(true);
Object value = field.get(object);
```

## 调用私有方法

```
Method method = object.getClass().getDeclaredMethod("privateMethod");
method.setAccessable(true);
Object returnValue = method.invoke(object,argumentOne);
```