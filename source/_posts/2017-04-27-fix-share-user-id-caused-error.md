---
title: sharedUserId
category: android
tags: [fix]
excerpt: 使用相同的sharedUserId导致的一个问题
date: 2017-04-27 19:23:22
---
问题表现为

1. 安装失败

```
[2017-04-27 19:20:03 - app] Installation error: INSTALL_FAILED_SHARED_USER_INCOMPATIBLE
[2017-04-27 19:20:03 - app] Please check logcat output for more details.
[2017-04-27 19:20:03 - app] Launch canceled!
```

2. 申请ROOT权限时提示`Multiple Packages`

解决方法是，不要使用相同的```android:sharedUserId```！不要使用相同的```android:sharedUserId```！不要使用相同的```android:sharedUserId```