---
title: 改变placeholder字体颜色
date: 2016-06-11 20:00:16
category: ios
tags: [placeholder,color]
---

有两个方法可以设置`placeholder`的颜色，代码如下：
```
//方法一
self.textField.attributedPlaceholder = [[NSAttributedString alloc]
            initWithString:@"Placeholder text"
                attributes:@{ NSForegroundColorAttributeName: [UIColor whiteColor] }];
//方法二
self.textField.placeholder = @"Placeholder text";
[self.textField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];  
```