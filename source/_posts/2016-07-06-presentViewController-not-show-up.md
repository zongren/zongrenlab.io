---
title: presentViewController不显示
date: 2016-07-06 16:05:12
category: ios
tags: [tip,note]
---

有时候调用`presentViewController`之后，视图并不会立刻出现，这是因为`main runloop being asleep`，解决办法如下
```
[self presentViewController:vc animated:YES completion:nil];
CFRunLoopWakeUp(CFRunLoopGetCurrent());
//Or you can enqueue an empty block to the main queue:
[self presentViewController:vc animated:YES completion:nil];
dispatch_async(dispatch_get_main_queue(), ^{});
```