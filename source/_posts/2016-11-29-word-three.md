---
title: 读书单词（3）
comments: false
category: word
tags:
  - book
  - english
date: 2016-11-29 17:12:12
excerpt: 记录读书遇到的单词。
---
<!-- https://cn.bing.com/dict/search?q= -->
## [enormous](https://cn.bing.com/dict/search?q=enormous):巨大的；大量的；庞大的
{% blockquote James F. Kurose/Keith W. Ross,Computer Networking %}
Given this enormous complexity, is there any hope of organizing a network architecture, or at least our discussion of network architecture?
{% endblockquote %}
{% blockquote Google Translate  http://translate.google.cn/?hl=en#en/zh-CN/Given%20this%20enormous%20complexity%2C%20is%20there%20any%20hope%20of%20organizing%20a%20network%20architecture%2C%20or%20at%20least%20our%20discussion%20of%20network%20architecture%3F link %}
鉴于这种巨大的复杂性，组织网络架构，或者至少我们讨论网络架构有任何希望吗？
{% endblockquote %}

## [encapsulation](https://cn.bing.com/dict/search?q=encapsulation):封装；封闭；

{% blockquote %}
Firewalls allow for enforcement of the domain encapsulation.
防火墙允许增强对域的封装。
{% endblockquote %}