---
title: 修复支付宝、百川SDK冲突
date: 2016-08-25 14:05:39
category: miscellaneous
tags: [android,alipay]
---
如果你要同时使用支付宝SDK和百川SDK，那么需要下载真对百川SDK处理过的支付宝SDk，下载地址为https://doc.open.alipay.com/doc2/detail.htm?treeId=54&articleId=104509&docType=1。
P.S.
如果你使用AndroidStudio，那么需要用File>New>Import Module的方式导入百川SDK。
