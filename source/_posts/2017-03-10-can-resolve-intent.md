---
title: 判断知否支持Intent
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 判断知否支持Intent
date: 2017-03-10 17:43:49
---

```
PackageManager manager = context.getPackageManager();
List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
if (infos.size() > 0) {
    //Then there is an Application(s) can handle your intent
} else {
    //No Application can handle your intent
}
```