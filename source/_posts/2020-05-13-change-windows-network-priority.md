---
title: 修改Windows10网络优先级
category: miscellaneous
tags: [windows]
excerpt: 修改Windows10网络优先级
date: 2020-05-13 09:44:06
---

通过Powershell，使用管理员角色运行Powsershell

查看当前网络接口
```
Get-NetIPInterface
```

修改优先级，数字越低优先级越高
```
Set-NetIPInterface -InterfaceIndex 21 -InterfaceMetric 10
```