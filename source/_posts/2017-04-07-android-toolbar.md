---
title: 使用Toolbar
category: android
tags: [toolbar]
excerpt: 使用Toolbar
date: 2017-04-07 15:01:14
---
首先在布局文件中创建`toolbar`
```
<android.support.v7.widget.Toolbar
    android:id="@+id/toolbar"
    android:layout_width="match_parent"
    android:layout_height="?attr/actionBarSize"
    >
</android.support.v7.widget.Toolbar>
```

然后在Activity中设置toolbar
```
Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
setSupportActionBar(toolbar);
```


P.S. 使用`contentInset`属性移除`padding`
```
android:contentInsetStart="0dp"
android:contentInsetLeft="0dp"
app:contentInsetLeft="0dp"
app:contentInsetStart="0dp"
```