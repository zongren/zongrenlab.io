---
title: 使用avdmanager.bat（Windows下）创建Android虚拟机
category: android
tags: [terminal]
excerpt: 使用avdmanager.bat（Windows下）创建Android虚拟机
date: 2018-05-08 13:29:27
---
配置.android/respositories.cfg文件，如果不存在，请创建，下面的例子是东软Andorid镜像
```
### User Sources for Android Repository
#Thu Jun 01 14:15:58 CST 2017
disp00=Neusoft Mirror

count=1

enabled00=true

src00=http\://mirrors.neusoft.edu.cn/android/repository/sys-img/android/sys-img.xml
```

下载指定版本的系统镜像，如下所示，下载api23的x86镜像
```
./sdkmanager.bat "system-images;android-23;google_apis;x86"
```

接受license
```
./sdkmanager.bat --licenses
```

创建虚拟机
```
./avdmanager.bat create avd -n test -k "system-images;android-23;google_apis;x86" -b x86 -c 100M -d 7 -f
```