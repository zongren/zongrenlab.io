---
title: 给View添加波纹效果
category: android
tags: [ui,ripple]
date: 2017-03-22 16:57:01
excerpt: 给View添加波纹效果
---
首先创建ripple.xml文件，内容如下
```
<?xml version="1.0" encoding="utf-8"?>
<ripple xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:color="@color/lightred"
    tools:targetApi="lollipop">
    <item android:id="@android:id/mask">
        <shape android:shape="rectangle">
            <solid android:color="@color/lightred" />
        </shape>
    </item>
</ripple>
```

使用background属性设置
```
<RelativeLayout
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@drawable/ripple_effect">
    ...
    </RelativeLayout>
```