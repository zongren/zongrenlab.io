---
title: 给git设置代理
category: miscellaneous
tags: [git,proxy]
excerpt: 给git设置代理
date: 2018-12-20 09:24:47
---

给所有git设置代理
```
git config --global http.proxy socks5://127.0.0.1:1080
```

仅github走代理
```
git config --global http.https://github.com.proxy socks5://127.0.0.1:1080
```