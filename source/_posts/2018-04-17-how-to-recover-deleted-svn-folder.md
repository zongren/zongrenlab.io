---
title: 恢复删除的SVN文件夹
category: miscellaneous
tags: [svn]
excerpt: 恢复删除的SVN文件夹
date: 2018-04-17 15:03:21
---
假设我们删除了root目录下的dir文件夹
首先查看root目录的日志
```
svn log -v svn://root/
```
找到dir最后一条记录对应的版本号，例如999

然后使用以下命令恢复
```
svn cp svn://root/dir@998 svn://root/dir
```
