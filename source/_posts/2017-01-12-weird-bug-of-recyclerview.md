---
title: 记一次Bug解决经历
category: android
tags: [recyclerview]
date: 2017-01-12 16:42:11
excerpt: onCreateViewHolder调用次数过多得问题。
---

之前的布局如下所示
```
|   LinearLayout                     |
|       - orientation vertical       |
|                  |                 |
|                  |                 |
|  RecyclerViewOne | RecyclerViewTwo |
|    - width 100dp |     - width 0dp |
|                  |     - weight 1  |
|                  |                 |
```
解决运行的时候发现有卡顿出现，经过一番调式，发现`onCreateViewHolder`调用的次数太多。正常情况下，系统会检测屏幕内item的个数，如果一屏只能显示5个item，那么`onCreateViewHolder`也只会调用5次，而在这种布局下，`onCreateViewHolder`调用了`n`次，`n`为`getItemCount`返回的数值。将布局修改为下面的样子后，bug解决
```
|   RelativeLayout                                    |
|                                                     |
|                  |                                  |
|                  |                                  |
|  RecyclerViewOne |   RecyclerViewTwo                |
|    - width 100dp |     - width match_parent         |
|                  |     - toRightOf RecyclerViewOne  |
|                  |                                  |
```

分析一下，问题之所以会出现，是因为LinearLayout的方向为水平，所以系统会检测在水平方向上，屏幕需要多少个item。