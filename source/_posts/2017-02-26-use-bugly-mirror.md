---
title: 使用bugly镜像
category: android
tags: [android-studio,mirror]
date: 2017-02-26 16:05:57
excerpt: 使用腾讯bugly镜像解决android studio更新和android sdk下载太慢的问题。
---

这也算是一种中国特色了

具体使用方法见官方网页：[Android development tools mirror.](http://android-mirror.bugly.qq.com:8080/include/usage.html)

## 更新 最新官方网址
[腾讯大师兄 - 移动开发者工具镜像站点](http://dsx.bugly.qq.com/)

## 更新 官方已关闭镜像站点

## 更新 官方开了另一个镜像
https://mirrors.cloud.tencent.com/

使用方法为添加环境变量
```
export SDK_TEST_BASE_URL=https://mirrors.cloud.tencent.com/AndroidSDK/
```