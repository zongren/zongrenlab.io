---
title: 在framework中使用本土化语言文件
category: ios
tags: [localization]
date: 2016-11-17 12:55:34
excerpt: 如果你的iOS项目使用了framework，并且希望在framework中使用本土化语言文件，那么你应该注意这篇文章说的。
---
如果你企图在`framework`中直接使用`NSLocalizedString`完成本土化，你应该会失败，因为查看一下`NSLocalizedString`的定义
```
#define NSLocalizedString(key, comment) \
	    [NSBundle.mainBundle localizedStringForKey:(key) value:@"" table:nil]
```
它调用的对象是`NSBundle.mainBundle`，很明显是行不通的，解决办法是自定义宏，像下面这样
```
#define LOCALIZE(arg) [[NSBundle bundleForClass:[self class]] localizedStringForKey:(arg) value:@"" table:nil]
#define LOCALIZE_FORMAT(format,arg,...) [NSString stringWithFormat:LOCALIZE(format),arg]
```
使用起来很简单
```
LOCALIZE(@"classNameIsEmpty")
LOCALIZE_FORMAT(@"formatClassNotFound",intent.className)
```