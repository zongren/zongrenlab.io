---
title: 原码
tags:
  - development
  - binary
  - computer-science
category: development
date: 2016-09-19 11:51:46
---

维基百科链接：[Signed number representations - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Signed_number_representations#Signed_magnitude_representation)
原码也叫Singed Magnitude Representation、Sign–magnitude、Sign and Magnitude，是计算机系统用来表示有符号数的一种方式。