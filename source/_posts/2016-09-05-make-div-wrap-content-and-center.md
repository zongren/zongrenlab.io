---
title: 使div宽度自适应并且居中
date: 2016-09-05 11:51:22
category: miscellaneous
tags: [css,div,center,wrap-content]
---
如果想要某个div的宽度根据内容改变，一般有两种方法，代码如下
```
display: inline-block;
display: table;
```
其中`display: table;`比较好，因为使用`display: inline-block;`的话，你还得使用额外的负margin值来消除多余的空白。

如果一个div的宽度已经是固定的值或者根据内容改变的，那么只需要以下代码即可使该div居中
```
margin: 0 auto;
```