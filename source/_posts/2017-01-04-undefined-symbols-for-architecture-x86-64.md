---
title: Undefined symbols for architecture x86_64
category: ios
tags: [compile]
date: 2017-01-04 22:32:05
excerpt: Undefined symbols for architecture x86_64
---
在项目中导入`ZipArchive`库后，编译报错`Undefined symbols for architecture x86_64`
试过使用`lipo`命令查看framework支持的结构
```
lipo -info ZipArchive.framework/ZipArchive
```
结果显示如下
```
Architectures in the fat file: ZipArchive are: armv7 arm64 i386 x86_64
```
最后发现删掉`Other linker flags`中的`-ObjC`后，终于解决了问题
