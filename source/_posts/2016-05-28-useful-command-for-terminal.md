---
title: 常用终端命令
date: 2016-05-28 22:41:26
category: miscellaneous
tags: [osx,terminal]
---

在OSX系统下写博客很方便，命令使用起来也很方便，记录一下在`macOS`系统下经常使用的一些命令

## 隐藏一个文件
```
chflags hidden /path/of/the/file/you/want/to/hide
```

## 取消隐藏
```
chflags nohidden /path/of/the/file/you/want/to/show
``` 

## 显示所有隐藏的文件
```
defaults write com.apple.finder AppleShowAllFiles Yes && killall Finder
```   

## 不显示隐藏的文件
```
defaults write com.apple.finder AppleShowAllFiles No && killall Finder
```   

## 添加路径到`PATH`变量中
将以下代码添加到`~/.bash_profile`文件中，不存在的话可以创建一个
```
export PATH=/usr/local/mysql/bin:$PATH
```
然后执行以下命令
```
source ~/.bash_profile
```

## 添加命令快捷方式
```
alias serve_blog="cd ~/github/blog && jekyll serve --config _config.yml,_config_dev.yml"
```

## 添加可执行权限
```
chmod 755 ~/XcodeProjects/App/.git/hooks/pre-commit
```