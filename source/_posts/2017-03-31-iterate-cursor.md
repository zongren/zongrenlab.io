---
title: Cursor遍历的正确方式
category: android
tags: [android]
date: 2017-03-31 11:14:33
excerpt: Cursor遍历的正确方式
---

使用`while`遍历`cursor`的话，不要调用`cursor.moveToFirst()`，这一点很关键，如果要移动到第一行，可以使用`cursor.moveToPosition(-1)`
```
cursor.moveToPosition(-1);
while (cursor.moveToNext()) {
    ...
}
```
使用for
```
for( cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext() ) {
    ...
}
```