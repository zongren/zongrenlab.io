---
title: 给Git添加全局忽略文件
date: 2016-06-11 21:10:04
category: development
tags: [git,tip,note]
---

使用OS X开发的时候，我习惯把隐藏文件显示出来，导致在使用Git进行版本控制的时候出现了问题，Git会把每个文件夹内的`.DS_Store`文件加入到版本控制中，所以需要一条全局规则屏蔽这个文件
```
echo .DS_Store >> ~/.gitignore.global
git config --global core.excludesfile ~/.gitignore.global
```