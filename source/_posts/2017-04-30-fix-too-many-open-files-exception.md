---
title: 解决“Too many open files”异常
category: android
tags: [java,shell,process]
excerpt: 解决“Too many open files”异常
date: 2017-04-30 11:23:49
---
异常如下所示
```
java.io.IOException: Too many open files
	at java.lang.ProcessManager.exec(Native Method)
	at java.lang.ProcessManager.exec(ProcessManager.java:209)
	at java.lang.Runtime.exec(Runtime.java:174)
	at java.lang.Runtime.exec(Runtime.java:247)
	at java.lang.Runtime.exec(Runtime.java:190)
```
原代码如下所示
```
public static void executeCommand(String command){  
    Process process = null;
    try {  
        process = Runtime.getRuntime().exec(command);
    } catch(Exception exception){  
      exception.printStackTrace();  
      throw exception;  
    }
} 
```
添加以下代码
```
finally {
    if(process!=null){
        process.destroy();
    }
}
```