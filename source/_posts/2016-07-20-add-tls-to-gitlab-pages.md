---
title: 使用Let's Encrypt给gitlab Pages添加TLS支持
date: 2016-07-20 09:14:59
category: miscellaneous
tags: [blog]
---
## 介绍
之前写到过，由于`gitlab`没有屏蔽百度爬虫，并且原生支持`Hexo`博客生成工具，所以把博客从`github`和`Coding`移到`gitlab`上，后来又发现`gitlab`支持自定义域名添加`TLS`证书，需要定期（大概三个月）更新证书，现在记录一下相关操作，以免有所遗漏。

## 操作步骤
以下的相关操作需要在`Linux`环境下运行，`OS X`环境下不确保成功，而且我使用最新版的`OS X 10.11`确实没有成功，后来在`OS X`系统下使用`VirtualBox`安装`Linux Mint 18`虚拟机，成功执行运行相关命令。

### 创建博客并申请域名
在`gitlab`上创建博客非常简单，可以参考[gitlab Pages官方例子](https://gitlab.com/groups/pages)，并不仅仅支持`Jekyll`和`Hexo`，还支持其他很多种静态博客生成工具。购买域名并设置`NS`和`DNS`,使域名解析到`gitlab Pages`，具体操作请参考[官方文档说明](http://docs.gitlab.com/ee/pages/README.html)。

### 克隆`letsencrypt`
请在命令行输入以下命令
```
git clone https://github.com/letsencrypt/letsencrypt
cd letsencrypt
```

### 运行`letsencrypt-auto`
```
./letsencrypt-auto certonly -a manual -d zongren.me
```
如果需要同时添加多个域名，例如`www.zongren.me`，需要添加额外的`-d`参数，例如
```
./letsencrypt-auto certonly -a manual -d zongren.me -d www.zongren.me
```

### 添加文件
执行以上命令后会弹出提示，问你是否愿意暴露你的服务器IP，选择Yes
然后屏幕会提示以下内容，意思是该链接需要输出指定的内容才可以验证你对域名的所有权，输出的内容不能包含任何多余的字符
```
Make sure your web server displays the following content at
http://zongren.me/.well-known/acme-challenge/bvlSeyJiWzestsXGP60SHmYhzBKHtfzh4XUZ4Ts8yHk before continuing:

bvlSeyJiWzestsXGP60SHmYhzBKHtfzh4XUZ4Ts8yHk.Pd1PI7gRddQn94AtjdVO0PKecPR3vdLhSySPNCUfxJg

If you don't have HTTP server configured, you can run the following
command on the target server (as root):

mkdir -p /tmp/certbot/public_html/.well-known/acme-challenge
cd /tmp/certbot/public_html
printf "%s" bvlSeyJiWzestsXGP60SHmYhzBKHtfzh4XUZ4Ts8yHk.Pd1PI7gRddQn94AtjdVO0PKecPR3vdLhSySPNCUfxJg > .well-known/acme-challenge/bvlSeyJiWzestsXGP60SHmYhzBKHtfzh4XUZ4Ts8yHk
# run only once per server:
$(command -v python2 || command -v python2.7 || command -v python2.6) -c \
"import BaseHTTPServer, SimpleHTTPServer; \
s = BaseHTTPServer.HTTPServer(('', 80), SimpleHTTPServer.SimpleHTTPRequestHandler); \
s.serve_forever()" 
Press ENTER to continue
```

### 创建文件并上传到服务器
接下来就是要满足上述的条件，可以分别创建目录和文件，然后输入指定的内容，也可以使用每个博客生成工具的`permalink`属性，自动创建目录和文件，具体操作请参考每个博客生成工具的说明文档，在此不再赘述。本博客使用`Hexo`生成工具，操作方法如下
#### 安装插件
首先需要安装一个第三方插件`hexo-processor-static`，这个插件会把source/_static文件夹下的所有内容原封不动地复制到public文件夹中。代码如下
```
npm install hexo-processor-static --save
```
#### 创建文件夹和文件
然后在`source`文件夹下创建`_static`文件夹,并按上述路径创建文件夹和文件
#### 写上指定的内容
最后写入指定的内容，注意不包含任何多余的空格和换行等其他字符

### 验证文件内容
将上述文件上传到服务器上后，稍等片刻，确保链接可以访问，并且内容正确，然后在就可以点击回车确认了，如果提示以下内容，说明操作已经成功了，接下来就可以在`gitlab`上重新设置`https`的域名了。如果你之前输入的`-d`参数不止一个的话，需要重复2.4-2.6步。
```
IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at
   /etc/letsencrypt/live/YOURDOMAIN.org/fullchain.pem. Your cert will
   expire on 2016-07-04. To obtain a new version of the certificate in
   the future, simply run Let's Encrypt again.
 - If you like Let's Encrypt, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```
### 设置域名
#### 首先移除原来的域名
#### 重新添加原来的域名
接下来重新添加原来的域名，只不过这次要输入证书上的内容。
将`/etc/letsencrypt/live/zongren.me/fullchain.pem`的内容复制到`Certificate (PEM)`中，`/etc/letsencrypt/live/zongren.me/privkey.pem`的内容复制到`Key (PEM)`，你需要`sudo`才能读取这些文件。另外注意即使输入了```-d zongren.me```和```-d zongren.me```两个参数，也只会生成一个证书，这是正常现象，而且添加`www.zongren.me`域名时使用相同的文件。
