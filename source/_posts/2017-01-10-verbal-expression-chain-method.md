---
title: Objtive-C使用block实现流式接口
category: miscellaneous
tags: []
date: 2017-01-10 09:35:14
excerpt: 如何在Objtive-C中实现流式接口（fluent interface）。
---
首先看一下Java语言中一个典型的流式接口调用方式
{% blockquote Java https://zh.wikipedia.org/wiki/%E6%B5%81%E5%BC%8F%E6%8E%A5%E5%8F%A3#Java wikipedia %}
Author author = AUTHOR.as("author");
create.selectFrom(author)
      .where(exists(selectOne()
                   .from(BOOK)
                   .where(BOOK.STATUS.eq(BOOK_STATUS.SOLD_OUT))
                   .and(BOOK.AUTHOR_ID.eq(author.ID))));
{% endblockquote %}

它的实现非常简单，只要在每个方法中都返回`this`即可，然而在Objective-C中确不是那么容易，至少不是很好看，例如下面的代码
```
[[[[[[[Person alloc] init] getUp] wash] eatBreakfast] driveToWork] eatLaunch];
```
这种流式接口没有任何意义，可读性差，容易写错括号等。

想要实现可读性好，容易调用的流式接口，只能通过block实现了，如下所示
{% blockquote  ObjectiveCVerbalExpressions https://github.com/VerbalExpressions/ObjectiveCVerbalExpressions#testing-if-we-have-a-valid-url GitHub %}
VerbalExpressions *tester = VerEx()
.startOfLine(YES)
.then(@"http")
.maybe(@"s")
.then(@"://")
.maybe(@"www")
.anythingBut(@" ")
.endOfLine(YES);
{% endblockquote %}
