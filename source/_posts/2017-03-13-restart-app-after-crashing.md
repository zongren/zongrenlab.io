---
title: Android崩溃后重启应用
category: android
tags:
  - code
  - tip
  - fragment
excerpt: Android崩溃后重启应用
date: 2017-03-13 14:53:52
---
发生崩溃，1秒后重启应用，也可以放在主Activity里。
```
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        final PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0,
                new Intent(context, SplashActivity.class),0);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, intent);
                System.exit(2);
            }
        });
    }
}
```