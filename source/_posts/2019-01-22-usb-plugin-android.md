---
title: USB掃碼盒子和讀卡器插拔導致Activity重新創建
category: android
tags: [usb]
excerpt: USB掃碼盒子和讀卡器插拔導致Activity重新創建
date: 2019-01-22 11:17:37
---

USB掃碼盒子和讀卡器一般識別爲鍵盤，在AndroidManifest添加以下配置
```
        <activity
            android:name=".main.MainActivity"
            android:configChanges="orientation|keyboard"
```

系統會調用MainActivity的```onConfigurationChanged```方法