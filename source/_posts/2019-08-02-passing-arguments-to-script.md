---
title: 向脚本传递参数
category: miscellaneous
tags: [terminal,bash,macos]
excerpt: 向脚本传递参数
date: 2019-08-02 15:53:19
---
第一种
```
#!/bin/sh
argc="$@" 
echo "$0's argument is " $argc
x=0
# x=0 for unset variable
for arg in $argc
do
   case $x in
        "--project_path" )
          project_path=$arg ;;
        "--project_name" )
          project_name=$arg ;;
        "--workspace_name" )
          workspace_name=$arg ;;
        "--scheme_name" )
          scheme_name=$arg ;;
        "--profile" )
          profile=$arg ;;
        "--team" )
          team=$arg ;;
        "--identity" )
          identity=$arg ;;
        "--domain" )
          domain=$arg ;;
    esac
    x=$arg
done

```

第二种
```
#!/bin/bash

helpFunction()
{
   echo ""
   echo "Usage: $0 -a parameterA -b parameterB -c parameterC"
   echo -e "\t-a Description of what is parameterA"
   echo -e "\t-b Description of what is parameterB"
   echo -e "\t-c Description of what is parameterC"
   exit 1 # Exit script after printing help
}

while getopts "a:b:c:" opt
do
   case "$opt" in
      a ) parameterA="$OPTARG" ;;
      b ) parameterB="$OPTARG" ;;
      c ) parameterC="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$parameterA" ] || [ -z "$parameterB" ] || [ -z "$parameterC" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

# Begin script in case all parameters are correct
echo "$parameterA"
echo "$parameterB"
echo "$parameterC"
```

第三种
```
#!/bin/sh
while [ ! -z "$1" ];
do
 case "$1" in
     "--project_path" )
          project_path="$2" ;;
        "--project_name" )
          project_name="$2" ;;
        "--workspace_name" )
          workspace_name="$2" ;;
        "--scheme_name" )
          scheme_name="$2" ;;
        "--profile" )
          profile="$2" ;;
        "--team" )
          team="$2" ;;
        "--identity" )
          identity="$2" ;;
        "--domain" )
          domain="$2" ;;
        "--bundle" )
          bundle="$2" ;;
        "--repository" )
          repository="$2" ;;
    esac
    shift
done
```