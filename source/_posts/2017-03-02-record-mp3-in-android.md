---
title: Android录制mp3
category: android
tags: [record,tip]
date: 2017-03-02 00:06:56
excerpt: 如何录制mp3格式的音频。
---
这是来自StackOverflow上的一位网友的答案：
{% blockquote Advait S http://stackoverflow.com/a/33054794 Saving .mp3 files using MediaRecorder %}
MediaRecorder recorder = new MediaRecorder();
recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
recorder.setOutputFile(Environment.getExternalStorageDirectory()
        .getAbsolutePath() + "/myrecording.mp3");
recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
recorder.prepare();
recorder.start();
{% endblockquote %}
