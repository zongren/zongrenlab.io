---
title: Java中的延时操作
date: 2016-05-19 22:35:52
category: android
tags: [java,delay]
---
请参考以下代码
```
new android.os.Handler().postDelayed(
    new Runnable() {
        public void run() {
            Log.i("tag", "This'll run 300 milliseconds later");
        }
    }, 
300);
```
