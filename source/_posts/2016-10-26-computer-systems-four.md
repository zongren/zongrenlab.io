---
title: Computer Systems读书笔记（4）
comments: false
category: reading
tags:
  - book
  - computer
date: 2016-10-26 15:00:40
---
## 打卡日期
2016年10月26日，以下内容为`Computer Systems`的读书笔记之第`4`篇。

## 书籍链接
附上书籍链接：[Computer Systems](https://book.douban.com/subject/3023631/)

## 书籍章节
第1.7.1章，16页-20页，本章介绍了操作系统如何管理计算机硬件和软件。

## 读书笔记
## 进程
进程（process）是操作系统对正在运行的程序的抽象，计算机之所以能“同时”运行多个进程（程序），是因为操作系统执行上下文切换（context switching）完成的。具体过程如下图所示
{% raw %}
<div style="overflow-x:auto;overflow-y:hidden;width:100%;"><a href="/images/context-switching.svg" target="_blank"><img src="/images/context-switching.svg" alt="context-switching" style="max-width:unset;width:500px;"/></a></div>
{% endraw %}
## 线程
现代计算机系统中，一个进程通常包括若干个执行单元，叫做线程（thread）。进程中的线程共享当前上下文里的代码和数据。
## 虚拟内存
虚拟内存（Virtual Memory）使得进程认为它独自使用内存。Linux系统下，进程的虚拟内存区域如下图所示
{% raw %}
<div style="overflow-x:auto;overflow-y:hidden;width:100%;"><a href="/images/virtual-memory.svg" target="_blank"><img src="/images/virtual-memory.svg" alt="virtual-memory" style="max-width:unset;width:500px;"/></a></div>
{% endraw %}
地址从底向上增加，分别为
* 程序代码和数据（Program code and data）
程序代码和数据区由可执行文件初始化，大小固定。
一旦程序运行
* 堆（Heap）
通过调用C语言标准库中的`malloc`和`free`，堆的大小随之扩张和缩小。
* 共享库（Shared libraries）
* 栈（Stack）
栈和堆的大小都是可以变化的，不同的是，它在调用用户方法的时候扩张，退出（return）用户方法的时候缩小。
* 内核虚拟内存
内核虚拟内存区域是操作系统常驻内存的区域，程序无法直接读写该区域或调用该区域的方法。
## 文件
一个文件就是字节的序列，不多不少。