---
title: pod install导致无法提交代码
date: 2016-10-17 17:21:24
category: miscellaneous
tags: [gem,pod,git]
---
在向项目中添加`ZipArchive`依赖的时候，出现了无法`commit`的错误，提示以下内容
```
Pods project file should be in ASCII format, but Cocoapods converted Pods project file to XML by default. Install 'xcproj' in your $PATH via brew to fix.
```
按照说明，我安装了`xcproj`，可问题还是没有解决
```
brew install xcproj
```
后来通过指定`cocoapods`的版本为预览版，解决了这个问题
```
[sudo] gem install cocoapods --pre
```
执行之前，`pod`的版本为`1.0.1`，执行后`pod`的版本为`1.1.0.beta.2`，也可以使用`1.0.0`及之前的版本，也能解决这个问题，不过会警告说
```
[!] The version of CocoaPods used to generate the lockfile (1.0.1) is higher than the version of the current executable (1.0.0). Incompatibility issues may arise.
```
如果提示权限问题的，请参考{% post_link sudo-less-gem "gem安装权限" %}
另外指定`pod`命令的版本
```
pod _1.0.0_ install cocoapods
```