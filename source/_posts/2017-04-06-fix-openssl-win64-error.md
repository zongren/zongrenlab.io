---
title: 修复“OpenSSL-Win64”错误
category: miscellaneous
tags: [npm]
date: 2017-04-06 10:33:18
excerpt: 修复“OpenSSL-Win64”错误
---

## update

## 安装Windows-OpenSSL
在这个网站（[slproweb.com](https://slproweb.com/products/Win32OpenSSL.html)）下载并安装```Win64 OpenSSL```

## 手动创建文件夹并复制
在Github上的一个repository中下载[libeay32.lib](https://github.com/ReadyTalk/win32/blob/master/msvc/lib/libeay32.lib)文件，然后创建目录```C:\OpenSSL-Win64\lib\```，并复制该文件到文件夹中

在执行`npm install`的时候提示一下错误
```
cannot open input file 'C:\OpenSSL-Win64\lib\libeay32.lib'
```
暂时没有解决