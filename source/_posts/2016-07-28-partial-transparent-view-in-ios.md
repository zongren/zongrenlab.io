---
title: iOS部分透明视图
date: 2016-07-28 09:11:27
category: ios
tags: [tip]
---

有时候需要视图内部镂空的效果，以下代码即可轻松实现
```
- (void)drawRect:(CGRect)rect {
    [self.backgroundColor setFill];
    UIRectFill (rect);

    /*for (NSValue* holeRectValue in self.rectArray) {
        CGRect holeRect = [holeRectValue CGRectValue];
        CGRect holeRectIntersection = CGRectIntersection (holeRect, rect);
        [[UIColor clearColor] setFill];
        UIRectFill (holeRectIntersection);
    }*/

    /*for (NSValue* holeRectValue in self.rectArray) {
        CGRect holeRect = [holeRectValue CGRectValue];
        CGRect holeRectIntersection = CGRectIntersection (holeRect, rect);

        CGContextRef context = UIGraphicsGetCurrentContext ();

        if (CGRectIntersectsRect (holeRectIntersection, rect)) {
            CGContextAddEllipseInRect (context, holeRectIntersection);
            CGContextClip (context);
            CGContextClearRect (context, holeRectIntersection);
            CGContextSetFillColorWithColor (context, [UIColor clearColor].CGColor);
            CGContextFillRect (context, holeRectIntersection);
        }
    }*/

    CAShapeLayer* layer = [[CAShapeLayer alloc] init];
    CGMutablePathRef path = CGPathCreateMutable ();

    for (NSValue* rectValue in self.rectArray) {
        CGPathAddEllipseInRect (path, nil, [rectValue CGRectValue]);
    }

    CGPathAddRect (path, nil, self.bounds);
    layer.path = path;
    layer.fillRule = kCAFillRuleEvenOdd;
    self.layer.mask = layer;
}
```