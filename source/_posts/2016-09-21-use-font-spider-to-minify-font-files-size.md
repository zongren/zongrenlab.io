---
title: 使用font spider压缩字体文件大小
date: 2016-09-21 08:40:06
category: miscellaneous
tags: [blog,minify]
---
一直对博客不能使用自定义中文字体耿耿于怀，因为中文字体文件实在是太大了，根本不可能直接使用，现在找到了一款开源的nodejs工具（[font-spider](https://github.com/aui/font-spider)），能够针对博客内容压缩字体文件，这么一看还是很有用的。首先看了一下官方的文档，使用起来还是很简单的，首先是安装
```bash
npm install font-spider -g
```
然后执行`font-spider`命令
```bash
font-spider public/*.html
```
执行之后会在原字体文件夹生成新的压缩后的字体文件，覆盖之前的字体文件，并且会备份原来的字体文件到`.font-spider`文件夹内。这个工具还有一个好处就是，只需要准备好`TrueType`（`ttf`）格式的字体文件就可以了，它会自动生成`eot`，`woff`，`woff2`，`svg`格式的文件
然后更改`css`相关的字体属性，例如
```css
@font-face {
  font-family: 'source';
  src: url('../fonts/source.eot');
  src:
    url('../fonts/source.eot?#iefix') format('embedded-opentype'),
    url('../fonts/source.woff2') format('woff2'),
    url('../fonts/source.woff') format('woff'),
    url('../fonts/source.ttf') format('truetype'),
    url('../fonts/source.svg') format('svg');
  font-weight: normal;
  font-style: normal;
}
body{
  font-family: 'source';
}
```

```
fonts/KaTeX_AMS-Regular.ttf
fonts/KaTeX_Caligraphic-Bold.ttf
fonts/KaTeX_Caligraphic-Regular.ttf
fonts/KaTeX_Fraktur-Bold.ttf
fonts/KaTeX_Fraktur-Regular.ttf
fonts/KaTeX_Main-Bold.ttf
fonts/KaTeX_Main-Italic.ttf
fonts/KaTeX_Main-Regular.ttf
fonts/KaTeX_Math-Italic.ttf
fonts/KaTeX_SansSerif-Regular.ttf
fonts/KaTeX_Script-Regular.ttf
fonts/KaTeX_Size1-Regular.ttf
fonts/KaTeX_Size2-Regular.ttf
fonts/KaTeX_Size3-Regular.ttf
fonts/KaTeX_Size4-Regular.ttf
fonts/KaTeX_Typewriter-Regular.ttf
```