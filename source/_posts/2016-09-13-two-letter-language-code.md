---
title: 两个字符组成的语言代码
date: 2016-09-13 09:01:14
category: miscellaneous
tags: [language,html,code]
---
以下表格展示了在`html`元素中用作`lang`属性中的语言代码，按照语言（不是语言代码）字母顺序排序，注意这是`primary-code`，完整的语言代码定义为
```
language-code = primary-code ( "-" subcode )*
```
例如中文包括了`zh-CN`, `zh-TW`等。
详见[www.w3.org](https://www.w3.org/TR/html4/struct/dirlang.html#h-8.1.1)


语言 | 语言代码
-------- | ----
Abkhazian | AB
Afar | AA
Afrikaans | AF
Albanian | SQ
Amharic | AM
Arabic | AR
Armenian | HY
Assamese | AS
Aymara | AY
Azerbaijani | AZ
Bashkir | BA
Basque | EU
Bengali, Bangla | BN
Bhutani | DZ
Bihari | BH
Bislama | BI
Breton | BR
Bulgarian | BG
Burmese | MY
Byelorussian | BE
Cambodian | KM
Catalan | CA
Chinese | ZH
Corsican | CO
Croatian | HR
Czech | CS
Danish | DA
Dutch | NL
English, American | EN
Esperanto | EO
Estonian | ET
Faeroese | FO
Fiji | FJ
Finnish | FI
French | FR
Frisian | FY
Gaelic (Scots Gaelic) | GD
Galician | GL
Georgian | KA
German | DE
Greek | EL
Greenlandic | KL
Guarani | GN
Gujarati | GU
Hausa | HA
Hebrew | IW
Hindi | HI
Hungarian | HU
Icelandic | IS
Indonesian | IN
Interlingua | IA
Interlingue | IE
Inupiak | IK
Irish | GA
Italian | IT
Japanese | JA
Javanese | JW
Kannada | KN
Kashmiri | KS
Kazakh | KK
Kinyarwanda | RW
Kirghiz | KY
Kirundi | RN
Korean | KO
Kurdish | KU
Laothian | LO
Latin | LA
Latvian, Lettish | LV
Lingala | LN
Lithuanian | LT
Macedonian | MK
Malagasy | MG
Malay | MS
Malayalam | ML
Maltese | MT
Maori | MI
Marathi | MR
Moldavian | MO
Mongolian | MN
Nauru | NA
Nepali | NE
Norwegian | NO
Occitan | OC
Oriya | OR
Oromo, Afan | OM
Pashto, Pushto | PS
Persian | FA
Polish | PL
Portuguese | PT
Punjabi | PA
Quechua | QU
Rhaeto-Romance | RM
Romanian | RO
Russian | RU
Samoan | SM
Sangro | SG
Sanskrit | SA
Serbian | SR
Serbo-Croatian | SH
Sesotho | ST
Setswana | TN
Shona | SN
Sindhi | SD
Singhalese | SI
Siswati | SS
Slovak | SK
Slovenian | SL
Somali | SO
Spanish | ES
Sudanese | SU
Swahili | SW
Swedish | SV
Tagalog | TL
Tajik | TG
Tamil | TA
Tatar | TT
Tegulu | TE
Thai | TH
Tibetan | BO
Tigrinya | TI
Tonga | TO
Tsonga | TS
Turkish | TR
Turkmen | TK
Twi | TW
Ukrainian | UK
Urdu | UR
Uzbek | UZ
Vietnamese | VI
Volapuk | VO
Welsh | CY
Wolof | WO
Xhosa | XH
Yiddish | JI
Yoruba | YO
Zulu | ZU
