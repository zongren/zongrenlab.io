---
title: Java原始数据类型
date: 2016-09-02 15:01:21
category: miscellaneous
tags: [java,data-type,note]
---
记录一下Java中的原始数据类型

类型 | 说明
--- | ----
byte | 8位有符号整数，用补码（二补数）表示
Short | 16位有符号整数，用补码（二补数）表示
int | 32位有符号整数，用补码（二补数）表示
long | 64位有符号整数，用补码（二补数）表示
float | 32位单精度浮点数，符合IEEE 754标准
double | 64位双精度浮点数，符合IEEE 754标准
boolean | 布尔值，只包含一位数据
char | 16位Unicode值


