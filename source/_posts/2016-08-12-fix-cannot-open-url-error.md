---
title: 修复canOpenURL错误
date: 2016-08-12 16:25:01
category: ios
tags: [ios9,note]
---

在iOS9中如果要判断是否安装了QQ，需要把QQ加入白名单中，否则会提示以下错误
```
2015-09-13 15:51:10.906[2948:1118021] -canOpenURL: failed for URL: "mqq://qqapp" - error: "This app is not allowed to query for scheme mqq"
```
将应用加入白名单的方法很简单，在项目info.plist中添加以下代码
```
<key>LSApplicationQueriesSchemes</key>
<array>
    <string>mqqOpensdkSSoLogin</string>
    <string>mqzone</string>
    <string>sinaweibo</string>
    <string>alipayauth</string>
    <string>alipay</string>
    <string>safepay</string>
    <string>mqq</string>
    <string>mqqapi</string>
    <string>mqqopensdkapiV3</string>
    <string>mqqopensdkapiV2</string>
    <string>mqqapiwallet</string>
    <string>mqqwpa</string>
    <string>mqqbrowser</string>
    <string>wtloginmqq2</string>
    <string>weixin</string>
    <string>wechat</string>
    <string>uppaysdk</string>
    <string>uppaywallet</string>
    <string>uppayx1</string>
    <string>uppayx2</string>
    <string>uppayx3</string>
</array>
```
上面包含了QQ、微信、微博、支付宝、银联的scheme。