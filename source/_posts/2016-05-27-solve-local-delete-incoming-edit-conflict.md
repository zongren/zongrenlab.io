---
title: 解决本地更改文件后跟服务器端冲突
date: 2016-05-27 08:51:47
category: development
tags: [version-control,svn,tip,solve]
---
## 问题
使用SVN进行版本控制的时候经常碰到这种情况，一个人删除或者重命名一个文件，另一个人修改了这个文件，导致第一个人UPDATE的时候报错，提示`Tree conflicts:local delete, incoming edit upon update`。从字面意思上看就是说本地删除了，但是有更新需要添加到这个文件，然而找不到这个文件，所以报错

## 解决办法
遇到这种问题时，首先删除文件的人执行以下命令，即恢复删除的文件
```
touch file_deleted.type
svn revert path_of_deleted_file
svn update path_of_deleted_file
```
如果是windows下，需要执行以下命令来创建文件（也可以直接创建）
```
copy /y NUL file_deleted.type >NUL
svn revert path_of_deleted_file
svn update path_of_deleted_file
```
执行完命令后，找到删除文件的人和修改文件的人，分析一下应该怎么处理，如果这个文件确实需要删除，那就让删除文件的人执行以下命令
```
svn delete path_of_deleted_file
svn commit -m "Delete file"
```
如果不需要删除这个文件，择不需要执行上述命令

## 总结
一般在删除或者重命名文件或文件夹时，要及时做好沟通，删除后所有团队成员都不应该对这些文件或文件夹进行任何操作，防止再发生这种情况。