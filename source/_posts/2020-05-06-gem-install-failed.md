---
title: gem安装失败
category: ios
tags: [macos,gem,xcode,macos10.15]
excerpt: gem安装失败
date: 2020-05-06 16:57:52
---

Xcode 11自带了macos10.15的SDK，导致gem安装失败
```
MacBookPro15:iOSProjects zongren$ sudo gem install cocoapods --version=1.9.1
Building native extensions. This could take a while...
ERROR:  Error installing cocoapods:
	ERROR: Failed to build gem native extension.

    current directory: /Library/Ruby/Gems/2.3.0/gems/ffi-1.12.2/ext/ffi_c
/System/Library/Frameworks/Ruby.framework/Versions/2.3/usr/bin/ruby -r ./siteconf20200506-16837-dqatxt.rb extconf.rb
mkmf.rb can't find header files for ruby at /System/Library/Frameworks/Ruby.framework/Versions/2.3/usr/lib/ruby/include/ruby.h

extconf failed, exit code 1

```

通过命令```ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubyhdrdir"]'```发现确实使用了macOS10.15的ruby头文件
```
MacBookPro15:iOSProjects zongren$ ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubyhdrdir"]'
/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.15.sdk/System/Library/Frameworks/Ruby.framework/Versions/2.3/usr/include/ruby-2.3.0
```

切换成macOS10.14
```
sudo xcode-select --switch /Library/Developer/CommandLineTools
```

安装gem之后切换回macOS10.15
```
sudo xcode-select --switch /Applications/Xcode.app
```