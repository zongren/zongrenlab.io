---
title: 给popViewControllerAnimated方法变相添加完成回调
category: ios
tags: [tip]
date: 2017-01-04 11:58:32
excerpt: 给popViewControllerAnimated方法变相添加完成回调
---
使用CoreAnimation framework可以给popViewControllerAnimated方法变相添加完成回调
```
[CATransaction begin];
[CATransaction setCompletionBlock:^{
    // handle completion here
}];

[self.navigationController popViewControllerAnimated:YES];

[CATransaction commit];
```