---
title: 修复“java.lang.NoClassDefFoundError”异常
category: android
tags: [fix]
date: 2016-10-27 09:32:59
---
随着项目的进行，第三方库越来越多，这也就不可避免的遇到了[64K方法限制](https://developer.android.com/studio/build/multidex.html)，如果不解决的话，在Android5.0以下的机器就会产生下面的异常`java.lang.NoClassDefFoundError`：
```
E/AndroidRuntime: FATAL EXCEPTION: main
                  Process: com.szy.yishopcustomer, PID: 23083
                  java.lang.NoClassDefFoundError: org.greenrobot.eventbus.EventBusBuilder
                      at org.greenrobot.eventbus.EventBus.<clinit>(EventBus.java:48)
                      at com.szy.yishopcustomer.Application.CommonApplication.onCreate(CommonApplication.java:30)
                      at android.app.Instrumentation.callApplicationOnCreate(Instrumentation.java:1007)
                      at android.app.ActivityThread.handleBindApplication(ActivityThread.java:4339)
                      at android.app.ActivityThread.access$1500(ActivityThread.java:138)
                      at android.app.ActivityThread$H.handleMessage(ActivityThread.java:1259)
                      at android.os.Handler.dispatchMessage(Handler.java:102)
                      at android.os.Looper.loop(Looper.java:136)
                      at android.app.ActivityThread.main(ActivityThread.java:5014)
                      at java.lang.reflect.Method.invokeNative(Native Method)
                      at java.lang.reflect.Method.invoke(Method.java:515)
                      at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:806)
                      at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:622)
                      at dalvik.system.NativeStart.main(Native Method)
```
解决方法也很简单，只需使用[multidex](https://developer.android.com/topic/libraries/support-library/features.html#multidex)即可。
首先配置`app`（注意不是根目录下的）下的build.gradle文件
```
android {
    defaultConfig {
        multiDexEnabled true
        ...
    }
    ...
}
dependencies {
  compile 'com.android.support:multidex:1.0.1'
  ...
}
```
然后需要修改`Application`，如果你使用了自定义的Application，参考以下代码修改`CommonApplication.java`文件
```
public class CommonApplication extends MultiDexApplication {
    ...
}
```
如果你没有使用自定义的Application，修改`AndroidManifest.xml`文件
```
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="your.package.name">
    <application
        ...
        android:name="android.support.multidex.MultiDexApplication">
        ...
    </application>
</manifest>
```
