---
title: 修改UITableView头部和尾部颜色
date: 2016-07-26 17:32:15
category: ios
tags: [tip]
---
最简单最快捷的方法就是实现```tableView:willDisplayHeaderView:forSection```方法，该方法适用于`iOS6`及以上的版本中，所以应该是目前最正确的方法

```
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [UIColor blackColor];

    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];

    // Another way to set the background color
    // Note: does not preserve gradient effect of original header
    // header.contentView.backgroundColor = [UIColor blackColor];
}
```