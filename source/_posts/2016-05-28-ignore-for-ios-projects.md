---
title: iOS项目忽略文件列表
date: 2016-05-28 23:02:06
category: development
tags: [svn,git]
---
在协同开发iOS项目时，一般要忽略以下文件
    # Xcode
    #
    # gitignore contributors: remember to update Global/Xcode.gitignore, Objective-C.gitignore & Swift.gitignore

    # Build generated
    build/
    DerivedData

    # Various settings
    *.pbxuser
    !default.pbxuser
    *.mode1v3
    !default.mode1v3
    *.mode2v3
    !default.mode2v3
    *.perspectivev3
    !default.perspectivev3
    xcuserdata

    # Other
    *.xccheckout
    *.moved-aside
    *.xcuserstate
    *.xcscmblueprint

    # Obj-C/Swift specific
    *.hmap
    *.ipa

    # CocoaPods
    #
    # We recommend against adding the Pods directory to your .gitignore. However
    # you should judge for yourself, the pros and cons are mentioned at:
    # https://guides.cocoapods.org/using/using-cocoapods.html#should-i-check-the-pods-directory-into-source-control
    #
    # Pods/

    # Carthage
    #
    # Add this line if you want to avoid checking in source code from Carthage dependencies.
    # Carthage/Checkouts

    Carthage/Build

    # fastlane
    #
    # It is recommended to not store the screenshots in the git repo. Instead, use fastlane to re-generate the 
    # screenshots whenever they are needed.
    # For more information about the recommended setup visit:
    # https://github.com/fastlane/fastlane/blob/master/docs/Gitignore.md

    fastlane/report.xml
    fastlane/screenshots
    
    # OSX hidden files
    .DS_Store
    
    # Windows hidden files
    Thumbs.db
    
如果是在OSX系统下开发，还要忽略.DS_Store文件（如果开启了显示隐藏文件选项），同理Windows下也要忽略一些常见的隐藏文件，如Thumbs.db等