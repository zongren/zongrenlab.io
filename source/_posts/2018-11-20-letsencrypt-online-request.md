---
title: 使用在线工具（及macOS）获取LetsEncrypt证书
category: miscellaneous
tags: [gitlab-pages,letsencrypt]
excerpt: 使用在线工具（及macOS）获取LetsEncrypt证书
date: 2018-11-20 09:48:22
---

#更新
GitLab官方已支持自动更新https证书
{% img /images/gitlab-official-letsencrypt-support.png gitlab-official-letsencrypt-support.png %}

使用{% post_link use-gitlab-letsencrypt-to-auto-renew-gitlab-page-https %}工具自动生成及配置gitlab page证书的时候，经常遇到`JWS has invalid anti-replay nonce`错误

于是使用一个在线工具作为代替[Get HTTPS for free!](https://gethttpsforfree.com/)

使用`cat domain.key`获取private key