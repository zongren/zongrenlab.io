---
title: Java 1.8下的签名操作
category: android
tags:
  - code
  - tip
  - fragment
excerpt: Java 1.8下的签名操作
date: 2017-03-15 14:02:36
---

在Java 1.8运行环境下，`jarsigner`需要`-sigalg`和`-digestalg`这两个参数，具体命令如下所示
```
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore android.keystore app.apk android
```

P.S.
生成密钥
```
keytool -genkeypair -v -keystore android.keystore -alias android -keyalg RSA -validity 36500
```

验证密钥
```
keytool -list -v -keystore android.keystore
```

不要使用Android Studio 2.2新增签名方法，按照下图设置，详细说明见[官方文档](https://developer.android.com/about/versions/nougat/android-7.0.html#apk_signature_v2)

![v2signature](/images/v2signature.png)