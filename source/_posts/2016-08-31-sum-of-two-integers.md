---
title: Sum of Two Integers
date: 2016-08-31 16:47:36
category: leetcode
tags: [algorithm]
---
## 问题链接
[Sum of Two Integers | LeetCode OJ](https://leetcode.com/problems/sum-of-two-integers/)

## 问题描述
计算两个整数的和，但是不能使用加减号，例如给出a=1，b=2，返回3

## 解决方法
```
public class Solution {
    public int getSum(int a, int b) {
        int carry = a & b;
        int result = a ^ b;
        while(carry != 0)
        {
            int shiftedCarry = carry << 1;
            carry = result & shiftedCarry;
            result ^= shiftedCarry;
        }
        return result;
    }
}
```

