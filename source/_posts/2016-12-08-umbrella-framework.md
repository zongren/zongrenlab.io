---
title: Umbrella Framework
category: ios
date: 2016-12-08 00:32:28
excerpt: 如何有效管理iOS基础项目和项目（非程序架构）
---
## 更新
请查看{% post_link carthage-with-nested-dependencies 更好的解决方案 %}

## 说明 
首先需要说明的是，Apple并不推荐创建Umbrella Framework，原文如下：
{% blockquote %}
While it is possible to create umbrella frameworks using Xcode, doing so is unnecessary for most developers and is not recommended. Apple uses umbrella frameworks to mask some of the interdependencies between libraries in the operating system. In nearly all cases, you should be able to include your code in a single, standard framework bundle. Alternatively, if your code was sufficiently modular, you could create multiple frameworks, but in that case, the dependencies between modules would be minimal or nonexistent and should not warrant the creation of an umbrella for them.
{% endblockquote %}
Loosely translation如下
{% blockquote %}
开发者尽管能够使用Xcode创建Umbrella framework，但是没必要这么做，我们也不推荐这么做。Apple使用Umbrella framework是为了解决操作系统中的库文件互相依赖。几乎所有情况下，你都可以使用单一的framwork文件。如果你的代码模块划分得很好，还可以使用多个framework文件，但是这种情况下，模块之间的依赖性应该降到最低，并且不应该担保创建Umbrella framework。
{% endblockquote %}
问题的关键在最后一句，想象这样一个场景，公司A有若干个iOS应用需要开发，这些应用有一些通用的代码、资源和第三方库，尽管这些都可以在每个项目分别添加，但是整合起来一方便更新，二节省时间。

## 创建基础项目
首先需要创建基础项目(Base)，使用Xcode新建Cocoa Touch Framework，然后修改相关配置
* 将`Project>Base>Build Settings>Architectures>Build Active Architecture Only`设置为`NO`
* 将`Project>Base>Build Settings>Build Locations>Pre-configuration Build Products Path`设置为`$(SRCROOT)/Products`。
创建完成之后，添加代码，点击运行，确认无误之后，就可以把基础项目上传到版本控制服务器上了。
接下来就是创建Umbrella framework，首先把所有`.framework`文件拖入`Build Phases>Link Binary With Libraries`中，然后点击`Build Phases`中的加号添加`New Copy Files Phase`，`Destination`选择Frameworks，然后把其它`.framework`文件拖进来，Build成功之后你可以在Finder中查看BaseSample.framework文件夹，里面会多出一个Frameworks文件夹。

## 导入基础项目
接下来需要在各个项目导入基础项目了，例如使用`git`
```
cd ProjectSample
git submodule add https://github.com/zongren/BaseSample.git ProjectSample/External/BaseSample
```
然后在项目中右键`Add Files to "ProjectSample"`，选择`BaseSample.xcodeproj`，然后将`BaseSample.xcodeproj/Products/BaseSample.framework`拖入`Targets>ProjectSample>Embedded Binaries`中。
之所以打开`BaseSample.xcodeproj`，而不是直接使用`BaseSample.framework`，是因为开发人员更容易查看源码，并提出改进意见，如果不希望开发者看到或者修改源码，可以只引用framework文件。

如果提示找不到`<BaseSample/BaseSample.h>`文件，请修改`Build Settings>Search Paths>Framework Search Paths`，将framework所在文件夹添加进去。

## Sample
GitHub地址：[https://github.com/zongren/UmbrellaFramework](https://github.com/zongren/UmbrellaFramework)

## 常见问题

### dyld: Library not loaded
修改项目（不是Base Framework）的`Build Settings>Build Options>Always Empbed Swift Standard Libraries`属性，将其设置为YES

### Localization 失效
项目必须包含Localizable.strings和对应的语言，即使该文件为空。

### unrecognized selector