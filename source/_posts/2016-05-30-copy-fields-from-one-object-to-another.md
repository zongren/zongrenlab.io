---
title: 把一个对象的字段复制到另一个对象
date: 2016-05-30 15:38:43
category: android
tags: [java,reflection,util]
---

## 问题
项目中有两个模型，一个为GoodsModelParent，另一个叫GoodsModelChild，GoodsModelChild继承GoodsModelParent。GoodsModelChild的所有字段都和GoodsModelParent保持一致，只不过增加了一个`mChecked`字段。现在的程序流程为从服务器获取`JSON`格式的数据，解析成对应的模型，即GoodsModelParent，然而Adapter中使用的模型为GoodsModelChild，于是我想找一种比较方便的方法能把一个GoodsModeParent的对象的字段复制到GoodsModelChild中，并返回一个GoodsModelChild对象。

## 解决办法
使用Java Reflection API可以很方便地解决这个问题，估计会损失一部分性能，不过大大减少了代码量。解决方法如下所示：
```
/**
* Copy fields in source to target.
*
* @param source The source object.
* @param target The target object.
*/
public static void copyFields(Object source, Object target) {
    Field[] fieldsSource = source.getClass().getFields();
    Field[] fieldsTarget = target.getClass().getFields();

    for (Field fieldTarget : fieldsTarget) {
        for (Field fieldSource : fieldsSource) {
            if (fieldTarget.getName().equals(fieldSource.getName())) {
                try {
                    fieldTarget.set(target, fieldSource.get(source));
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}

/**
* Create a target object and copy fields in source object to it.
*
* @param source      The source object.
* @param targetClass The target object class.
* @param <T>         The target object.
* @return The target object.
*/
public static <T> T copyFields(Object source, Class<T> targetClass) {
    T targetObject = null;
    try {
        targetObject = targetClass.newInstance();
        Utils.copyFields(source, targetObject);
    } catch (IllegalAccessException e) {
        e.printStackTrace();
    } catch (InstantiationException e) {
        e.printStackTrace();
    }
    return targetObject;
}
```

使用起来很方便，如下所示：
```
//先创建对象，再使用copyFields方法
ShopInfoModel source = new ShopInfoModel();
ShopInfoModel target  = new ShopInfoModel();
Utils.copyFields(source,target);
//也可以直接生成新的对象
ShopInfoModel source = new ShopInfoModel();
ShopInfoModel target = Utils.copyFields(source, ShopInfoModel.class)
```