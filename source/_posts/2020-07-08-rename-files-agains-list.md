---
title: 使用数组重命名文件
category: miscellaneous
tags: [macos]
excerpt: rename-files-agains-list
date: 2020-07-08 16:55:10
---
```
#!/bin/bash
nameArray=(蒜泥豆角 金针三丝 拍黄瓜 小菜)
i=0
for f in *;
do 
extension="${f##*.}"
mv "$f" "${nameArray[i]]}.$extension"
i=$((i+1));
done
```