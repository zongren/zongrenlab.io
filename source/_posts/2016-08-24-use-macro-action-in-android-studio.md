---
title: Android Studio下的宏操作
date: 2016-08-24 09:41:28
category: android
tags: [android-studio]
---
如果你使用Android Studio，你可能会想要在保存的时候执行一些代码格式化操作，通过Macro Action可轻松实现这个功能。选择Edit>Macros>Start Macro Recording开始记录操作，例如我就按顺序记录了Code>Reformat Code，Code>Rearrange Code，和File>Save All，然后保存为Custom Save。最后方便起见，给这个宏设置快捷键即可实现，在保存的时候执行代码排序和格式化操作。