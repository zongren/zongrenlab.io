---
title: 处理第三方回调
date: 2016-10-17 10:54:00
category: ios
tags: [callback]
---
现在做一款应用，无论是登录、分享还是支付，都难免接入一些第三方应用，例如微信、QQ、支付宝、微博、银联等。如果接入的应用比较多（例如我们的产品），处理它们的回调就变得非常重要。而处理这些回调的思路很简单，根据`NSURL`的`host`属性判断回调的来源，然后调用相应的接口。另外也可以使用其它方式判断回调的来源，例如使用`sourceApplication`或者`options`参数。也可以不判断，而是依次调用第三方Api，例如
```
-(BOOL)handleUrl:(NSURL*)url{
    //微信支付、微信分享、微信登录
    if ([WXApi handleOpenURL:url delegate:[App getWXApiDelegate]]) {
        return YES;
    }
    //QQ登录
    else if ([TencentOAuth HandleOpenURL:url]) {
        return YES;
    }
    ...
}
#pragma mark -
```
上述代码的逻辑是，首先询问`WXApi`能不能处理这个`url`，如果`url`不能被`WXApi`处理，那么接着询问`TencentOAuth`，依次类推。
比较特殊的是`支付宝`和`银联支付`，它们的SDK并没有判断是否能够处理某个`url`的接口，所以只能通过`url.host`或者其它变量判断是否由`支付宝`和`银联支付`处理该`url`。

常见的几个应用的相关场景的`url.host`如下表所示

| 应用名称 | 使用场景 |  url.host |
| ------- | ------ | -------- |
| 支付宝 | 支付 | safepay |
| 微信 | 支付 | pay |
| 微信 | 分享 | platformId=wechat |
| 微信 | 登录 | oauth |
| 银联 | 支付 | uppayresult |
| QQ | 登录 | qzapp |
| QQ | 分享 | response_from_qq |
| 微博 | 登录 | response |
| 微博 | 分享 | response | 

最后贴一下我们的产品目前使用的代码
```
#pragma mark - 第三方应用回调
-(BOOL)handleUrl:(NSURL*)url{
    //支付宝支付
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService]
         processOrderWithPaymentResult:url
         standbyCallback:^(NSDictionary* data) {
             id<PayDelegate> delegate = [[App sharedInstance] payDelegate];
             if (delegate && [delegate respondsToSelector:@selector (onAlipayFinished:)]) {
                 [delegate onAlipayFinished:data];
             }
         }];
        return YES;
    }
    //微信支付、微信分享、微信登录
    else if ([url.host isEqualToString:@"pay"] || [url.host isEqualToString:@"oauth"] || [url.host isEqualToString:@"platformId=wechat"]) {
        return [WXApi handleOpenURL:url delegate:[App getWXApiDelegate]];
    }
    //银联支付
    else if ([url.host isEqualToString:@"uppayresult"]) {
        [[UPPaymentControl defaultControl]
         handlePaymentResult:url
         completeBlock:^(NSString* code, NSDictionary* data) {
             id<PayDelegate> delegate = [[App sharedInstance] payDelegate];
             if (delegate &&
                 [delegate respondsToSelector:@selector (onUnionPayFinished:data:)]) {
                 [delegate onUnionPayFinished:code data:data];
             }
         }];
        return YES;
    }
    //QQ登录
    else if ([url.host isEqualToString:@"qzapp"]) {
        return [TencentOAuth HandleOpenURL:url];
    }
    //QQ分享
    else if ([url.host isEqualToString:@"response_from_qq"]) {
        return [QQApiInterface handleOpenURL:url delegate:[App getQQApiInterfaceDelegate]];
    }
    //微博登录、微博分享
    else if ([url.host isEqualToString:@"response"]) {
        return [WeiboSDK handleOpenURL:url delegate:[App getWeiboSDKDelegate]];
    }
    return NO;
}

- (BOOL)application:(UIApplication*)application
            openURL:(NSURL*)url
  sourceApplication:(NSString*)sourceApplication
         annotation:(id)annotation {
   return [self handleUrl:url];
}

- (BOOL)application:(UIApplication*)application
            openURL:(NSURL*)url
            options:(NSDictionary<NSString*, id>*)options {
    return [self handleUrl:url];
}

#pragma mark -
```
其中`App`是单例对象，用来保存当前回调的处理者，即`代理`对象。如果不喜欢单例模式，还可以直接在`AppDelegate`文件中声明这些回调代理对象，并给其赋值。