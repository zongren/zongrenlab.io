---
title: 从SVN版本控制中删除文件
date: 2016-05-12 13:54:56
category: development
tags: [terminal,command,svn]
excerpt: 如何从SVN版本控制中删除一些文件。
---

有时不小心把某些文件或文件夹加入了`svn`版本控制，后来决定不需要这些文件或者文件夹在版本控制中

自然要把它们加入到`svn:ignore`或`svn:global-ignores`中，做完这些还不行，还需要手动删除这些文件

你可以删除指定的文件或文件夹

```
svn delete file_or_folder_name --keeplocal
```

`--keeplocal`means keep local，即不删除本地文件

有时需要批量删除文件或文件夹，不可能一个一个查找吧，通过以下命令可以快速执行此操作

```
find . -type d  -name '.idea' -exec svn delete {} --keep-local \;
```

这个命令删除了当前文件夹下的所有名为`.idea`的文件夹，包括自文件夹内的，并且保存本地文件，你可以把`d`改为`f`来删除文件

很明显，这是`linux`或者`osx`下的命令，如果是`windows`下，应该怎么操作呢

```
for /F "usebackq" %i in (`dir /s /b *.iml`) do svn delete %i --keep-local
```
上面的命令删除当前文件夹及自文件夹下的所有名为`*.iml`的文件

如果要删除文件夹，需要之行以下命令
```
FOR /D /r %G in ("build?") DO svn delete %G --keep-local
```
上面的命令删除当前文件夹和子文件夹下的名为**build**的文件夹