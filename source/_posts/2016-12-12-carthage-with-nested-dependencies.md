---
title: 使用Carthage解决嵌套的库依赖
category: ios
tags: [carthage]
date: 2016-12-12 14:50:45
excerpt: 如何使用Carthage解决嵌套的库依赖
---

由于某些原因，比如对常用库的封装，对资源的封装等，公司A将这些常用代码和资源打包成了一个`framework`文件（下文称之为Base.framework），这样其他项目可以更方便引用。针对这种情况，我认为Carthage是最好解决方案。使用此解决方案后，项目的结构非常清晰简洁：
```
Project
    - Project
        - AppDelegate
        ...
    - Frameworks
        - Base
        - Toast
        - MBProgressHUD
        ...
```
Base的项目结构如下
```
Base 
    - Base
    - Frameworks
        - Toast
        - MBProgressHUD
```
再对比一下使用这个方案之前，采用Umbrella Framework方案的结构：
```
Project
    - Base
        - Frameworks 
            - Toast
            - MBProgressHUD
    - Project
        - AppDelegate
        ...
    - Frameworks
        - Base
            - Toast
            - MBProgressHUD
        ...
```
不使用Umbrella Framework的话就是下面这样：
```
Project
    - Base
        - Frameworks 
            - Toast
            - MBProgressHUD
    - Project
        - AppDelegate
        ...
    - Frameworks
        - Base
        - Toast
        - MBProgressHUD
        ...
```
Carthage的具体使用方法可以查看GitHub官方主页：[Carthage](https://github.com/carthage/Carthage)，这里只说一下嵌套依赖需要注意的地方：

* Base.framework文件夹内不应该出现Frameworks文件夹和其它framework文件
* Base.framework项目不需要添加`Run Script Phases`[^1]，把该流程加在具体项目上
* 如果Base.framework引入了其它framework文件（而不是通过源码打包生成的），需要手动拖入具体项目里。

举个例子：[国外Github](https://github.com/zongren/ProjectSample.git)，[国内oschina](https://git.oschina.net/zongren/ProjectSample.git)，clone后执行`carthage update`即可查看效果。

[^1]:
选中项目target，进入`Build Phases`页，点击加号，选择`New Run Script Phase`，输入以下内容：
```
/usr/local/bin/carthage copy-frameworks
```
在下方的`Input Files`一栏中，添加所有使用`Carthage`打包的`framework`文件，例如：
```
$(SRCROOT)/Carthage/Build/iOS/BaseSample.framework
$(SRCROOT)/Carthage/Build/iOS/Toast.framework
$(SRCROOT)/Carthage/Build/iOS/MBProgressHUD.framework
```