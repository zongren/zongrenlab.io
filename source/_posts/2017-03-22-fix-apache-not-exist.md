---
title: 修复com.apache.http不存在的错误
category: android
tags: [build]
date: 2017-03-22 09:59:05
excerpt: 修复com.apache.http不存在的错误
---
在module下面的build.gradle文件中的android括号中添加这句话
```
android{
    useLibrary 'org.apache.http.legacy'
    ...
}
```

需要升级`com.android.tools.build:gradle`到`2.0`以上，才能使用`useLibrary`