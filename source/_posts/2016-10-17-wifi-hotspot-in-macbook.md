---
title: 打开macbook的wifi热点功能
date: 2016-10-17 14:10:24
category: miscellaneous
tags: [wifi]
---
macbook本身就支持分享互联网连接，所以可以很方便的作为wifi热点使用，下面介绍了如何打开macbook的wifi热点功能
## 进入`系统偏好设置`>`共享`
![macbook-hotspot-step-one](/images/macbook-hotspot-step-one.png)
## 选择`共享来源`和`共享端口`
![macbook-hotspot-step-two](/images/macbook-hotspot-step-two.png)

## 设置`Wi-Fi相关选项`
![macbook-hotspot-step-three](/images/macbook-hotspot-step-three.png)
## 启动`互联网共享`选项
![macbook-hotspot-step-four](/images/macbook-hotspot-step-four.png)
![macbook-hotspot-finish](/images/macbook-hotspot-finish.png)