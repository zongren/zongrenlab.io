---
title: 更改文件模板中的用户名称
date: 2016-05-19 11:10:08
category: miscellaneous
tags: [template,file-header,android,android-studio]
---
Android Studio默认使用系统用户名作为`${USER}`变量的值，有时候想重命名这个值，只需要在`Settings -> File and Code Templates -> Includes -> File Header`文件的头部添加`#set( $USER = "Your name" )`就可以了
