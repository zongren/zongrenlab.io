---
title: 在gitlab pages中使用staticman
date: 2016-09-23 09:05:37
category: miscellaneous
tags: [gitlab,staticman,tutorial]
---

[staticman](https://staticman.net)是非常好用的静态博客评论实现，但是可惜只支持`github`和`jekyll`，不过通过使用[github webhooks](https://developer.github.com/webhooks/)和[gitlab triggers](https://docs.gitlab.com/ee/api/build_triggers.html)功能就能实现在gitlab pages使用staticman。

## 创建staticmanapp
第一步要做的就是创建staticmanapp，按照[官方文档](https://staticman.net/docs/index.html)操作即可，也可以查看我创建好的[repository](https://github.com/zongren/comment)。

## 创建trigger
在gitlab 项目菜单选择`Triggers > Add trigger`，然后复制下面的链接
```
curl -X POST \
     -F token=TOKEN \
     -F ref=REF_NAME \
     https://gitlab.com/api/v3/projects/1400576/trigger/builds
```


## 创建webhook
点击github 项目`Settings>Webhooks>Add webhook`，`Payload URL`填写上面的链接，并添加参数，例如
```
https://gitlab.com/api/v3/projects/1400576/trigger/builds?token=5e763611ads5fb89598220414e334b&ref=master
```
注意将token替换