---
title: 开机启动应用
category: android
tags: [boot,reboot]
excerpt: 开机启动应用
date: 2018-11-20 10:32:05
---

创建BroadCastReceiver
```
public class BootActivity extends BroadcastReceiver {
  public void onReceive(Context context, Intent intent) {
    if (intent != null && intent.getAction() != null && intent.getAction().contentEquals(Intent.ACTION_BOOT_COMPLETED)) {
      Intent startActivityIntent = new Intent(context, MainActivity.class);
      startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(startActivityIntent);
    }
  }
}

```

在Manifest里添加权限
```
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
```

在Manifest里添加receiver
```
<receiver
    android:name=".main.BootActivity"
    android:enabled="true"
    android:exported="true"
    android:permission="android.permission.RECEIVE_BOOT_COMPLETED">

    <intent-filter>
        <action android:name="android.intent.action.BOOT_COMPLETED" />
        <action android:name="android.intent.action.QUICKBOOT_POWERON" />

        <category android:name="android.intent.category.DEFAULT" />
    </intent-filter>

</receiver>
```