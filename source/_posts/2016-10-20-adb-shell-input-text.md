---
title: Android模拟器输入中文
date: 2016-10-20 09:52:30
category: android
tags: [emulator,chinese,input]
---
我一直使用Android官方提供的模拟器（Android Studio），由于x86版本速度比arm版快很多，所以通常都用x86的镜像，那么输入中文就比较麻烦了，常用的一些输入法都不支持x86，只能通过其他方式输入中文。我推荐使用[senzhk/ADBKeyBoard](https://github.com/senzhk/ADBKeyBoard)。它的使用方法非常简单，输入效率也非常不错，例如
1. 输入中文
```
adb shell am broadcast -a ADB_INPUT_TEXT --es msg '测试'
```

2. 发送键盘按键  (67 = KEYCODE_DEL)
```
adb shell am broadcast -a ADB_INPUT_CODE --ei code 67
```

3. 发送键盘动作 (2 = IME_ACTION_GO)
```
adb shell am broadcast -a ADB_EDITOR_CODE --ei code 2
```

4. 发送unicode值
```
To send 😸 Cat
adb shell am broadcast -a ADB_INPUT_CHARS --eia chars '128568,32,67,97,116'
```

更加详细的安装和使用方式请参考该项目的github主页。需要说明的是以下代码不能输入中文
```
adb shell input text '测试'
```