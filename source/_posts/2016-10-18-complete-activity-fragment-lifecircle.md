---
title: 完整的Activity&Fragment生命周期
date: 2016-10-18 16:41:34
category: android
tags: [life-cycle,activity,fragment]
---
[下图](https://github.com/xxv/android-lifecycle/)详细描述了Activity和Fragment的生命周期，在[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)许可下进行分享。
{% raw %}
<div style="overflow-x:auto;overflow-y:hidden;width:100%;"><a href="/images/android-lifecycle.svg" target="_blank"><img src="/images/android-lifecycle.svg" alt="complete-android-and-fragment-lifecycle" style="max-width:unset;width:1000px;"/></a></div>
{% endraw %}