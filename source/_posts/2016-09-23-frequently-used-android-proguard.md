---
title: 常用第三方android proguard规则
date: 2016-09-23 10:34:10
category: [miscellaneous]
tags: [prguard]
---

## 信鸽
官方文档：[腾讯大数据-开发者中心](http://developer.qq.com/wiki/xg/Android%E6%8E%A5%E5%85%A5/Android%20SDK%E5%BF%AB%E9%80%9F%E6%8E%A5%E5%85%A5/Android%20SDK%E5%BF%AB%E9%80%9F%E6%8E%A5%E5%85%A5.html)
```
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep class com.tencent.android.tpush.**  {* ;}
-keep class com.tencent.mid.**  {* ;}
```

## 支付宝（alipay）
官方文档：[蚂蚁金服开放平台 - 文档中心](https://doc.open.alipay.com/docs/doc.htm?spm=a219a.7386797.0.0.9EpUti&treeId=59&articleId=103683&docType=1)，以下内容和官方文档不同。
```
-dontwarn com.alipay.**
-dontwarn HttpUtils.HttpFetcher
-dontwarn com.ta.utdid2.**
-dontwarn com.ut.device.**
-keep class com.alipay.android.app.IAlixPay{*;}
-keep class com.alipay.android.app.IAlixPay$Stub{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{ public *;}
-keep class com.alipay.sdk.app.AuthTask{ public *;}
-keep class com.alipay.mobilesecuritysdk.*
-keep class com.ut.*
```

## 银联（unionpay）
官方文档：[商家技术服务](https://open.unionpay.com/ajweb/help/faq/list?id=241&level=0&from=0&keyword=proguard)，以下内容和官方文档不同。
```
-dontwarn com.unionpay.**
-keep class com.unionpay.** { *; }
```

## 其它
还可以查看github上的开源的常用[android proguard snippets](https://github.com/krschultz/android-proguard-snippets)以及[android proguard cn](https://github.com/msdx/android-proguard-cn)