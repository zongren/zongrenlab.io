---
title: Computer Networking读书笔记（5）
comments: false
category: reading
tags:
  - book
  - network
  - computer
date: 2016-11-29 17:12:35
excerpt: 这是一篇文章Computer Networking读书笔记。
---
## 打卡日期
2016年11月29-30日，以下内容为`Computer Networking`的读书笔记之第`5`篇。

## 书籍链接
附上书籍链接：[Computer Networking](https://book.douban.com/subject/10573157/)

## 书籍章节
第1.5.1章，47页-53页

## 读书笔记

### 网络架构分层
为了减少网络架构的复杂程度，人们把网络分成了很多层，每一层都会1）执行设定好的动作，2）触发下一层的动作。分层还有其他好处，比如某一层的实现方法需要修改，只要它提供相同的服务（对于上层来说），并且仍然使用下层提供的服务，就不会对整个系统造成影响。
分层可能造成的不利影响有1）某一层的功能和低层的功能重复，2）某一层必须使用其他层的内容，打破了分层。

### 互联网协议栈模型（Internet protocol stack）
 | | 
-- | --- | ----
5 | 应用层 | Application
4 | 传输层 | Transport
3 | 网络层 | Network
2 | 链路层 | Link
1 | 物理层 | Physical

### 开放系统互联通信参考模型（OSI，Open System Interconnection Reference Model）
 | | 
-- | --- | ----
7 | 应用层 | Application Layer
6 | 表示层 | Presentation Layer
5 | 会话层 | Session Layer 
4 | 传输层 | Transport
3 | 网络层 | Network
2 | 链路层 | Link
1 | 物理层 | Physical

通常把OSI模型最上面的3层当作互联网协议栈中的应用层。

### 应用层
应用层是网络应用和它们的协议所在的地方，常见的协议有HTTP（请求并传输数据)、SMTP（传输电子邮件）、FTP（端到端传输文件）等，常见的应用有DNS（Domain Name System）。我们把这一层的数据包叫做`消息`（`message`）。

### 传输层
传输层传输应用层的`消息`，传输层有两个协议，分别为`TCP`（Transmission Control Protocol，传输控制协议）和`UDP`（User Datagram Protocol，用户数据报协议）。TCP提供面向连接的服务，这个服务包括保证应用层数据到达目的地和流控制（flow control，也就是发送、接收速度控制）。TCP会把长消息分成若干个片段（segment），还提供拥堵情况控制机制，以便消息来源能够根据网络拥堵情况调节发送速度。我们把这一层的数据包叫做`片段`（`segment`)。

### 网络层
网络层协议栈必须包含IP协议（Internet Protocol），通常还包含一些路由协议（routing protocols）。我们把这一层的数据包叫做`数据报`（`datagram`）。传输层把一个`片段`和目标地址（IP地址）发送到网络层。网络层被经常叫做IP层，反映出了IP的重要性。

### 链路层
链路层负责把数据包从一个节点移动到另一个节点。具体来说，在每一个节点上，网络层把数据报发送到链路层，链路层把数据沿着路由发送到另一个节点，并在下一个节点中将数据报取出发到网络层。常见的链路层协议包括了以太网（Ethernet），Wi-Fi，有线网络的DOCSIS（Data-Over-Cable Service Inteface Specification）。我们把这一层的数据包叫做`帧`（`frame`）。

### 物理层
物理层负责移动数据包中的每一位（bit），从一个节点到另一个节点。这一层的协议是链路相关的，并且还取决于链路的介质，例如双绞线（twisted-pair copper wire）、单模光线（single mode fiber optics）等。例如Ethernet有许多对应着物理层的协议，一个对应双绞线，一个对应同轴线缆（coaxial cabel）等。
