---
title: 更改input标签的样式
date: 2016-09-05 11:50:16
category: miscellaneous
tags: [css,input]
---
```
input[type=text]:{
    font-size: 15px
    -webkit-appearance: none
    height: 24px
    width: 120px
    padding: 0px 10px
    border: 1px solid #fff
    -webkit-border-radius: 0px
    border-radius: 0px
    color: primary-color
    outline: none
    vertical-align: bottom
}
input[type=text]:focus{
    border: 1px solid primary-color
}
input[type=submit]{
    margin-left: 5px
    -webkit-appearance: none
    height: 26px
    width: 46px
    border: 0
    background: primary-color
    color: #fff
    cursor:pointer
    -webkit-border-radius: 0px
    border-radius: 0px
    outline: none
}
```
