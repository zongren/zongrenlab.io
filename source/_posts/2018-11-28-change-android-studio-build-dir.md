---
title: 修改Android项目build文件夹
category: android
tags: [android-studio,build]
excerpt: 通过修改Android项目build文件夹位置，解决Windows下长路径导致的编译错误
date: 2018-11-28 21:55:54
---

如果项目有很多```product flavor```，导致编译后文件夹名称太长，有时会导致```Windows```系统下出现问题，可以用过修改```build```文件夹位置来解决

在根目录```build.gradle```文件下添加以下代码
```
allprojects {
    ...
    buildDir = "C:/tmp/${rootProject.name}/${project.name}"
}
```

修改后所有的```build```文件都会位于```C:/tmp```目录下，如图所示
{% img /images/change_android_build_dir.png change_android_build_dir %}