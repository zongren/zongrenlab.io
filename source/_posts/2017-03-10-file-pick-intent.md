---
title: 选取文件Intent
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 选取文件Intent
date: 2017-03-10 17:42:53
---
代码如下
```
Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
intent.setType("file/*");
```
三星设备需要以下代码
```
Intent intent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
intent.putExtra("CONTENT_TYPE", "*/*");
intent.addCategory(Intent.CATEGORY_DEFAULT);
```