---
title: 去掉UIScrollView的inset
date: 2016-07-05 14:19:15
category: ios
tags: [tip,note]
---

如果你的页面包含了UIScrollView，UITableView或者UICollectionView，有时候页面会自动添加`inset`，使用以下代码去掉这些`inset`
```
self.edgesForExtendedLayout = UIRectEdgeNone;
self.automaticallyAdjustsScrollViewInsets = NO;
```