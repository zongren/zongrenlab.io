---
title: JSONModel的使用方法（一）
date: 2016-07-05 14:32:14
category: ios
tags: [jsonmodel,note]
---

### 将`non-objc`类型的变量设置为`Optional`
如果你试图将`NSInteger`这种类型的变量添加`Optional`协议，编译器会提示以下错误
```
Invalid protocol qualifiers on non-Objc type
```
这个时候你只能重写`+ (BOOL)propertyIsOptional:(NSString*)propertyName`方法，即
```
@implementation ResponseModelCartGoodsModel
+ (BOOL)propertyIsOptional:(NSString*)propertyName {
    if([propertyName isEqualToString:@"goods_number"]){
        return YES;
    }
    return NO;
}
@end
```

### 不确定键值的字典类型的解析办法
有时候会碰到这种用id或者其他不确定的值作为key的情况，如图所示
```
{
    "goods_list":{
        "333":{
            "goods_name":"xxx",
            ...
        },
        "334"{
            "goods_name":"xxx",
            ...
        },
        ...
    }
}
```
本身我是反对这种情况的，因为这种情况下使用数组是更好的方式，不过接口返回的数据不能说改就改，这种情况在android（java）下很好解决，使用Map类型解析即可，然而在iOS下解析起来却比较麻烦，以下为解析办法
```
//首先声明一个NSString协议，用作keyType
@protocol NSString
@end
//然后是正常的模型和协议，用作objectType
@interface ResponseModelCartGoodsModel : JSONModel
...
@end
@protocol ResponseModelCartGoodsModel
@end
//解析方法
@interface ResponseModelCartShopListModel : JSONModel
...
@property (nonatomic, strong) NSDictionary<NSString, ResponseModelCartGoodsModel>* goods_list;
@end
```