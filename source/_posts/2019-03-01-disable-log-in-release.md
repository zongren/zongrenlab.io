---
title: release版本下禁用Log.d等输出
category: android
tags: [proguard]
excerpt: 使用-assumenosideeffects选项屏蔽某些方法
date: 2019-03-01 17:34:56
---

最近接入的一个第三方sdk大量调用Log方法输出日志，于是查找资料，发现可以通过[proguard](https://www.guardsquare.com/en/products/proguard/manual/examples#logging)实现

首先创建```proguard-android-optimize.txt```文件，然后添加以下内容到```proguard-rules.pro'```
```
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
    public static *** i(...);
    public static *** w(...);
}
```

最后修改build.gradle文件
```
android{
    buildTypes {
        release {
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
}
```

按照[官方文档](https://www.guardsquare.com/en/products/proguard/manual/usage#assumenosideeffects)的解释
Proguard执行optimizition的时候，检测所有调用此方法的代码，如果此方法的返回值没有用，则移除这个调用