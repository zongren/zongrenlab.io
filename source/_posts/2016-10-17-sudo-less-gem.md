---
title: gem安装权限
date: 2016-10-17 09:31:11
category: miscellaneous
tags: [gem,sudo]
---
如果执行`sudo gem install`命令的时候提示权限问题
```
sudo gem install cocoapods
ERROR:  While executing gem ... (Errno::EPERM)
    Operation not permitted - /usr/bin/pod
```
解决方法是在`.bash_profile`中写入以下内容
```
export GEM_HOME=$HOME/.gem
export PATH=$GEM_HOME/bin:$PATH
```
然后执行`source`命令
```
source .bash_profile
```
如果之前安装了该`gem`，建议先卸载之前的`gem`，然后重新安装
```
sudo gem uninstall cocoapods
gem install cocoapods
```
