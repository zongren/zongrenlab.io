---
title: 解决无法查看源码的错误
date: 2016-06-21 09:48:59
category: android
tags: [note]
---

android开发相对iOS开发，最大的优势有源码可以参考，不过有时候查看源码会出现以下错误
```
Sources for `Android API 21 Platform` not found
```
解决办法很简单，首先确保已经安装了对应版本的sdk，然后重新设置Android SDK Location即可。
