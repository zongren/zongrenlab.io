---
title: iOS下的static、extern、const
date: 2016-08-24 15:51:38
category: ios
tags: [static,const,extern]
---
## 静态（static）
**静态变量**在整个程序运行期间只会分配一次（内存），如果静态变量在方法内声明，那么它只在这个方法内可见，如果静态变量在文件中（方法外）声明，那么它在所有方法内可见。**静态方法**只在当前文件中可见，可以看作私有方法。
如果在头文件声明静态变量，那么不同源文件引用改头文件，会产生不同的静态变量，而不是一个。如果想声明一个全局变量，必须使用extern，并且**仅在一个源文件**中对其赋值。一个变量不同同时声明为static和extern，因为本质上内部链接（Internal linkage）和外部链接（External linkage）互相冲突。

## 引用（extern）
首先在头文件中声明变量：
```
// Constants.h
extern NSString * const THIS_IS_A_CONSTANT;
```
最后，在Constants.m中通过赋值定义常量：
```
// Constants.m
NSString * const THIS_IS_A_CONSTANT = @"THIS_IS_A_CONSTANT";
```

## 常量（const）
const将后面的符号声明为常量，例如
```
const NSString * MY_CONSTANT = @"CONSTANT";
//等同于
NSString const * MY_CONSTANT = @"CONSTATN";
//以下代码不会报错
MY_CONSTANT = @"NOT_CONSTANT";
```
将一个指针设置为常量，但是指针指向的内容是可变的，也就是MY_CONSTANT的值是可变的。而
```
NSString * const MY_CONSTANT = @"CONSTANT";
//以下代码会报错
MY_CONSTANT = @"NOT_CONSTANT";
```
将MY_CONSTANT设置为常量，MY_CONSTANT的值不能改变。


