---
title: Gradle项目使用多个settings文件
category: miscellaneous
tags: [gradle,android-studio]
excerpt: 如何在Gradle-Based项目中，使用多个settings.gradle文件，方便本地调试，并且忽略本地module
date: 2019-07-24 09:44:23
---
在settings.gradle中添加以下代码
```
Properties properties = new Properties()
properties.load(new FileInputStream(new File('local.properties')))
def settings = properties.getProperty("settings")
if(settings == null){
    settings = 'default_settings';
}

apply from: "${settings}.gradle"
```

创建默认default_settings.gradle文件
```
//default_settings.gradle
include ':app', ':library'
```

指定其它gradle文件
```
//local.properties
settings=other_settings
```

例如
```
//other_settings.gradle
include ':app', ':library', 'local_module1', 'local_module2', 'local_module3', 'local_module4'
```