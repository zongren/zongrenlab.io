---
title: 为什么ViewGroup类的onDraw方法不被调用       
date: 2016-06-23 14:29:48
category: android
tags: [note,tip]
---

简单来说就是ViewGroup默认调用`setWillNotDraw(true)`方法，禁用了onDraw方法。开启也很简单，在构造函数中调用`setWillNotDraw(false)`即可。