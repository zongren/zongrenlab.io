---
title: JUnit测试分层结构
category: android
tags: [test,junit]
excerpt: JUnit测试分层结构
date: 2019-02-21 13:43:42
---

使用[hierarchicalcontextrunner](https://github.com/bechte/junit-hierarchicalcontextrunner)库可以很方便的将junit测试分成若干层，每层都可以设置独立的setup和teardown方法

首先引入库，在gradle中添加
```
testImplementation 'de.bechte.junit:junit-hierarchicalcontextrunner:4.12.1'
```

在junit测试代码上添加@RunWith
```
@RunWith(HierarchicalContextRunner.class)
public class SomeTest {
}
```

创建内部类
```
@RunWith(HierarchicalContextRunner.class)
public class SomeTest {
    public class SubTest{
        @Test
        public void test(){
            
        }
    }
}
```