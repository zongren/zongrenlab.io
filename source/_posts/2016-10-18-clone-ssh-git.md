---
title: 使用SSH链接克隆GIT仓库
date: 2016-10-18 13:35:43
category: miscellaneous
tags: [git,ssh]
---
如果你想使用`ssh`链接克隆`git`仓库，分为以下几步
## 创建SSH密钥对
### 打开终端
### 输入以下指令
```
ssh-keygen -t rsa -b 4096 -C "zongren@live.com"
Enter a file in which to save the key (/Users/zongren/.ssh/id_rsa): //回车
Enter passphrase (empty for no passphrase): //输入密钥
Enter same passphrase again: //再次输入密钥
```
## 修改github设置
打开`Settings`>`SSH and GPG keys`，点击`New SSH key`添加新的SSH密钥，输入title和之前生成的`/Users/zongren/.ssh/id_rsa.pub`文件的内容。
## 测试
任意克隆一个git仓库，需要输入密钥
```
git clone git@github.com:zongren/hexo-theme-nojs.git ~/hexo-theme-nojs
```
