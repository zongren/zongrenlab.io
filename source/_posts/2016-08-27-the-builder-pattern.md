---
title: 生成器模式
date: 2016-08-27 13:46:34
category: development
tags: [design-pattern]
---
生成器模式（英：Builder Pattern）是一种设计模式，又名：建造模式，是一种对象构建模式（或创建性模式、Creation Pattern)。它可以将复杂对象的建造过程抽象出来（抽象类别），使这个抽象过程的不同实现方法可以构造出不同表现（属性）的对象。
在以下情况使用生成器模式：
* 当创建复杂对象的算法应该独立于该对象的组成部分以及它们的装配方式时；
* 当构造过程必须允许被构造的对象有不同的表示时。


以Java语言为例，简单说明如何使用生成器模式。首先有一个类叫做Cake，用来创建Cake对象。
```
Class Cake{
    int size;//必要属性
    int sugar;//可选属性
    int salt;//可选属性
    ...
}
```
这个类有三个属性，其中有一个是必需的，其它的为可选属性，如果全部用构造函数传入这些参数，那么一共需要以下几个函数
```
public Cake(int size);
public Cake(int size,int sugar);
public Cake(int size,int salt);
public Cake(int size,int sugar,int salt);
//public Cake(int size,int salt,int sugar);
```
这么写有几个缺点，一是构造函数数量太多，二是构造函数的参数顺序不容易记住。现在给Cake类添加生成器
```
Class Cake{
    int size;//必要属性
    int sugar;//可选属性
    int salt;//可选属性
    ...

    public Cake(Builder builder){
        this.size = builder.size;
        this.sugar = builder.sugar;
        this.salt = builder.salt;
    }

    public static Class Builder{
        int size;//必要属性
        int sugar;//可选属性
        int salt;//可选属性
        ...
        public Builder(int size){
            this.size = size;
            this.sugar = 0;
            this.salt = 0;
        }

        public Builder addSugar(int sugar){
            this.sugar += sugar;
            return this;
        }

        public Builder addSalt(int salt){
            this.salt += salt;
            return this;
        }

        public Cake makeCake(){
            return new Cake(this);
        }
    }
}
```
使用方法如下
```
Cake cake = new Cake.Builder(12)
                    .addSalt()
                    .addSugar()
                    .addSalt()
                    .addSugar()
                    .makeCake();
```
生成器模式适用于属性较多的类而且需要一次性传入构造函数。