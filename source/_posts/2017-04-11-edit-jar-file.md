---
title: 编辑jar文件
category: miscellaneous
tags: [java]
excerpt: 编辑jar文件
date: 2017-04-11 10:25:05
---
jar文件就是zip压缩文件，编辑jar文件也就是编辑压缩包里的class文件，而class文件是二进制文件，需要类似[classeditor](http://classeditor.sourceforge.net/)的工具进行编辑。

除了编辑class文件，你还可以使用[jd-gui](https://github.com/java-decompiler/jd-gui)反编译jar文件，在项目中创建好对应的包，然后复制其中的文件即可。