---
title: 卸载Office2016的密钥
date: 2016-08-11 21:42:21
category: miscellaneous
tags: [office]
---
如何更改Office2016的key呢，之前在卸载程序的地方点击更改，然后输入新的密钥即可，现在点击更改会弹出修复窗口，这时需要通过脚本卸载之前安装的密钥，首先需要查看安装的密钥的最后5位，在命令行执行以下命令
```
cscript "C:\Program Files\Microsoft Office\Office16\ospp.vbs" /dstatus
```
命令行会输出当前密钥的相关信息，记住密钥的最后5位
然后执行删除密钥的命令
```
cd C:\Program Files\Microsoft Office\Office16
cscript ospp.vbs /unpkey:XXXXX
```
注意将XXXXX替换为密钥的最后5位

