---
title: 有用的宏
date: 2016-07-24 08:20:49
category: ios
tags: [objective-c,marco]
---

记录一下在开发中经常用到的宏定义，有方法，有变量
```
//
//  Util.h
//
//  Functionality macros
//  Created by 宗仁 on 16/7/24.
//

#ifndef Util_h
#define Util_h

#pragma mark View frame
#define WIDTH(view) view.frame.size.width
#define HEIGHT(view) view.frame.size.height
#define X(view) view.frame.origin.x
#define Y(view) view.frame.origin.y
#define LEFT(view) view.frame.origin.x
#define TOP(view) view.frame.origin.y
#define BOTTOM(view) (view.frame.origin.y + view.frame.size.height)
#define RIGHT(view) (view.frame.origin.x + view.frame.size.width)
#pragma mark -

#pragma mark Log

#ifdef DEBUG
#define DLOG(fmt, ...) NSLog ((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define DLOG(...)
#endif

// ALOG always displays output regardless of the DEBUG setting
#define ALOG(fmt, ...) NSLog ((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#pragma mark -

#pragma mark Color
#define RGB(r, g, b) [UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:1.0]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:a]
#define HEXCOLOR(c)                                  \
    [UIColor colorWithRed:((c >> 16) & 0xFF) / 255.0 \
                    green:((c >> 8) & 0xFF) / 255.0  \
                     blue:(c & 0xFF) / 255.0         \
                    alpha:1.0];
#pragma mark -

#pragma mark System Version

#define SYSTEM_VERSION_EQUAL_TO(v) \
    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v) \
    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) \
    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) \
    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) \
    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#pragma mark -

#pragma mark CGRect
#define RECT_X(f) f.origin.x
#define RECT_Y(f) f.origin.y
#define RECT_WIDTH(f) f.size.width
#define RECT_HEIGHT(f) f.size.height
#define RECT_SET_W(f, w) CGRectMake (RECT_X (f), RECT_Y (f), w, RECT_HEIGHT (f))
#define RECT_SET_H(f, h) CGRectMake (RECT_X (f), RECT_Y (f), RECT_WIDTH (f), h)
#define RECT_SET_X(f, x) CGRectMake (x, RECT_Y (f), RECT_WIDTH (f), RECT_HEIGHT (f))
#define RECT_SET_Y(f, y) CGRectMake (RECT_X (f), y, RECT_WIDTH (f), RECT_HEIGHT (f))
#define RECT_SET_SIZE(f, w, h) CGRectMake (RECT_X (f), RECT_Y (f), w, h)
#define RECT_SET_ORIGIN(f, x, y) CGRectMake (x, y, RECT_WIDTH (f), RECT_HEIGHT (f))
#pragma mark -

#endif /* Util_h */
```