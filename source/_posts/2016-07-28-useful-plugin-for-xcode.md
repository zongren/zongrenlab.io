---
title: Xcode插件
date: 2016-07-28 17:45:59
category: development
tags: [xcode,plugin]
---

记录一下在平时开发时经常用的一些Xcode插件，所有插件均通过[Alcatraz](http://alcatraz.io/)安装，排名不分先后。

* [AMLocalizedStringBuilder](https://github.com/MellongLau/AMLocalizedStringBuilder-Xcode-Plugin)
这款插件能根据Localizable.strings快速生成语言文件（模型），使用起来非常方便。

* [AMMethod2Implement](https://github.com/MellongLau/AMMethod2Implement)
方法快速声明以及实现。

* [Xcode-CComment](https://github.com/flexih/Xcode-CComment)
提供块状注释。

* [ClangFormat-Xcode](https://github.com/travisjeffery/ClangFormat-Xcode)
支持设置保存自动格式化，团队协作必备。

* [KSImageNamed-Xcode](https://github.com/ksuther/KSImageNamed-Xcode)
图片资源较多的时候，这款插件非常有用，根据名称预览图片

* [VVDocumenter-Xcode](https://github.com/onevcat/VVDocumenter-Xcode)
快速生成JavaDoc格式的注释

* [Xcode_copy_line](https://github.com/mthiesen/Xcode_copy_line)
复制粘贴行，必备。

* [FuzzyAutocompletePlugin](https://github.com/FuzzyAutocomplete/FuzzyAutocompletePlugin)
代码自动完成增强版，适合我这种健忘的人。

* [HOStringSense-for-Xcode](https://github.com/holtwick/HOStringSense-for-Xcode)
字符串提示工具。

* [Auto-Importer](https://github.com/lucholaf/Auto-Importer-for-Xcode)
自动导入头文件。

* [ColorSense](https://github.com/omz/ColorSense-for-Xcode)
颜色增强工具。