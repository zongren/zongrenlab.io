---
title: 使用Java 8 Stream api 取出一组对象的某个属性
category: android
tags: [java, stream]
excerpt: 使用Java 8 Stream api 取出一组对象的某个属性，并返回List或Array对象
date: 2018-05-07 16:15:18
---
加入我想从一个包含Person对象的List中取出所有人的性命，并返回List<String>对象
```
personList.stream().map(Person::getName).collect(Collectors.toList());

personList.stream().map(Person::getName).toArray(String[]::new);

```