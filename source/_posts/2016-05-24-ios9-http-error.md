---
title: iOS9网络请求错误
date: 2016-05-24 20:29
category: ios
tags: [network,http,tip,note]
---
iOS9引入了新特性App Transport Security (ATS)，新特性要求App内访问的网络必须使用HTTPS协议。临时解决办法为，在对应的target的info.plist文件中添加以下字段：
* 在Info.plist中添加NSAppTransportSecurity类型Dictionary。
* 在NSAppTransportSecurity下添加NSAllowsArbitraryLoads类型Boolean,值设为YES