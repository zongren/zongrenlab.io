---
title: 重写nojs主题
date: 2016-09-19 10:34:54
category: miscellaneous
tags: [nojs,css,html]
---
最近重写了nojs主题，移除了hexo-theme-light相关的代码，重新布局，并且兼容了ie7（搜索功能涉及到跨域访问问题，暂时不解决）。现在整个页面分为三部分，分别为content、footer、cd-top，其中content包括导航栏和页面内容。页面的宽度限制为900像素及以下，在手机上的效果算是可以接受吧。限制宽度的主要原因是保证在宽屏上的显示效果。除了视觉上的修改，还对页面结构做了优化，尽量减少了标签嵌套的层数。