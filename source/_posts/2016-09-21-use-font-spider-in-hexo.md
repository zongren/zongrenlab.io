---
title: 在hexo中使用fonts-spider
date: 2016-09-21 16:07:07
category: miscellaneous
tags: [hexo,font-spider]
---
之前的{% post_link use-font-spider-to-minify-font-files-size 文章 %}讲了如何使用`font-spider`压缩字体文件，然而在hexo中使用font-spider会稍微复杂一些，在hexo博客系统中使用font-spider的正确流程为
1. hexo clean
2. hexo generate
3. font-spider public/*.html
4. hexo deploy

其中第3步并不是简单的一条命令，还需要进行额外的处理，因为在html文件中，css的链接形式一般为
```html
<link rel="stylesheet" href="/css/style.css" media="screen" type="text/css">
```
font-spider会把`/css/style.css`识别为`本地绝对路径`，导致执行失败，我已经在github上向作者提问，暂时没有回复，目前能想到的解决办法就是，写一个脚本，将主题中的`/css/style.css`更改为正确的本地绝对路径，例如`/Users/zongren/blog/public/css/style.css`，注意是`hexo generate`之后的绝对路径，一般为`public`目录，然后再执行`font-spider`命令就可以了，其中还涉及到字体文件的拷贝和备份，详情见以下代码 

```bash
#!/bin/sh
cd /Users/zongren/blog

#replace css files path
sed -i -e 's/"\/css\/katex.css"/"\/Users\/zongren\/blog\/public\/css\/katex.css"/g' _config.yml
sed -i -e 's/"\/css\//"\/Users\/zongren\/blog\/public\/css\//g' themes/nojs/layout/_partial/head.ejs
cat _config.yml
cat themes/nojs/layout/_partial/head.ejs

hexo clean
hexo generate
rm -rf public/fonts/*
cp -R themes/nojs/source/_fonts/. public/fonts/
font-spider public/*.html --debug
rm -rf themes/nojs/source/fonts/*
cp -R public/fonts/. themes/nojs/source/fonts/
rm -rf themes/nojs/source/fonts/.font-spider

#recover css files path
mv _config.yml-e _config.yml
mv themes/nojs/layout/_partial/head.ejs-e themes/nojs/layout/_partial/head.ejs
cat _config.yml
cat themes/nojs/layout/_partial/head.ejs

git add .
git commit -m 'Update'
git pull
git push
```

添加可执行权限后，还可以将它作为`pre-commit`钩子，详见{% post_link use-git-hooks 使用git钩子 %}，不过需要删除最后几行代码。