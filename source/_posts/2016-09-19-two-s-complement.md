---
title: 补码
tags:
  - development
  - binary
  - computer-science
category: development
date: 2016-09-19 11:51:31
---

维基百科链接：[Two's complement - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Two%27s_complement)
补码，也叫二补数、Two's complement，是计算机系统用来表示有符号数的一种方式。