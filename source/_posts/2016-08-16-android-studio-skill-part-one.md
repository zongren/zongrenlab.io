---
title: Android Studio实用技巧（一）
date: 2016-08-16 13:52:43
category: android
tags: [android-studio,android,tip]
---
以下内容为在实际开发中经常使用的Android Studio快捷键或者小技巧，熟练掌握以下内容能够极大地提升在Android Studio上的开发效率。除非特殊说明，在OS X下用COMMAND代替CTRL，用OPTION代替ALT，Keymap使用默认的`Mac OS X 10.5+`

## 快捷键
首先介绍一下各种实用的快捷键

### 文件结构（File Structure）
**CTRL+F12**
显示当前文件的结构（变量和方法），并且支持搜索


### 调用层级（Call Hierarchy）
**CTRL+ALT+H**
`OS X`下的快捷键为`CTRL+OPTION+H`，查看方法的调用层级

### 快速查看定义（Quick Definition Lookup）
**CTRL+SHIFT+I**
`OS X`下的快捷键为`OPTION+SPACE`，在当前文件中快速查看其他类中的方法的实现

### 书签（Bookmark）
**F11**
`OS X`下的快捷键为`F3`，将当前位置添加到书签中或者从书签中移除。
**SHIFT+F11**
`OS X`下的快捷键为`COMMAND+F3`，显示所有书签

### 查找功能（Find Actions）
**CTRL+SHIFT+A**
关键字查找菜单或动作（Action），例如输入想要Clean项目，输入clean即可

### 移动行（Move Lines）
**ALT+SHIFT+UP/DOWN**
移动当前行

### 编辑行（Edit Lines）
**CTRL+Y**
删除（不会复制），`OS X`下的快捷键为`COMMAND+DELETE`
**CTRL+X**
删除并复制（剪切）
**CTRL+D**
立即复制并粘贴

### 版本控制弹窗（VCS Operations Popup）
**ALT+`**
`OS X`下的快捷键为`CTRL+V`，弹出常用版本控制命令

### 隐藏所有工具栏（Hide All Tool Windows）
**CTRL+SHIFT+F12**
关闭或恢复所有工具栏，只会恢复通过这个命令关闭的工具栏。  

### 参数信息（Parameter Info）
**CTRL+P**
显示方法所需的参数

### 重命名（Rename）
**SHIFT+F6**
对变量，方法，文件等重命名  

## 给断点添加条件
通过右键断点，可以对一个断点加入条件。只有当满足条件时，才会进入到断点中

## 调试指定进程
点击`Attach Debugger To Android Process`可以调试指定的进程

## 分析变量赋值记录
`Find Actions`输入”Analyze Data Flow to Here”，可以查看某个变量某个参数的赋值纪录

## 使用Enter和Tab选择代码提示的区别
使用Enter选择代码提示，不会清除之后的代码，而使用Tab会清空之后的代码

## 列编辑
按住`ALT`并选择多行，就可以同时修改多行的代码