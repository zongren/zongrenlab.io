---
title: 执行命令并关闭
category: miscellaneous
tags: [macos,terminal,shell]
excerpt: 执行命令并关闭
date: 2020-03-06 14:14:44
---
假设有个命令在执行完毕后不会自动结束，使用```nohup```
```
 #!/bin/bash
nohup scrcpy &
```

见：[nohup](https://ss64.com/osx/nohup.html)