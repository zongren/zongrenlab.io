---
title: 在Hexo博客中画图
date: 2016-08-31 11:57:48
category: [development]
tags: [diagram,uml]
---
由于这个博客主要是记录软件开发过程遇到的问题和解决办法，偶尔会遇到画流程图和时序图的需求，现在找到一个非常实用的插件[hexo-tag-plantuml](https://github.com/oohcoder/hexo-tag-plantuml)能够实现这个功能，本质上是采用服务器端生成svn、png图片的方式实现，并且该网站不支持https，PlantUML支持以下几种

* 时序图
* 用例图
* 类图
* 活动图
* 组件图
* 状态图
* 对象图
* 部署图
* 线框图形界面接口

时序图演示
```
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: another authentication Response
```