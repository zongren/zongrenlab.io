---
title: Computer Networking读书笔记（4）
comments: false
category: reading
tags:
  - book
  - network
  - computer
date: 2016-10-26 15:00:28
---
## 打卡日期
2016年10月26日，以下内容为`Computer Networking`的读书笔记之第`4`篇。

## 书籍链接
附上书籍链接：[Computer Networking](https://book.douban.com/subject/10573157/)

## 书籍章节
第1.4.2、1.4.3、1.4.4章，39页-46页，这几个章节继续介绍网络系统中的延迟现象。

## 读书笔记

### 排队延迟和丢包现象
节点延迟（nodal delay）中最复杂，最有趣的就是排队延迟（queueing delay）。举例说明排队延迟，10个数据包同时到达一个节点，那么第一个出去的数据包没有任何排队延迟，最后一个出去的数据包有最大的排队延迟（等待前面9个数据包）。
### 交通强度（traffic intensity）
假设所有数据包的长度都是`L`bits，数据包到达队列的速率为`a`packets/sec，传输速率为`R`bits/sec，那么此时的交通强度为`La/R`，如果La/R>1的话，那么队列将无限增长。
![average-queuing-delay](/images/average-queuing-delay.svg)
### 丢包现象
丢包现象（packet loss）是指，一个数据包到达了节点，发现队列已经满了，路由器不得不丢弃这个数据包。被丢弃的数据包会被重新发送。
### 端到端延迟
假设从一端到另一端一共有`N-1`个路由器，那么端到端(end-to-end)的延迟为
![computer-networking-four-1](/images/computer-networking-four-1.png)
传输延迟为
![computer-networking-four-2](/images/computer-networking-four-2.png)
其中`L`是数据包长度，`R`为线路传输速率。
### traceroute程序
我们可以使用`traceroute`程序查看从本机另一端的节点以及延迟
```
zongrendeMacbookPro15:~ zongren$ traceroute zongren.me
traceroute: Warning: zongren.me has multiple addresses; using 104.27.172.21
traceroute to zongren.me (104.27.172.21), 64 hops max, 52 byte packets
 1  156.93.223.222.broad.qh.he.dynamic.163data.com.cn (222.223.93.156)  0.624 ms  0.392 ms  0.333 ms
 2  1.93.223.222.broad.qh.he.dynamic.163data.com.cn (222.223.93.1)  1.891 ms  1.321 ms  1.117 ms
 3  27.129.42.45 (27.129.42.45)  0.842 ms  0.861 ms
    27.129.42.5 (27.129.42.5)  0.931 ms
 4  27.129.41.29 (27.129.41.29)  4.519 ms  7.227 ms
    27.129.41.13 (27.129.41.13)  6.399 ms
 5  202.97.25.93 (202.97.25.93)  13.400 ms
    202.97.80.101 (202.97.80.101)  8.971 ms
    202.97.25.89 (202.97.25.89)  10.630 ms
 6  202.97.53.110 (202.97.53.110)  17.820 ms
    202.97.53.94 (202.97.53.94)  22.961 ms  26.003 ms
 7  202.97.58.98 (202.97.58.98)  15.991 ms  14.183 ms  15.913 ms
 8  202.97.52.182 (202.97.52.182)  224.938 ms  224.861 ms  224.566 ms
 9  202.97.50.78 (202.97.50.78)  197.840 ms  200.955 ms  203.388 ms
10  218.30.54.62 (218.30.54.62)  203.460 ms  202.814 ms  204.486 ms
11  xe-10-3-0.cr0-sjc1.ip4.gtt.net (89.149.182.125)  213.127 ms  210.990 ms
    xe-5-3-3.cr0-sjc1.ip4.gtt.net (89.149.185.149)  214.457 ms
12  as1421.ge-0-3-1-520.cr1.sfo1.us.as4436.gtt.net (69.22.130.242)  240.800 ms  245.580 ms  244.368 ms
13  104.27.172.21 (104.27.172.21)  216.472 ms  213.007 ms  212.452 ms
```
还可以使用可视化的工具，[PingPlotter](https://www.pingplotter.com/)来完成

### 吞吐量（throughput）
如下图所示，两台终端之间仅有一台路由器
![throughput](/images/throughput.svg)
吞吐量为`min{Rc, Rs}`，即两者中的较小的那个，也就是所谓的瓶颈（bottleneck），如果有`N-1`个路由器，那么吞吐量为` min{R1, R2,..., RN}`
