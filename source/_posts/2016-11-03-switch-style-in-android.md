---
title: 修改Switch样式
category: android
tags: [switch]
date: 2016-11-03 15:59:40
excerpt: 如何修改Android自带的Switch控件的样式。
---
第一步，创建Switch主题
```
<style name="Switch" parent="Theme.AppCompat.Light">
    <!-- active thumb & track color (30% transparency) -->
    <item name="colorControlActivated">#46bdbf</item>

    <!-- inactive thumb color -->
    <item name="colorSwitchThumbNormal">#f1f1f1
    </item>

    <!-- inactive track color (30% transparency) -->
    <item name="android:colorForeground">#42221f1f
    </item>
</style>
```
在xml文件中使用这个主题
```
<Switch
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:theme="@style/Switch"
    />
```