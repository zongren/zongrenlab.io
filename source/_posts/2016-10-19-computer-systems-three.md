---
title: Computer Systems读书笔记（3）
category: reading
tags:
  - book
  - computer
date: 2016-10-19 14:11:18
comments: false
---
## 打卡日期
2016年10月19日，以下内容为`Computer Systems`的读书笔记之第`3`篇。

## 书籍链接
附上书籍链接：[Computer Systems](https://book.douban.com/subject/3023631/)

## 书籍章节
第1.7章，14页-15页

## 读书笔记
操作系统管理计算机硬件，例如处理器，内存，磁盘等。
### 计算机系统层级
![computer-systems-layer-view](/images/computer-systems-layer-view.svg)
### 操作系统提供的抽象
![computer-systems-abstraction](/images/computer-systems-abstraction.svg)