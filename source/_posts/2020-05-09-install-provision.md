---
title: 使用脚本安装provision文件
category: ios
tags: [dev-op]
excerpt: 使用脚本安装provision文件
date: 2020-05-09 16:36:39
---
创建uuid

```
#!/bin/bash -euo pipefail

if [ ! -f "${1}" ]
then
  echo "Usage: $0 <path/to/mobileprovision/file>" 1>&2
  exit 1
fi

UUID=$( grep --text --after-context=1 UUID "${1}" | grep --ignore-case --only-matching "[-A-Z0-9]\{36\}" )
if [ -z "${UUID}" ]
then
  echo "Invalid mobileprovision file: ${1}" 1>&2
  exit 2
else
  echo "${UUID}"
fi

```

安装provision文件
```
#!/bin/bash -euo pipefail

BASH_SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$BASH_SOURCE_DIR"

# by default bash passes the glob characters if nothing matched the glob
# disable that
# http://stackoverflow.com/a/18887210/9636
shopt -s nullglob
# this creates a proper bash array, which we need since our profiles
# have funny characters in them
MOBILE_PROVISIONS=(*.mobileprovision)
# re-enable default nullglob behavior
shopt -u nullglob

# On a brand new machine that has never run any app on a development device
# the ~/Library/MobileDevice/"Provisioning Profiles" directory doesn't exist
mkdir -p ~/Library/MobileDevice/"Provisioning Profiles"

for mobileprovision in "${MOBILE_PROVISIONS[@]}"
do
  uuid=$( ./uuid-from-mobileprovision.bash "${mobileprovision}" )
  cp "${mobileprovision}" ~/Library/MobileDevice/"Provisioning Profiles"/"${uuid}".mobileprovision
done

```