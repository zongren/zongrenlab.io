---
title: Android代码片段
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 一个Android代码片段，尽量保证clean&no-hack
date: 2017-03-07 16:25:28
---

隐藏导航栏搜索试图（搜索框）
```
public void hideSearchView(SearchView searchView){
    if (!searchView.isIconified()) {
        searchView.setIconified(true);
    }
}
```

如果`showAsAction`是`collapseActionView`，使用以下方法隐藏`SearchView`
```
public void hideSearchView(MenuItem menuItem) {
    if (menuItem != null && menuItem.isActionViewExpanded()) {
        menuItem.collapseActionView();
    }
}
```