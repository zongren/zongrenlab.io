---
title: 在UIImage中使用AppIcon
date: 2016-09-01 11:08:16
category: ios
tags: [uiimage,app-icon]
---
由于App编译后，AppIcon存在于App Bundle中，而不是Asset Catalog中，而且名字转变为`AppIcon{size}.png`的格式，例如将图标资源文件命名为`CustomerAppIcon`，那么生成的图片为
```
CustomerAppIcon29x29@2x.png
CustomerAppIcon29x29@3x.png
CustomerAppIcon40x40@2x.png
CustomerAppIcon40x40@3x.png
CustomerAppIcon60x60@2x.png
CustomerAppIcon60x60@3x.png
```
所以想要使用AppIcon创建UIImage的话，需要使用以下代码
```
UIImage *appIcon = [UIImage imageNamed:@"CustomerAppIcon29x29"];
```
同理想LaunchImage也会根据启动资源文件名称重命名，例如
```
CustomerLaunchImage-700@2x.png
CustomerLaunchImage-800-667h@2x.png
CustomerLaunchImage-800-Portrait-736h@3x.png
CustomerLaunchImage@2x.png
```
需要使用以下代码获取LaunchImage
```
UIImage *appIcon = [UIImage imageNamed:@"CustomerLaunchImage"];
```