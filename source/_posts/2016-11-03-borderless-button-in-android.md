---
title: 无边框Button
category: android
tags: [button]
date: 2016-11-03 10:14:33
excerpt: 如何创建无边框的button
---
直接添加下面的属性
```
style="?android:attr/borderlessButtonStyle"
```
