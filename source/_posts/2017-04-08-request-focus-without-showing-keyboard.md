---
title: EditText获取焦点同时不显示键盘
category: android
tags: [edittext]
excerpt: EditText获取焦点同时不显示键盘
date: 2017-04-08 09:39:57
---
目前的解决办法是将`inputType`设置为`null`