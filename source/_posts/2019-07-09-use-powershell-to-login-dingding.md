---
title: 使用powershell自动登录钉钉
category: miscellaneous
tags: [powershell]
excerpt: 使用powershell自动登录钉钉
date: 2019-07-09 00:12:52
---
多台电脑（PC、Mac）的情况下，每次打开钉钉都需要输入密码，Windows下可以使用Powershell脚本解决问题

```
add-type -AssemblyName microsoft.VisualBasic
add-type -AssemblyName System.Windows.Forms

Start-Process -FilePath 'C:\Program Files\Sandboxie\Start.exe' -ArgumentList '/box:DingDing','C:\Users\Public\Desktop\钉钉.lnk'
Start-Sleep -s 2
[System.Windows.Forms.SendKeys]::SendWait('钉钉密码')
[System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
```

保存为```DingDingLogin.ps1```，右键使用Powershell运行即可

允许执行脚本
```
set-executionpolicy remotesigned
```

<del>macOS使用以下脚本
```
#!/bin/sh
osascript -e 'tell application "DingTalk"' -e 'delay 0.5' -e 'keystroke "钉钉密码"' -e 'delay 0.5' -e 'keystroke return' -e 'end tell'
```
</del>

保存为```DingDingLogin.command```，添加可执行权限
```
chmod +x DingDingLogin.command
```

可惜上输入方案在10.14上有权限问题，无奈只能换个方案
使用[cliclick](https://github.com/BlueM/cliclick)工具，修改上面的脚本
```
#!/bin/sh
echo 1234 | sudo -S echo start
open -a "DingTalk"
delay 3
~/Applications/cliclick t:钉钉密码
~/Applications/cliclick kp:return
exit
```

最后可以修改下Terminal配置，使脚本执行完毕后自动关闭窗口
![terminal-close-after-shell-execute](/images/terminal-close-after-shell-execute.png )