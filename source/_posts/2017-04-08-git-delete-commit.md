---
title: 删除git中的commit
category: miscellaneous
tags: [git]
excerpt: 删除git中的commit
date: 2017-04-08 09:40:59
---
例如我想删除这两个commit
```
git log
commit 7abf68285083173869a7481b5947c1823be034c4
Author: Staticman <hello@staticman.net>
Date:   Fri Apr 7 20:48:07 2017 +0100

    Add Staticman data

commit 765f07b55a2bc7e4af3a01544a2ba4769a796221
Author: Staticman <hello@staticman.net>
Date:   Fri Apr 7 19:13:58 2017 +0100

    Add Staticman data
```
输入`git rebase -i HEAD~2`

将下面两行里的pick改为d，也就是drop
```
pick d03dca0 Add Staticman data
pick 23174f3 Add Staticman data
```

然后保存即可
___________________________
如果需要删除服务器端的commit
执行以下命令
```
git reset --hard HEAD~2
git push origin master --force
```