---
title: RxJava和Retrofit2示例
category: android
tags: [rxjava,retrofit2]
excerpt: RxJava和Retrofit2示例
date: 2017-04-22 15:16:56
---

### 官网
[ReactiveX](http://reactivex.io/)，[Retrofit](https://square.github.io/retrofit/)
`Android`下使用`RxJava`需要还需要[RxAndroid](https://github.com/ReactiveX/RxAndroid)。

### 配置RxAndroid
修改`app`模块下的`build.gradle`文件，添加以下依赖
```
compile 'io.reactivex.rxjava2:rxandroid:2.0.1'
//推荐显示引用最新版的RxJava
compile 'io.reactivex.rxjava2:rxjava:2.0.9'
```
RxJava和RxAndroid的最新版本为 <img src='http://img.shields.io/maven-central/v/io.reactivex.rxjava2/rxjava.svg'/> <img src='http://img.shields.io/maven-central/v/io.reactivex.rxjava2/rxandroid.svg'/>

### 配置Retrofit
修改`app`模块下的`build.gradle`文件，添加以下依赖
```
compile 'com.squareup.retrofit2:retrofit:2.2.0'
```
Retrofit的最新版本为：<img src='http://img.shields.io/maven-central/v/com.squareup.retrofit2/retrofit.svg'>

### 配置Retrofit-Converters
Retrofit一般还要配合一些Converters进行模型解析，例如解析xml
```
compile 'com.squareup.retrofit2:converter-simplexml:2.2.0',{
    exclude module: 'stax-api'
}
```
更多解析器参考[官方文档](https://github.com/square/retrofit/tree/master/retrofit-converters)

### 配置Retrofit-Adapters
Retrofit需要Adapter才能和RxJava一起工作
```
compile 'com.squareup.retrofit2:adapter-rxjava2:2.2.0'
```
更多Adapter参考[官方文档](https://github.com/square/retrofit/tree/master/retrofit-adapters)

### Demo

A simple demo:[RxJavaRetrofit2Demo](https://github.com/zongren/RxJavaRetrofit2Demo)