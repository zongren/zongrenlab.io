---
title: grpc提示Http2Exception
category: android
tags: [rpc,grpc]
excerpt: grpc提示Http2Exception
date: 2019-03-06 22:25:42
---

最近学习[grpc](https://grpc.io/)工具的时候遇到一个问题，创建好服务端和客户端，并成功运行后，遇到以下异常信息
```
io.netty.handler.codec.http2.Http2Exception: HTTP/2 client preface string missing or corrupt.
```

在[StackOverflow](https://stackoverflow.com/a/46123281)找到解决方案
```
ManagedChannelBuilder
        .forAddress("10.0.2.2", 50051)
        .usePlaintext()
        .build();
```

即创建ManagedChannel的时候，设置usePlaintext属性，根据文档，这个属性是指
```
   * Use of a plaintext connection to the server. By default a secure connection mechanism
   * such as TLS will be used.
```