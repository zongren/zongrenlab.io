---
title: 整理你的Xcode项目（一）
date: 2016-06-24 14:36:12
category: ios
tags: [osx,xcode,ios,development]
---

我们都知道Xcode使用Group来管理项目，虽然项目中看起来很不错，但是打开Finder，所有的文件还是聚集在一个文件夹中，看起来非常混乱（除了这点也没有其他缺点），对于处女座的人来说根本不能接受这种文件夹结构。好在有一款非常好用的`Gem`可以快速整理Xcode项目的物理文件夹结构，下载地址和使用方法请戳[venmo/synx: A command-line tool that reorganizes your Xcode project folder to match your Xcode groups](https://github.com/venmo/synx)。
P.S. 还会自动将Xcode项目的文件按照类型（Group和文件）和文件名排序。
其实使用这个插件最主要的目的是避免团队协作时因为文件顺序造成冲突，只要在Push和Pull之前正确使用这个插件，就可以很大程度的减少冲突，甚至没有冲突。