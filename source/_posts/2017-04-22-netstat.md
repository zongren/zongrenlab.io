---
title: 使用netstat查看程序使用的端口
category: miscellaneous
tags: [netstat]
excerpt: 使用netstat查看程序使用的端口
date: 2017-04-22 16:06:32
---

首先查看程序的PID值，可以参考{% post_link fix-file-occupied 这个 %}

然后使用`netstat -n -a -o`查看所有端口
{% img /images/netstat.png netstat %}