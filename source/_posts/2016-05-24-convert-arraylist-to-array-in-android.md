---
title: 将List转换为数组类型
date: 2016-05-24 20:03:22
category: android
tags: [list,array,tip,note]
---
记录一下如何转换List和Array类型
## 将List转换为Array
```
List<String> stringList = new ArrayList<>();
Sring[] stringArray = new String[stringList.size()];
stringList.toArray(stringArray);
```

## 将Array转换为List
```
Integer[] spam = new Integer[] { 1, 2, 3 };
Arrays<Integer>.asList(spam);
```