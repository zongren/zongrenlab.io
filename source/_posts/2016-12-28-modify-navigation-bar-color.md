---
title: 修改导航栏背景色和字体颜色
category: ios
tags: [tips]
date: 2016-12-28 13:22:41
excerpt: 如何修改导航栏背景色和字体颜色
---
如何修改导航栏背景色，并且修改标题的颜色
```
//背景色
self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
//标题颜色
self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
```
和隐藏标题栏的处理方法类似，最好把这个过程放在`viewWillAppear`中
```
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //背景色
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    //标题颜色
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    //导航栏隐藏、显示
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
```
