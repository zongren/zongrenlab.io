---
title: 透明Activity
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 打开透明的Activity
date: 2017-06-21 11:37:34
---
创建主题
```
<?xml version="1.0" encoding="utf-8"?>
<resources>
  <style name="Theme.Transparent" parent="android:Theme">
    <item name="android:windowIsTranslucent">true</item>
    <item name="android:windowBackground">@android:color/transparent</item>
    <item name="android:windowContentOverlay">@null</item>
    <item name="android:windowNoTitle">true</item>
    <item name="android:windowIsFloating">true</item>
    <item name="android:backgroundDimEnabled">false</item>
  </style>
</resources>
```

设置主题
```
<activity android:name=".SampleActivity" android:theme="@style/Theme.Transparent">
...
</activity>
```