---
title: 管理复杂页面的按钮点击事件
date: 2016-06-09 16:47:25
category: ios
tags: [tableview,event,best-practice]
excerpt: 管理复杂页面的按钮点击事件
---

1. {% post_link manage-button-events 管理复杂页面的按钮点击事件 %}
2. {% post_link manage-event-in-cell 管理复杂页面的按钮点击事件（二） %}
3. {% post_link associated-object-in-objc 管理复杂页面的按钮点击事件（三） %}

## 问题
最近在做的App的首页是根据接口返回数据动态生成（使用代码创建）的，而且页面结构和页面数据都是可编辑的，从而导致界面上按钮的类型和顺序完全不确定，所以不能像普通页面那样使用IBAction（iOS下）的方式来处理点击事件，需要对按钮的事件进行有规则的处理。

## 方法
目前想到的方案是利用UIView的tag属性，添加一些额外信息，然后在事件中根据这些信息进行针对性的处理。首先定义按钮的类型，将页面上所有可能出现的按钮进行分类，使用NS_ENUM进行定义。
```
typedef NS_ENUM (NSInteger, ViewType) {
    GOODS,
    SHOP,
    CATEGORY,
    PROMOTION,
    ADD_TO_CART,
    ...
};
```
除了按钮类型，还要将按钮代表的数据存到tag中，由于数据比较大，一般是存储数据在数组中对应的位置。以下为完整代码
```
//
//  UIView+ExtraTag.h
//
//  Created by 宗仁 on 16/6/8.
//
#import <UIKit/UIKit.h>
@interface UIView (ExtraTag)
/**
 * 将viewType存储到tag中，buttonType的范围0-127(0x00-0x7F)
 */
- (void)setViewTypeForTag:(NSInteger)viewType;
/**
 * 将position存储到tag中，position的范围0-255(0x00-0xFF)
 */
- (void)setPositionForTag:(NSInteger)position;
/**
 * 将extraInfo存储到tag中，extraInfo的范围0-65535(0x0000-0xFFFF)
 */
- (void)setExtraInfoForTag:(NSInteger)extraInfo;
- (NSInteger)getViewTypeOfTag;
- (NSInteger)getPositionOfTag;
- (NSInteger)getExtraInfoOfTag;
@end
//
//  UIView+ExtraTag.m
//
//  Created by 宗仁 on 16/6/8.
//

#import "UIView+ExtraTag.h"

@implementation UIView (ExtraTag)

- (void)setViewTypeForTag:(NSInteger)viewType {
    self.tag = (self.tag & 0x00FFFFFF) | (0xFF000000 & (viewType << 24));
}

- (void)setPositionForTag:(NSInteger)position {
    self.tag = (self.tag & 0xFF00FFFF) | (0x00FF0000 & (position << 16));
}

- (void)setExtraInfoForTag:(NSInteger)extraInfo {
    self.tag = (self.tag & 0xFFFF0000) | (0x0000FFFF & extraInfo);
}

- (NSInteger)getViewTypeOfTag {
    return (self.tag >> 24) & 0x000000FF;
}

- (NSInteger)getPositionOfTag {
    return (self.tag >> 16) & 0x000000FF;
}

- (NSInteger)getExtraInfoOfTag {
    return self.tag & 0x0000FFFF;
}
@end

```

## 使用方法
在创建视图的时候写入信息，处理点击事件的时候读取信息，代码如下
```
- (void)viewDidLoad {
    ...
    [self.goToTopButton addTarget:self
                            action:@selector (buttonClicked:)
                    forControlEvents:UIControlEventTouchUpInside];
    [self.goToTopButton setPositionForTag:255];
    [self.goToTopButton setViewTypeForTag:GO_TO_TOP];
    [self.goToTopButton setExtraInfoForTag:65535];
    ...
}
...
#pragma mark Handle all button click event here
- (void)buttonClicked:(UIButton*)sender {
    NSInteger buttonType = [sender getViewTypeOfTag];
    NSInteger position = [sender getPositionOfTag];
    NSInteger extraInfo = [sender getExtraInfoOfTag];
    NSLog (@"Button clicked,buttonType is %ld,position is %li,extraInfo is %li", (long)buttonType,
           (long)position, (long)extraInfo);
    switch (buttonType) {
    case GO_TO_TOP:
        [self.tableView setContentOffset:CGPointMake (0, 0) animated:YES];
        break;
    default:
        break;
    }
}
```