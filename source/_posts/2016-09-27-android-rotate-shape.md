---
title: 如何在Android中使用旋转
date: 2016-09-27 14:57:41
category: android
tags: [shape,rotate,drawable]
---

本文介绍如何在drawable文件中使用旋转shape的方式画出三角形，首先看以下代码
```
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item>
        <shape>
            <corners android:radius="1dp"/>
            <solid android:color="@color/colorPrimary"/>
        </shape>
    </item>
    <item>
        <rotate
            android:fromDegrees="45"
            android:pivotX="25%"
            android:pivotY="-200%"
            >
            <shape>
                <solid android:color="@android:color/white"/>
            </shape>
        </rotate>
    </item>
    <item>
        <rotate
            android:fromDegrees="-45"
            android:pivotX="75%"
            android:pivotY="-200%"
            >
            <shape>
                <solid android:color="@android:color/white"/>
            </shape>
        </rotate>
    </item>
    <item
        android:bottom="-5dp"
        android:left="4dp"
        android:right="-5dp"
        android:top="-5dp"
        >
        <shape>
            <stroke
                android:width="1dp"
                android:color="@android:color/white"
                android:dashGap="2dp"
                android:dashWidth="1dp"
                />
            <solid android:color="@android:color/transparent"/>
        </shape>
    </item>
</layer-list>
```
因为我不需要动画效果，所以并不需要设置`android:toDegrees`属性，只需设置`android:fromDegrees`，也就是旋转的角度即可。`android:fromDegrees`大于0时顺时针旋转，小于0时逆时针旋转。`android:pivotX`和`android:pivotY`是旋转的轴点，如果不设置 `android:pivotX`和`android:pivotY`属性的话，默认使用中心点进行旋转。