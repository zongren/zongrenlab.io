---
title: 不使用StoryBoard
date: 2016-09-12 11:31:13
category: ios
tags: [storyboard]
---
如果你不想在项目中使用storyboard，那么需要做以下处理
* 移除`Info.plist`中的相关信息
```
<key>UIMainStoryboardFile</key>
<string>Main</string>
```
* 使用代码创建UIWindow
如果不使用storyboard，就需要使用代码创建UIWindow，在AppDelegate中
```
- (BOOL)application:(UIApplication*)application
    didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    SplashViewController* splashViewController = [[SplashViewController alloc] init];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = splashViewController;
    [self.window makeKeyAndVisible];
}
```