---
title: 保存Git密码
date: 2016-07-06 10:55:01
category: development
tags: [git,tip,note]
---

Mac下使用以下命令
```
git config --global credential.helper osxkeychain
# Set git to use the osxkeychain credential helper
```

Windows下使用以下命令
```
git config --global credential.helper wincred
```