---
title: remove-elment
category: leetcode
tags:
  - algorithm
date: 2016-10-13 09:30:18
---

## 问题链接
[Remove Element | LeetCode OJ](https://leetcode.com/problems/remove-element/)

## 问题描述
给出一个数组`nums`和一个值`val`，移除数组中值为`val`的元素并返回移除后的数组长度`l`，可以忽略`l-1`之后的元素。


## 解决办法
```
public class Solution {
    public int removeElement(int[] nums, int val) {
        if (nums.length == 0) {
            return 0;
        }
        int length = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[length] = nums[i];
                length++;
            }
        }
        return length;
    }
}
```
