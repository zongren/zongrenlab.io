---
title: 去除UINavigationBar渐变效果
date: 2016-07-04 10:22:01
category: ios
tags: [note,tip]
---
去除`UINavigationBar`的除渐变效果的方法是将`translucent`(半透明的)属性设置为NO，在代码中按照以下代码设置
```
navigationBar.translucent = NO;
```
你也可以在可视化编辑器中更改这个属性。