---
title: 删除Windows.old文件夹
category: miscellaneous
tags: [windows]
excerpt: Windows10预览版系统，无法使用磁盘清理删除Windows.old文件夹
date: 2018-02-10 14:57:45
---

一般来说，通过磁盘清理，是可以删除Windows.old文件夹，也就是每次升级系统之前的系统备份
{% img /images/disk-clean.png disk-clean %}

不过由于我加入了预览体验计划，系统还有Bug，导致我无法删除这个文件夹，只能通过以下命令手动删除

```
rd /S /Q C:\windows.old
```

注意需要已管理员运行命令行应用