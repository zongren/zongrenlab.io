---
title: 嵌套的RecyclerView如何实现wrap_content
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 嵌套的RecyclerView如何实现wrap_content
date: 2017-06-21 11:37:11
---
问题描述，在`ScrollView`内有一个`RecyclerView`，想要实现`wrap_content`高度，尝试了各种方法之后，发现最有效的还是

把`ScrollView`替换为`android.support.v4.widget.NestedScrollView`