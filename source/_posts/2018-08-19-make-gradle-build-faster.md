---
title: 加快gradle构建速度
category: android
tags: [gradle,build]
excerpt: 针对多模块项目,如何加快gradle构建速度
date: 2018-08-19 23:00:00
---

## 背景简介

目前开发的项目采用了多模块（`module`）管理的方式，模块数量超过40个。开发的过程中发现其构建的速度（点击`Run`和`Debug`）所需要的时间相对较长，于是通过官方的[文档](https://developer.android.google.cn/studio/build/optimize-your-build#create_libraries)和一些搜索，采用以下方法加快构建速度。

## 分析任务

首先通过以下命令，分析哪些gradle任务占用了较多时间
```
gradlew --profile --recompile-scripts --offline --rerun-tasks assembleMkbAkashaFaceV1AuthDeployRelease
```
这个命令执行了一次完整构建，并且把每个`gradle`任务所花费的时间都记录在了一个`html`文件中，如图所示
{% img /images/gradle-profile.png gradle-profile %}

建议在每次修改gradle配置之后，重新运行此任务，检查是否有改善


## 升级gradle版本

将gradle升级到android gradle plugin所支持的最新的版本，当前最新版本是4.9

## 增加gradle可用内存

根据`gradle`官方项目的一个[issue](https://github.com/gradle/gradle/issues/1413)和[commit](https://github.com/gradle/gradle/commit/73f32d68824582945f5ac1810600e8d87794c3d4)来看，似乎大项目（例如多模块）需要更多的内存运行`gradle`，而根据[官方文档](https://developer.android.google.cn/studio/build/optimize-your-build#dex_options)，同样指出最好通过增加`gradle`可用内存的方式，加快构建速度，在`gradle.properties`中，设置`gradle`所需内存

```
org.gradle.jvmargs=-Xmx2048m 
```

## 停用PNG处理

默认情况下，PNG图片会转换为`WebP`图片，开发时停用此功能，能节省不少时间
```
android {
  ...
  aaptOptions {
    cruncherEnabled false
  }
}
```
由于此选项不支持根据口味（`flavor`）配置，只能在发布的时候手动调整，以免apk太大

## 启用离线模式

由于某些原因，执行完一次完整的构建之后（`Gradle Sync`和`Rebuild`），最好开启离线模式，已避免网络因素导致的构建速度问题
要在使用`Android Studio`构建时离线使用`Gradle`，请执行以下操作：

1. 点击 File > Settings（在 Mac 上，点击 Android Studio > Preferences），打开 Preferences 窗口。
2. 在左侧窗格中，点击 Build, Execution, Deployment > Gradle。
3. 勾选 Offline work 复选框。
4. 点击 Apply 或 OK。

## 禁用lintVital任务

```
afterEvaluate {
    Set<Task> result = tasks.findAll { task -> task.name.startsWith('lintVital') }
    result.each { Task task ->
        task.enabled = false
    }
}
```

## 启用并行构建

在`gradle.properties`文件中加入以下代码
```
org.gradle.parallel=true
```
