---
title: 从相机获取Bitmap
date: 2016-07-12 17:32:47
category: android
tags: [camera,bitmap]
---
下面介绍了如何使用相机照相并存储照片
```
// Step one
File tempFile = File.createTempFile("camera", ".png", getExternalCacheDir());
mPath = tempFile.getAbsolutePath();
Uri uri = Uri.fromFile(tempFile);

// Step two
Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

// Step three
protected void onActivityResult(int requestCode, int resultCode, Intent data){
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    Bitmap bitmap = BitmapFactory.decodeFile(mPath, options);
}
```