---
title: 怎么翻译充值
category: miscellaneous
tags: [english]
excerpt: 怎么翻译充值
date: 2018-11-20 10:01:11
---

## 问题
最近做项目的时候，遇到一个问题，如何翻译饭卡及给饭卡翻译这个功能

## 结论

充值：
top up/transfer money
put money on it

充值卡：
prepaid card


## 引用

### [“充值卡”英文怎么说](http://language.chinadaily.com.cn/columnist/2013-10/28/content_17062278.htm)

### [Top Up 充值](http://www.bbc.co.uk/china/learningenglish/realenglish/re/2007/06/070601_top_up.shtml)

### [实用英语口语:“给手机充值”英语怎么说?](http://yingyu.xdf.cn/201208/9005125.html)