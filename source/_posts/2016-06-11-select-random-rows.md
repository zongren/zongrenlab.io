---
title: 从表中选出随机数据
date: 2016-06-11 20:43:17
category: miscellaneous
tags: [mysql,tip,note]
---

如果想从MySql数据库的表中随机选出几条数据，可以采用以下方式
```
SELECT * FROM `table` WHERE 1 ORDER BY RAND();
```
这个方法代码很简单，但是效率比较差，数据量大的话可以试试以下方式
```
SELECT * FROM `table` AS t1 JOIN (SELECT ROUND(RAND() * ((SELECT MAX(id) FROM `table`)-(SELECT MIN(id) FROM `table`))+(SELECT MIN(id) FROM `table`)) AS id) AS t2 WHERE t1.id >= t2.id ORDER BY t1.id LIMIT 1;
```
这种方式需要有一个自增字段。