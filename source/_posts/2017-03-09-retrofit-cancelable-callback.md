---
title: Android代码片段
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 从GitHub上复制的一个可以取消的的Callback
date: 2017-03-09 17:38:48
---
这是管理工具类
```
package com.zqsy.android.messenger.Util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallbacksManager {
    private final Set<CancelableCallback> callbacks = new HashSet<>();

    public void cancelAll() {
        for (CancelableCallback callback : callbacks) {
            // false to avoid java.util.ConcurrentModificationException alternatively we can use
            // iterator
            callback.cancel(false);
        }
        callbacks.clear();
    }

    public void resumeAll() {
        final Iterator<CancelableCallback> iterator = callbacks.iterator();
        while (iterator.hasNext()) {
            boolean remove = iterator.next().resume();
            if (remove) {
                iterator.remove();
            }
        }
    }

    public void pauseAll() {
        for (CancelableCallback callback : callbacks) {
            callback.pause();
        }
    }

    public void addCallback(CancelableCallback<?> callback) {
        callbacks.add(callback);
    }

    private void removeCallback(CancelableCallback<?> callback) {
        callbacks.remove(callback);
    }

    public abstract class CancelableCallback<T> implements Callback<T> {
        private boolean canceled;
        private boolean paused;

        private Call<T> pendingT;
        private Response<T> pendingResponse;
        private Throwable pendingError;

        public CancelableCallback() {
            this.canceled = false;
        }

        public void pause() {
            paused = true;
        }

        public boolean resume() {
            paused = false;
            // if callback was cancelled then no need to post pending results
            if (canceled) {
                return true;
            }
            if (pendingError != null) {
                onFailure(pendingT,pendingError);
                // to make sure not to post it again
                pendingError = null;
                return true;
            } else if (pendingT != null) {
                onPendingResponse(pendingT, pendingResponse);
                // to make sure not to post it again
                pendingT = null;
                pendingResponse = null;
                return true;
            }
            return false;
        }

        public void cancel(boolean remove) {
            canceled = true;
            if (remove) {
                removeCallback(this);
            }
        }

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            if (canceled) {
                return;
            }
            if (paused) {
                pendingT = call;
                pendingResponse = response;
                return;
            }
            onPendingResponse(call, response);
            removeCallback(this);
        }

        @Override
        public void onFailure(Call<T> call, Throwable throwable) {
            if (canceled) {
                return;
            }
            if (paused) {
                pendingError = throwable;
                return;
            }
            onPendingFailure(call,throwable);
            removeCallback(this);
        }

        public abstract void onPendingResponse(Call<T> call, Response<T> response);

        protected abstract void onPendingFailure(Call<T> call, Throwable throwable);
    }
}
```

使用方法
```
protected final CallbacksManager callbacksManager = new CallbacksManager();
private void downloadFile(String filePath){
  Call<ResponseBody> call = mClient.downloadFile(filePath);
  call.enqueue(callbacksManager.new CancelableCallback<ResponseBody>() {
      @Override
      public void onPendingResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
          if (response.isSuccessful()) {
              downloadFileSucceed(position, response.body(), fileName);
          } else {
              downloadFileFailed(position);
          }
      }

      @Override
      public void onPendingFailure(Call<ResponseBody> call, Throwable throwable) {
          downloadFileFailed(position);
      }
  });
}

@Override
public void onPause() {
    callbacksManager.pauseAll();
    super.onPause();
}

@Override
public void onResume() {
    super.onResume();
    callbacksManager.resumeAll();
}

@Override
public void onDestroy() {
    callbacksManager.cancelAll();
    super.onDestroy();
}
```