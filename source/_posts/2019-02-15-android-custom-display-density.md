---
title: 一种极低成本的Android屏幕适配方式
category: android
tags: [ui,display]
excerpt: 总结字节跳动团队Android屏幕适配方式及多屏幕适配
date: 2019-02-15 10:05:20
---

原文链接：[https://zhuanlan.zhihu.com/p/37199709](https://zhuanlan.zhihu.com/p/37199709)

android中的dp在渲染前会将dp转为px，计算公式：

* density = dpi / 160
* px =  dp * density
* px = dp * dpi / 160
* dp = px / density
* dp = px * 160 / dpi

dpi是根据屏幕物理分辨率和物理尺寸来计算的，每个设备可能不一样，如图所示

{% img /images/dpi_calculate.jpg dpi_calculate.jpg %}

举个例子：屏幕分辨率为：1920*1080，屏幕尺寸为5吋的话，那么dpi为440，如图所示

{% img /images/android_device_display_demo.jpg android_device_display_demo.jpg %}

由此产生的问题是，UI如果按照360dp设计，而应用开发者直接使用UI设计稿里的dp尺寸，会导致真实画面和设计画面不太一样，例如上图的设置，计算之后的dp就是

```
1080 * 160 / 440 = 392.7dp
```

所以解决方案就是修改DisplayMetrics对象的```density```和```densityDpi```属性

```
DisplayMetrics appDisplayMetrics = application.getResources().getDisplayMetrics();
float designedDensity = appDisplayMetrics.widthPixels / 360f;
float factor = designedDensity / appDisplayMetrics.density;
float designedScaledDensity = appDisplayMetrics.scaledDensity * factor;
int designedDensityDpi = (int) (160f * designedDensity);

appDisplayMetrics.scaledDensity = designedScaledDensity;
appDisplayMetrics.density = designedDensity;
appDisplayMetrics.densityDpi = designedDensityDpi;

DisplayMetrics activityDisplayMetrics = activity.getResources().getDisplayMetrics();
activityDisplayMetrics.scaledDensity = designedScaledDensity;
activityDisplayMetrics.density = designedDensity;
activityDisplayMetrics.densityDpi = designedDensityDpi;
```

然而针对多块屏幕的应用，如何设置呢？

目前想到的办法是，首先获取Display对象，创建Presentation对象，然后在Presentation布局中添加SurfaceView，然后使用这个SurfaceView创建VirtualDisplay，创建VirtualDisplay的时候提供自定义Density，然后使用此VirtualDisplay创建Presentation对象，并展示