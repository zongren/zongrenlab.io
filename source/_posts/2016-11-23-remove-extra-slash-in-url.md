---
title: 移除URL中多余的斜杠
category: miscellaneous
tags: [string]
date: 2016-11-23 10:52:37
---
首先看一下Java下的处理方法
```
public static String removeExtraSlashOfUrl(String url) {
    if (url == null || url.length() == 0) {
        return url;
    }
    return url.replaceAll("(?<!(http:|https:))/+", "/");
}
```
然后看一下Objective-C语言的版本
```
+(NSString*) removeExtraSlashOfUrl:(NSString*)url{
    if(!url || url.length == 0){
        return url;
    }
    NSString*pattern = @"(?<!(http:|https:))/+";
    NSRegularExpression*expression = [[NSRegularExpression alloc]initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    return [expression stringByReplacingMatchesInString:url options:0 range:NSMakeRange(0, url.length) withTemplate:@"/"];
}
``` 