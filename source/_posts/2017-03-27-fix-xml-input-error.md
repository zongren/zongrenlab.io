---
title: 修复xml提示input错误
category: miscellaneous
tags: [fix]
date: 2017-03-27 15:49:51
excerpt: 修复xml提示input错误
---

错误具体为
```
This page contains the following errors:

error on line 3349 at column 335: Input is not proper UTF-8, indicate encoding !
Bytes: 0x1B 0x3C 0x2F 0x63
Below is a rendering of the page up to the first error.
```

按照以下方法解决了问题
1. 使用正则`\x1B`在atom.xml中搜索，找到出问题的文章
2. 使用正则`\x1B`在文章中搜索，删除该字符
