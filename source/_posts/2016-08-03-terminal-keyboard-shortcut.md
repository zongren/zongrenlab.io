---
title: 终端快捷键
date: 2016-08-03 10:24:23
category: miscellaneous
tags: [osx,terminal,shortcurt]
---

开发iOS难免要接触终端（Terminal），如果记住以下的快捷键，会对开发效率有很大的提升。

## 移动光标

快捷键    |  说明   
-------- | -------
Ctrl + a | 移动到行首
Ctrl + e | 移动到行尾
         | Hold the Option key option and click on the current line = Jump Backwards
Ctrl + p | 上一条命令
Ctrl + n | 下一条命令
         |   Hold the Option key option and click on a previous line = Jump upwards
Ctrl + f | 向前（右）移动一个字符
Ctrl + b | 向后（左）移动一个字符
Alt + b  | 向后（左）移动一个单词
Option + Left Arrow | 同上
Alt + f | 向前（右）移动一个单词
Option + Right Arrow | 同上
Ctrl + xx | 光标在行首和当前位置切换

## 编辑
快捷键    |  说明   
-------- | -------
Ctrl + L | 清空屏幕
Alt + Delete | 删除光标之前的单词
Alt + d | 删除光标之后的单词
Ctrl + d | 删除当前字符
Ctrl + h | 删除光标之前的字符
Ctrl + w | 剪切光标之前的单词并复制到剪切板（使用Ctrl+y粘贴，而不是Common＋V）
Ctrl + k | 剪切光标之后的行并复制到剪切版
Ctrl + u | 剪切光标之后的行并复制到剪切版
Alt + t  | 将当前单词和之前的单词交换位置
Ctrl + t | 将当前字符和之前的字符交换位置
Esc + t  | 将光标之前的两个单词交换位置
Ctrl + y | 粘贴复制的内容
Alt + u  | 把当前字符以及这个单词之后的字符转换为大写
Alt + l  | 把当前字符以及这个单词之后的字符转换为小写
Alt + c  | 把当前字符转换为大写并移动光标到单词之后
Alt + r  | 恢复行（不清楚怎么用）
Ctrl + - | 撤销
TAB | 文件名提示
