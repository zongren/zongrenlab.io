---
title: 设置git用户名和邮箱
date: 2016-10-03 14:35:15
category: miscellaneous
tags: [git]
---
如何设置git用户名和邮箱呢，请看代码
```
git config user.name "Billy Everyteen"
# Set a new name
git config user.name
# Verify the setting
Billy Everyteen
git config user.email "example@example.com"
# Set a new email
git config user.email
# Verify the setting
example@example.com
```
