---
title: 更改input的Placeholder颜色    
date: 2016-09-05 11:48:20
category: miscellaneous
tags: [css,input]
---
```
::-webkit-input-placeholder {
   color: primary-color;
}

:-moz-placeholder { /* Firefox 18- */
   color: primary-color;  
}

::-moz-placeholder {  /* Firefox 19+ */
   color: primary-color;  
}

:-ms-input-placeholder {  
   color: primary-color;  
}
```