---
title: iOS中的虚类
date: 2016-08-26 09:33:33
category: ios
tags: [abstract,pattern]
---
iOS无法使用`abstract`关键字定义`虚类`或者`虚函数`，不过我们可以通过以下几个方法实现类似的效果。
## 使用doesNotRecognizeSelector抛出异常
### 虚类
使用以下代码可以实现类似虚类的效果
```
#import "AbstractObject.h"

@implementation AbstractObject
- (instancetype)init {
    if ([self isMemberOfClass:[AbstractObject class]]) {
        [self doesNotRecognizeSelector:_cmd];
        return nil;
    } else {
        self = [super init];
        if (self) {
            NSLog (@"Initialized");
        }
        return self;
    }
}
@end
```
ConcretObject继承AbstractObject，调用代码以及结果如下
```
AbstractObject* abstractObject = [[AbstractObject alloc] init];//运行时报错
ConcretObject* concretObject = [[ConcretObject alloc] init];//正常运行
```

### 虚函数
使用以下代码可以实现类似虚函数的效果，如果子类没有实现这个方法，那么父类调用该方法时就会抛出异常
```
- (void) abstractMethod{
   [self doesNotRecognizeSelector:_cmd];
}
```

## 使用宏抛出异常
这个方法和上个方法基本类似，只不过是使用自定义的宏抛出异常，方法如下
```
#define mustOverride() @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"%s must be overridden in a subclass/category", __PRETTY_FUNCTION__] userInfo:nil]
#define methodNotImplemented() mustOverride()
- (void) abstractMethod{
   mustOverride();
}
```
