---
title: macOS开启NTFS写入功能
category: miscellaneous
tags: [macos]
excerpt: 使用第三方工具，开启ntfs移动硬盘写入功能
date: 2018-08-15 17:00:17
---

macOS默认只支持读取ntfs硬盘，不支持写入，我从[medium上的一篇post](https://medium.com/the-lazy-coders-journal/enable-ntfs-write-support-in-macos-high-sierra-10-13-x-3fc8c67b5c1)中得知，可以使用一些第三方工具，让macOS支持写入ntfs硬盘

## 安装Homebrew
前往[Homebrew](https://brew.sh/index_zh-cn)官网或者直接使用以下脚本
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

```

## 关闭System Integrity Protection
按住Command+R开机，进入恢复模式，打开终端输入以下命令
```
csrutil disable
```

重启后，在终端输入以下命令判断是否成功关闭
```
csrutil status
```

现在应该显示
```
System Integrity Protection status: disabled.
```

## 安装FUSE
[osxfuse](https://osxfuse.github.io/)

```
brew cask install osxfuse
```

## 安装ntfs-3g
[ntfs-3g](https://www.tuxera.com/community/open-source-ntfs-3g/)
```
brew install ntfs-3g
```

## 异常处理

如果安装ntfs-3g出现了异常，例如
```
Error: Could not symlink sbin/ntfs-3g
usr/local/sbin is not writable.
```

这种情况是正常的，通过以下方式可以解决
### 创建sbin文件夹
```
sudo mkdir sbin
```

### 赋予写入权限
```
sudo chown -R $(whoami) $(brew --prefix) 
```

### link ntfs-3g
```
brew link ntfs-3g
```

## 替换ntfs驱动
```
sudo mv /sbin/mount_ntfs /sbin/mount_ntfs.original
sudo ln -s /usr/local/sbin/mount_ntfs /sbin/mount_ntfs
```

## 重新开启System Integrity Protection
按住Command+R开机，进入恢复模式，打开终端输入以下命令
```
csrutil enable
```

重启后，在终端输入以下命令判断是否成功开启
```
csrutil status
```

现在应该显示
```
System Integrity Protection status: enabled.
```
