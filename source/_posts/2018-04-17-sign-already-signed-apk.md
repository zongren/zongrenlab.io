---
title: 已签名的apk重新签名
category: android
tags: [sign]
excerpt: 使用jarsinger.exe对已签名的apk重新签名
date: 2018-04-17 15:24:00
---

将singed.apk重命名为signed.zip，并解压缩

删除`META-INF`文件夹，并重新压缩成unsigned.zip文件，然后重命名为unsigned.apk

使用jarsigner签名

```
jarsigner.exe -sigalg SHA1withRSA -digestalg SHA1 -keystore {keystore路径} {待签名apk路径} -storepass {keystore存储密码} -keypass {keystore密码} {keystore别名}
```

jarsigner运行文件在jdk/bin目录下