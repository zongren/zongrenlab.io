---
title: 设置GridView高度
date: 2016-05-21 09:12:56
category: android
tags: [gridview,tip,height]
---
如果将gridview设置为wrapcontent，gridview默认显示一行，多出的内容滚动显示。请参考以下代码
```
@Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightSpec;

        if (getLayoutParams().height == LayoutParams.WRAP_CONTENT) {
            heightSpec = MeasureSpec.makeMeasureSpec(
                    Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        }
        else {
            heightSpec = heightMeasureSpec;
        }

        super.onMeasure(widthMeasureSpec, heightSpec);
    }
```
