---
title: 有符号数表示
tags:
  - development
  - binary
  - computer-science
category: development
date: 2016-09-19 11:51:55
---

维基百科链接：[Signed number representations](https://en.wikipedia.org/wiki/Signed_number_representations)

计算机系统不能直接表示加号和减号，所以需要一种表达有符号数的方式，一般有{% post_link signed-magnitude-representation 原码 %}，{% post_link ones-complement 反码 %}，{% post_link two-s-complement 补码 %}，{% post_link excess-k 移码 %}等几种表示方式。