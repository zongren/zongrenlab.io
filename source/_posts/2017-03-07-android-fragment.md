---
title: Android代码片段
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 一个Android代码片段，尽量保证clean&no-hack
date: 2017-03-07 14:21:38
---

返回到Home Screen
```
Intent startMain = new Intent(Intent.ACTION_MAIN);
startMain.addCategory(Intent.CATEGORY_HOME);
startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
startActivity(startMain);
```