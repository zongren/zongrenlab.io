---
title: 代码风格
category: development
tags: [code-style,standard]
date: 2016-12-01 13:41:52
excerpt: This is the code styles applied to my projects and work.
---

## General code style

### Clarity
You have to make sure it is easy for other people to read your code,for example:
```
// This code is bad because others won't know what btn1 does.
Button btn1 = new Button();
```
```
// This code is good because others will know that buttonLogin is for login action.
Button buttonLogin = new Button();
```
```
// This code is misleading because it looks like both statements
// are inside the conditional.
if (condition)
  firstStatement();
  secondStatement();
```
```
// Much clearer!
if (condition) {
  firstStatement();
}

secondStatement();
```

## Language specific code styles
### PHP
### Objective-C
### Java
### JavaScript
### [EJS](http://ejs.co/)
### HTML
### Css
### [Stylus](http://stylus-lang.com/)

### Markdown

#### One empty line above the title(#,##,##...).

#### Use `pre` instead of images to show diagrams.For instance:
```
   |                            |                    |
---|--> Third party libraries --|--> Encapsulation --|---> Project
   |                            |                    |
```

## OS specific code styles
### Windows
### macOS
### Linux

## Platform specific code styles
### Windows
### iOS
### Android
