---
title: 提示找不到so文件的错误
category: miscellaneous
tags: [android-studio]
date: 2017-04-01 11:52:33
excerpt: 使用Android Studio打开一个从Eclipse导出的Android项目后，提示找不到so文件。
---
发现需要手动添加`jniLibs`目录到`build.gradle`文件
```
android {
    sourceSets {
        main {
            jniLibs.srcDirs = ['libs']
        }
    }        
}
```
