---
title: 获取SHA1值
category: android
tags: [keystore,sha1]
date: 2016-12-03 15:46:27
excerpt: 获取证书SHA1值。
---
在Linux/macOS下，使用以下命令获取证书信息
```
keytool -exportcert -list -v -alias <your-key-name> -keystore <path-to-production-keystore>
```
`keytool`一般位于`$JAVA_HOME/bin/`目录下。对于`debug`版本来说，默认的密钥路径为`~/.android/debug.keystore`，默认密码为`android`
```
keytool -exportcert -list -v -keystore ~/.android/debug.keystore
输入密钥库口令: android
```
`release`版本则输入对应的`alias`和密钥路径
```
keytool -exportcert -list -v -alias example -keystore ~/AndroidStudioProjects/example/example.jks
输入密钥库口令: example
```