---
title: 声明只读List
date: 2016-05-31 11:49:25
category: android
tags: [java]
---

There are three ways to do the trick,here is the code

```
{% raw %}
//Java 1.4 compatible way
public static final List STRINGS = Collections.unmodifiableList(
Arrays.asList(new String[] {"foo", "bar"}));
//For less ancient Java versions
public static final List<String> STRINGS = Collections.unmodifiableList(
Arrays.asList("foo", "bar"));
//A neat way to do this,incase you have a long list.
public static final List<String> list = Collections.unmodifiableList(
new ArrayList<String>() {{
    add("foo");
    add("bar");
    // etc
}});
{% endraw %}
```