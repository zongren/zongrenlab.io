---
title: 管理列表的有效方式
date: 2016-05-17 14:40:00
category: android
tags: [listview,practice]
---

本文主要讨论如何制作一个简单实用的列表组件，首先考虑最简单的情况，即只显示一个标题的列表常见的分类或者地区都属于这种情况，每个item只需显示一条标题，并且只有点击事件需要处理，以下是ListViewActivity的代码

```
/**
 * Created by zongren on 16/5/17.
 */
public abstract class ListViewActivity<T extends ListViewItem> extends CommonActivity implements OnBreadCrumbItemClickListener, ListViewFragmentChange<T> {
    private static final String TAG = "ListViewActivity";

    protected ArrayList<T> mSelectedDataList;

    @Bind(R.id.fragment_listView_breadCrumbLayout)
    protected BreadCrumbLayout mBreadCrumbLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLayoutId = R.layout.activity_listview;
        super.onCreate(savedInstanceState);
        mSelectedDataList = new ArrayList<>();
        mBreadCrumbLayout.setOnBreadCrumbItemClickListener(this);
    }

    @Override
    protected CommonFragment createFragment() {
        ListViewFragment fragment = createListViewFragment();
        fragment.setListViewFragmentChange(this);
        return fragment;
    }

    @Override
    public void onBackPressed() {
        int count = mFragmentManager.getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            mFragmentManager.popBackStack();
            mBreadCrumbLayout.removeLastBreadCrumb();
        }
    }

    public void openNextActivity() {
        Intent result = new Intent();
        result.putParcelableArrayListExtra(KEY.RESULT.getValue(), mSelectedDataList);
        result.setClass(this, getNextActivity());
        finish();
        startActivity(result);
    }

    public void onBreadCrumbItemClicked(int position) {
        mFragmentManager.popBackStack(String.valueOf(position), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        Log.i(TAG, "BreadCrumb clicked " + position);
    }

    @Override
    public void openNextFragment(T data) {
        mSelectedDataList.add(data);
        ListViewFragment nextFragment = createListViewFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(KEY.LISTVIEW_PARENT_DATA.getValue(), data);
        nextFragment.setArguments(arguments);
        nextFragment.setListViewFragmentChange(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.activity_common_fragment_container, nextFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(String.valueOf(mBreadCrumbLayout.getBreadCount()))
                .commitAllowingStateLoss();
        mBreadCrumbLayout.addBreadCrumb(data.getListItemBreadCrumb());
    }

    @Override
    public void onFinishFragment() {

        if (getCallingActivity() == null) {
            openNextActivity();
        } else {
            Intent result = new Intent();
            result.putParcelableArrayListExtra(KEY.RESULT.getValue(), mSelectedDataList);
            setResult(RESULT_OK, result);
            finish();
        }
    }

    abstract ListViewFragment<T> createListViewFragment();

    abstract Class getNextActivity();
}
```

以下是ListViewFragment的代码

```
/**
 * Created by zongren on 16/5/17.
 */
public abstract class ListViewFragment<T extends ListViewItem> extends CommonFragment {
    private static final String TAG = "ListViewFragment";

    protected ArrayList<T> mDataList;
    protected T mParentData;
    protected ListViewFragmentChange mListViewFragmentChange;
    protected BaseAdapter mBaseAdapter;

    @Bind(R.id.fragment_listView_listView)
    protected ListView mListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mParentData = arguments.getParcelable(KEY.LISTVIEW_PARENT_DATA.getValue());
        }
        mLayoutId = R.layout.fragment_listview;
        mDataList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListViewFragmentChange != null) {
                    if (isEnd(position)) {
                        mListViewFragmentChange.onFinishFragment();
                    } else {
                        mListViewFragmentChange.openNextFragment(mDataList.get(position));
                    }
                }
            }
        });

        mBaseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return mDataList.size();
            }

            @Override
            public Object getItem(int position) {
                return mDataList.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.item_listview, parent, false);
                    ((TextView) convertView).setText(mDataList.get(position).getListItemTitle());
                }
                return convertView;
            }
        };
        mListView.setAdapter(mBaseAdapter);

        return v;
    }

    public void setListViewFragmentChange(ListViewFragmentChange listViewFragmentChange) {
        mListViewFragmentChange = listViewFragmentChange;
    }

    abstract boolean isEnd(int position);
}
```