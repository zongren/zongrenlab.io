---
title: iOS下的发布订阅模式
date: 2016-05-24 10:41:15
category: ios
tags: [publish,subscribe,pattern,tip,note]
---

发布一条消息（Publish a message）
```
[[NSNotificationCenter defaultCenter] postNotificationName:@"Notification_GetUserProfileSuccess" object:userProfile userInfo:nil];
```
订阅一条消息（Subscribe a message）
```
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUserProfileSuccess:) name:@"Notification_GetUserProfileSuccess" object:nil];
```
移除订阅
```
[[NSNotificationCenter defaultCenter] removeObserver:self name:@"Notification_GetUserProfileSuccess" object:nil];
```