---
title: First Unique Character in a String
date: 2016-08-31 08:43:57
category: leetcode
tags: [algorithm]
---
## 问题链接
[First Unique Character in a String | LeetCode OJ](https://leetcode.com/problems/first-unique-character-in-a-string/)

## 问题描述
给出一个字符串，找到**第一个不重复**的字符并返回它的位置，如果不存在，则返回－1，例如
```
s = "leetcode"
返回 0.

s = "loveleetcode",
返回 2.

s = "",
返回 -1;
```


## 解决办法
以下为我的解决方法，语言为Java
```
public class Solution {
    public int firstUniqChar(String s) {
        String origin = s;
        String a = null;
        while(true){
            if(s.length() == 0){
                if(a == null){
                    return -1;
                }
                else{
                    return origin.indexOf(a);
                }
            }
            else if(s.length() == 1){
                if(a == null){
                    return origin.indexOf(s);
                }
                else{
                    if(s.contains(a)){
                        return -1;
                    }
                    else{
                        return origin.indexOf(a);
                    }
                }
            }
            else{
                a = s.substring(0,1);
                s = s.substring(1);
                if(s.contains(a)){
                    s = s.replace(a,"");
                    a = null;
                }
                else{
                    return origin.indexOf(a);
                }
            }
            
        }
    }
}
```
