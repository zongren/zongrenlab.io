---
title: 删除git仓库的其它commit
date: 2016-10-03 14:13:12
category: miscellaneous
tags: [git]
---
有时候你会把一些大文件丢到git仓库中，然后后悔又删掉它们，不过每次`clone`，`fork`的时候就很痛苦，因为这些文件已经保存在`.git`文件夹中。那如何删除这些文件呢，最合理的办法就是使用`git gc`删除它们，不过你也可以删除所有的`commit`，然后重新初始化`git`仓库。
## 删除.git
```
rm -rf .git
```
## 初始化
```
git init
git add .
git commit -m "Initial Commit"
```
## 提交
```
git remote add origin <git-url>
git push -u --force origin master
```
## 测试
然后你就可以执行`clone`命令，测试一下是不是成功了。