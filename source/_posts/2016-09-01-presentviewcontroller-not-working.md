---
title: presentViewController不起作用
date: 2016-09-01 11:23:32
category: ios
tags: [controller,tip]
---
在`rootViewController`的`viewDidLoad`方法中调用以下代码是不会起作用的，解决办法是在`viewDidAppear`方法中调用。
```
[self presentViewController:viewController animated:YES completion:nil];
```
