---
title: 使用反射找到内部静态类 
category: android 
tags: [java] 
excerpt: 使用反射找到内部静态类。
date: 2017-10-12 10:52:19 
---

例如在类`ClassA`中存在一个静态类`ClassB`，现在想通过反射找到类`ClassB`并修改其中的变量

```
Class classB = Class.forName(packageName + ".ClassA$ClassB);
Field[] fields = classB.getDeclaredFields();
for (Field field : fields) {
}
```
________________________________________________
另外修改一个静态变量时
```
for (Field field : fields) {
    if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
        field.set(null,"valueForStaticField");
    }
}
```