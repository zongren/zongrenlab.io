---
title: 转义SVN命令中的@符号
date: 2016-05-26 11:52:44
category: development
tags: [version-control,svn,tip,note]
---
## 问题
最近碰到一个问题，我们的项目使用SVN管理图片资源，包括Android下和iOS下的资源文件，开发iOS的都知道，iOS的图片资源命名为`image_name@2x.png`这种形式，结果有一次执行以下命令的时候就报错了
```
svn delete "bg_check_normal@2x.png"
```
## 解决办法
通过一番谷歌，终于找到了解决办法
{% blockquote Ben Collins-Sussman, SVNBOOK %}
The perceptive reader is probably wondering at this point whether the peg revision syntax causes problems for working copy paths or URLs that actually have at signs in them. After all, how does svn know whether news@11 is the name of a directory in my tree or just a syntax for “revision 11 of news”? Thankfully, while svn will always assume the latter, there is a trivial workaround. You need only append an at sign to the end of the path, such as news@11@. svn cares only about the last at sign in the argument, and it is not considered illegal to omit a literal peg revision specifier after that at sign. This workaround even applies to paths that end in an at sign—you would use filename@@ to talk about a file named filename@.
{% endblockquote %}
即在命令的最后面加上`@`符号
```
svn delete "bg_check_normal@2x.png"@
```