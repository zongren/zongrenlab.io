---
title: OpenVPN代理指定IP
category: miscellaneous
tags: [openvpn]
excerpt: OpenVPN代理指定IP
date: 2019-07-08 14:35:55
---

修改配置文件，注释以下配置
```
#redirect-gateway
```

再添加以下配置
```
route-nopull 
route 192.168.1.254 255.255.255.255
```