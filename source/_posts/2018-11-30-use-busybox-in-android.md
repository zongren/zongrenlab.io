---
title: 在Android系统下使用busybox工具
category: android
tags: [tool,busybox]
excerpt: 在Android系统下使用busybox工具
date: 2018-11-30 10:42:12
---
据busybox[官方介绍](https://busybox.net/about.html)，busybox是嵌入式linux系统里的瑞士军刀，当然也可以在Android系统下使用，其包含了许多linxu下的常用命令，具体可查看[官方文档](https://busybox.net/downloads/BusyBox.html)

第一步，下载busybox
从github上的[busybox-android](https://github.com/Gnurou/busybox-android)下载busybox可执行文件，或者安装脚本

第二步，将busybox可执行文件复制到Android设备

```
adb push busybox-android /system/bin/busybox
```

第三步，添加可执行权限
```
adb shell chmod 777 /system/bin/busybox
```

测试traceroute命令
```
adb shell busybox traceroute 8.8.8.8
```