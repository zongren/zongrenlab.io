---
title: 使用FART查找并替换文件内容
category: miscellaneous
tags: [tool]
date: 2017-03-28 14:55:43
excerpt: Windows下的一款命令行小工具，用于在单个文件或多个文件中查找并替换字符串。
---
下载地址见[官方网站](http://fart-it.sourceforge.net/)
使用方法很简单
```
Usage: FART [options] [--] <wildcard>[,...] [find_string] [replace_string]

Options:
 -h --help          Show this help message (ignores other options)
 -q --quiet         Suppress output to stdio / stderr
 -V --verbose       Show more information
 -r --recursive     Process sub-folders recursively
 -c --count         Only show filenames, match counts and totals
 -i --ignore-case   Case insensitive text comparison
 -v --invert        Print lines NOT containing the find string
 -n --line-number   Print line number before each line (1-based)
 -w --word          Match whole word (uses C syntax, like grep)
 -f --filename      Find (and replace) filename instead of contents
 -B --binary        Also search (and replace) in binary files (CAUTION)
 -C --c-style       Allow C-style extended characters (\xFF\0\t\n\r\\ etc.)
    --cvs           Skip cvs dirs; execute "cvs edit" before changing files
 -a --adapt         Adapt the case of replace_string to found string
 -b --backup        Make a backup of each changed file
 -p --preview       Do not change the files but print the changes
```

例如把文件`a.bat`中的字符串`atom`替换成`stun`
```
fart.exe a.bat atom stun
```