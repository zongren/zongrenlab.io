---
title: 约束应该写在哪
date: 2016-06-07 17:59:23
category: ios
tags: [tip]
---

在对页面上的imageView或者label设置新的属性后，会触发相关事件（因为我不知道），如果把约束写在构造函数里，这个时候就不是很方便调用了。例如项目中用到的一个垂直切换文章的控件
```
//
//  ArticleCell.m
//
//  Created by 宗仁 on 16/6/3.
//

#import "ArticleCell.h"
const NSInteger TITLE_WIDTH = 100;
const NSInteger ICON_WIDTH = 30;
@interface ArticleCell ()
@property (nonatomic, strong) ArticleTitleModel* title;
@property (nonatomic, strong) NSArray<ArticleItemModel*>* items;
@property (nonnull, strong) UIView* container;
@property (nonnull, strong) UIView* dummyContainer;
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UIImageView* iconImageView;
@property (nonatomic, strong) UILabel* articleLabel;
@property (nonatomic, strong) UIImageView* dummyIconImageView;
@property (nonatomic, strong) UILabel* dummyArticleLabel;
@property (nonatomic, assign) NSInteger currentPosition;

@end
@implementation ArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString*)reuseIdentifier
              title:(ArticleTitleModel*)title
           andItems:(NSArray<ArticleItemModel*>*)items {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.title = title;
        self.items = items;
        self.currentPosition = 0;
        ArticleItemModel* firstArticle = [items objectAtIndex:0];
        NSInteger nextIndex = 1;
        if (nextIndex >= items.count) {
            nextIndex = 0;
        }
        ArticleItemModel* secondArticle = [items objectAtIndex:nextIndex];

        self.container = [[UIView alloc] initWithFrame:CGRectZero];
        self.dummyContainer = [[UIView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.container];
        [self.contentView addSubview:self.dummyContainer];

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.titleLabel setText:title.name];
        [self.titleLabel setTextColor:[UIColor colorWithHexString:title.color]];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.titleLabel];

        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        if ([Utils isNullString:firstArticle.article_thumb]) {
            [self.iconImageView setImage:[UIImage imageNamed:@"ic_speaker"]];
        } else {
            [self.iconImageView sd_setImageWithURL:[Utils urlOfImage:firstArticle.article_thumb]];
        }
        [self.container addSubview:self.iconImageView];

        self.articleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.articleLabel.text = firstArticle.title;
        self.articleLabel.textAlignment = NSTextAlignmentLeft;
        [self.container addSubview:self.articleLabel];

        self.dummyIconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        if ([Utils isNullString:secondArticle.article_thumb]) {
            [self.dummyIconImageView setImage:[UIImage imageNamed:@"ic_speaker"]];
        } else {
            [self.dummyIconImageView sd_setImageWithURL:[Utils urlOfImage:secondArticle.article_thumb]];
        }
        [self.dummyContainer addSubview:self.dummyIconImageView];

        self.dummyArticleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.dummyArticleLabel.text = secondArticle.title;
        self.dummyArticleLabel.textAlignment = NSTextAlignmentLeft;
        [self.dummyContainer addSubview:self.dummyArticleLabel];

        self.clipsToBounds = YES;
        [NSTimer scheduledTimerWithTimeInterval:2.0f
                                         target:self
                                       selector:@selector (switchView)
                                       userInfo:self
                                        repeats:YES];
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}
- (void)updateConstraints {
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo (self.contentView.mas_top);
        make.left.equalTo (self.contentView.mas_left);
        make.bottom.equalTo (self.contentView.mas_bottom);
        make.size.width.equalTo (@(TITLE_WIDTH));
    }];
    [self.container mas_remakeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo (self.contentView.mas_top);
        make.left.equalTo (self.titleLabel.mas_right);
        make.bottom.equalTo (self.contentView.mas_bottom);
        make.right.equalTo (self.contentView.mas_right);
    }];
    [self.dummyContainer mas_remakeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo (self.contentView.mas_bottom);
        make.left.equalTo (self.container.mas_left);
        make.right.equalTo (self.container.mas_right);
        make.height.equalTo (self.container.mas_height);
    }];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker* make) {
        make.left.equalTo (self.container.mas_left);
        make.centerY.equalTo (self.container.mas_centerY);
        make.size.width.equalTo (@(ICON_WIDTH));
        make.size.height.equalTo (@(ICON_WIDTH));
    }];
    [self.articleLabel mas_remakeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo (self.container.mas_top);
        make.left.equalTo (self.iconImageView.mas_right);
        make.bottom.equalTo (self.container.mas_bottom);
        make.right.equalTo (self.container.mas_right);
    }];
    [self.dummyIconImageView mas_remakeConstraints:^(MASConstraintMaker* make) {
        make.left.equalTo (self.dummyContainer.mas_left);
        make.centerY.equalTo (self.dummyContainer.mas_centerY);
        make.width.equalTo (@(ICON_WIDTH));
        make.height.equalTo (@(ICON_WIDTH));
    }];
    [self.dummyArticleLabel mas_remakeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo (self.dummyContainer.mas_top);
        make.left.equalTo (self.dummyIconImageView.mas_right);
        make.bottom.equalTo (self.dummyContainer.mas_bottom);
        make.right.equalTo (self.dummyContainer.mas_right);
    }];
    [super updateConstraints];
}

- (void)switchView {
    CGRect dummyContainerFrame = self.dummyContainer.frame;
    CGFloat offset = dummyContainerFrame.size.height;

    [UIView animateWithDuration:1
        animations:^{
            self.container.center = CGPointMake (self.container.center.x, self.container.center.y - offset);
            self.dummyContainer.center =
                CGPointMake (self.dummyContainer.center.x, self.dummyContainer.center.y - offset);
        }
        completion:^(BOOL finished) {
            //动画完成之后交换指针，然后将dummyView重置位置
            //然后给dummyView赋值
            UILabel* label = self.articleLabel;
            self.articleLabel = self.dummyArticleLabel;
            self.dummyArticleLabel = label;

            UIImageView* icon = self.iconImageView;
            self.iconImageView = self.dummyIconImageView;
            self.dummyIconImageView = icon;

            UIView* container = self.container;
            self.container = self.dummyContainer;
            self.dummyContainer = container;
            self.dummyContainer.frame = dummyContainerFrame;
            self.currentPosition++;
            if (self.currentPosition >= self.items.count) {
                self.currentPosition = 0;
            }
            NSInteger nextIndex = self.currentPosition + 1;
            if (nextIndex >= self.items.count) {
                nextIndex = 0;
            }
            ArticleItemModel* nextArticle = [self.items objectAtIndex:nextIndex];

            if ([Utils isNullString:nextArticle.article_thumb]) {
                [self.dummyIconImageView setImage:[UIImage imageNamed:@"ic_speaker"]];
            } else {
                [self.dummyIconImageView sd_setImageWithURL:[Utils urlOfImage:nextArticle.article_thumb]];
            }
            self.dummyArticleLabel.text = nextArticle.title;
            [self setNeedsUpdateConstraints];
        }];
}

@end
```
