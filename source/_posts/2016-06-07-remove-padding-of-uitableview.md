---
title: 移除UITableView的头部和尾部空白
date: 2016-06-07 17:30:42
category: ios
tags: [tip]
---
UITableView默认头部和尾部包含一定的空白区域，通过以下代码可以删除空白区域
```
- (void)viewDidLoad {
    ...
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = false;
    ...
}
...
#pragma mark UITableViewDataSource
- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
```