---
title: ssh免密登录
category: miscellaneous
tags: [linux,ssh]
excerpt: 使用证书密钥实现ssh免密登录
date: 2020-04-17 10:41:43
---

在本地主机终端执行命令
```
ssh-keygen -t rsa -b 2048
>>/Users/zongren/.ssh/root100
>>no password
```

复制证书
```
ssh-copy-id -i /Users/zongren/.ssh/root100 root@192.168.1.100
```

登录
```
ssh -i /Users/zongren/.ssh/root100 root@192.168.1.100
```