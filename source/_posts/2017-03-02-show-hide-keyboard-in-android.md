---
title: Android隐藏和显示键盘
category: android
tags: [keyboard,tip]
date: 2017-03-02 00:06:46
excerpt: Android强制隐藏和显示键盘
---
强制显示键盘
```
private void showKeyBoard(EditText editText) {
    InputMethodManager manager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    manager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
}
```
强制隐藏键盘
```
private void hideKeyboard(EditText editText) {
    InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
    manager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
}
```