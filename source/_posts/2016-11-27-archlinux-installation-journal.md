---
title: ArchLinux安装日志 
date: 2016-11-27 02:46:18 
category: miscellaneous 
tags: [linux] 
excerpt: Archlinux安装日志
---
## 安装时出现黑屏无法启动的情况
在选择U盘作为启动项并选择`boot archlinux x86_64`后，出现了黑屏的现象，找到的临时解决方法是选择`boot`之前，点击F1，然后选择e (dit)，输入`nomodeset`和空格，然后确认即可。

## 分区
因为是安装Windows双系统，所以现在`windows`系统中使用磁盘工具创建一个100G大小的分区，不用格式化。然后在安装`archlinux`的时候使用`fdisk`创建{%raw%}<span style="text-decoration: line-through;">esp和</span>{%endraw%}[^1]系统分区，具体命令如下
```
fdisk /dev/sda
```
{%raw%}<span style="text-decoration: line-through;">可以使用<code>m</code>查看命令说明，首先创建ESP，也就是EFI System Partition</span>{%endraw%}
{%raw%}<div style="text-decoration: line-through;">{%endraw%}
```
fdisk>n
#使用默认number
#使用默认起始sector
#不使用默认结束sector，输入500M
+size500M
#转换格式
fdisk>t
#选择刚才的number
#输入1，然后确认即可
```
{%raw%}</div>{%endraw%}
然后创建系统分区，至于创建几个分区就看自己的情况了，我只创建一个分区，创建流程和上面的类似，就不再赘述。最后格式化分区
```
mkfs.fat -F32 /dev/sda3
mkfs.ext4 /dev/sda4
```

## 网络
如果准备使用无线网络[^2]安装系统的话，我建议将密码验证关闭，不然`wpa_supplicant`有可能提示你驱动不兼容等问题。连接无线网络分为以下几个步骤
### 关闭dhcpcd
```
systemctl stop dhcpcd
```
### 开启无线设备
```
ip link set wlp4s0 up
```
`wlp4s0`是设备的名称，可以通过`ip link`命令查看
### 连接无线网络
```
iw wlp4s0 connect zongren
#检测是否连接成功
iw dev wlp4s0 link
```
### 开启dhcpcd
```
systemctl start dhcpcd
```

## 启动
其实安装的过程还是很顺利的，在启动这一步遇到了很多麻烦，还好最后都解决了，需要注意的是，以下内容仅针对`UEFI`启动模式

### 装载EFI分区
装载EFI分区，并创建arch文件夹
```
mount /dev/sdaX /esp
mkdir /esp/EFI/arch
```
请根据实际情况替换`X`

### 安装systemd-boot
```
bootctl --path=/esp install
```

### 查看partuuid
输入以下命令，并记录archlinux分区对应的`partuuid`
```
ls -l /dev/disk/by-partuuid
```

### 复制启动文件到EFI分区
```
cp -R /boot/. /esp/EFI/arch/
```

### 添加entry
创建arch.conf文件
```
nano /esp/entries/arch.conf
```
写入以下内容
```
title          Arch Linux
linux          /EFI/arch/vmlinuz-linux
initrd         /EFI/arch/initramfs-linux.img
options        root=PARTUUID=YOUR_PARTUUID rw
```
将`YOUR_PARTUUID`替换为刚才记录的值，`options`还可以添加其它参数，例如`nomodeset`
### 修改loader.conf（可选）
创建好entry之后，可以修改`loader.conf`文件，从而设置默认启动项
```
default  arch
timeout  3
editor   0
```

### 卸载EFI分区
```
umount /esp
```

## 备注
[^1]: 如果已经存在ESP（EFI System Partition）分区的话，不需要再创建一个
[^2]: 还可以用USB连接手机，然后使用手机共享的wifi网络
