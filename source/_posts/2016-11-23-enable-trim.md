---
title: 开启第三方固态硬盘trim
category: miscellaneous
tags: [macOS]
date: 2016-11-23 21:16:20
---
在终端输入以下命令
```
sudo trimforce enable
```
重启并检查是否成功开启trim，检查方法为查看`关于本机-系统报告-SATA/SATA Express-TRIM 支持`。