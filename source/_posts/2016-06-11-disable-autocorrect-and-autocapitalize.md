---
title: 关闭自动校正和自动大写
date: 2016-06-11 20:40:46
category: miscellaneous
tags: [web,ios,tip,note]
---

如果想关闭input标签在iOS下的首字母大写和自动校正功能，需要设置以下两个属性
```
<input autocorrect="off" autocapitalize="off" />
```