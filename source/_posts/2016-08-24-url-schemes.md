---
title: iOS中的Url Schemes
date: 2016-08-24 15:36:16
category: ios
tags: [url]
---
iOS应用可以设置自己的Url Scheme，然后其他应用（例如系统浏览器）就可以根据这个scheme打开该应用。举例说明，使用微信分享图片，分享之后微信会提示是否返回原来的应用，点击微信提供的返回键（不是状态栏里的返回）就会回到之前的应用并且调用AppDelegate中的相关方法。微信之所以知道返回到哪个应用就是靠自定义Url Scheme，按照微信的要求，应用需要添加一条Url Scheme，且它的值必须为微信平台申请的应用ID
```
<key>CFBundleURLTypes</key>
<array>
    <dict>
        <key>CFBundleTypeRole</key>
        <string>Editor</string>
        <key>CFBundleURLName</key>
        <string>com.weixin</string>
        <key>CFBundleURLSchemes</key>
        <array>
            <string>wxbd99129723484cf2</string>
        </array>
    </dict>
</array>
```
添加Url Scheme的方法有两个，一个是通过Target的Info标签页，在最下方的URL Types处添加，另外还可以通过info.plist添加。