---
title: 滚动隐藏标题栏
date: 2016-07-27 09:58:58
category: ios
tags: [tutorial]
---
在iOS7以及之前想要实现滚动隐藏标题栏的特性，通常依赖第三方库，如[AMScrollingNavbar](https://github.com/andreamazz/AMScrollingNavbar)。现在iOS8提供了更加方便简洁的属性来完成这个任务，代码如下
```
class UINavigationController : UIViewController {
    ...     
    /// When the keyboard appears, the navigation controller's navigationBar toolbar will be hidden. The bars will remain hidden when the keyboard dismisses, but a tap in the content area will show them.
    @availability(iOS, introduced=8.0)
    var hidesBarsWhenKeyboardAppears: Bool
    /// When the user swipes, the navigation controller's navigationBar & toolbar will be hidden (on a swipe up) or shown (on a swipe down). The toolbar only participates if it has items.
    @availability(iOS, introduced=8.0)
    var hidesBarsOnSwipe: Bool
    /// The gesture recognizer that triggers if the bars will hide or show due to a swipe. Do not change the delegate or attempt to replace this gesture by overriding this method.
    @availability(iOS, introduced=8.0)
    var barHideOnSwipeGestureRecognizer: UIPanGestureRecognizer { get }
    /// When the UINavigationController's vertical size class is compact, hide the UINavigationBar and UIToolbar. Unhandled taps in the regions that would normally be occupied by these bars will reveal the bars.
    @availability(iOS, introduced=8.0)
    var hidesBarsWhenVerticallyCompact: Bool
    /// When the user taps, the navigation controller's navigationBar & toolbar will be hidden or shown, depending on the hidden state of the navigationBar. The toolbar will only be shown if it has items to display.
    @availability(iOS, introduced=8.0)
    var hidesBarsOnTap: Bool
    /// The gesture recognizer used to recognize if the bars will hide or show due to a tap in content. Do not change the delegate or attempt to replace this gesture by overriding this method.
    @availability(iOS, introduced=8.0)
    unowned(unsafe) var barHideOnTapGestureRecognizer: UITapGestureRecognizer { get }
}
```
需要注意的是，这些属性是更改NavigationController的属性，所以会影响整个NavigationController下的所有ViewController的行为。同时一般在```viewDidAppear```方法里更改这些属性，代码如下
```
class QuotesTableViewController: UITableViewController {
 
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBarHidden = false; 
        navigationController?.hidesBarsOnSwipe = true
    }
}
```
