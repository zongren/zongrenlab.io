---
title: Longest Common Prefix
date: 2016-09-06 16:32:21
category: leetcode
tags: [algorithm]
---
## 问题链接
[Longest Common Prefix | LeetCode OJ](https://leetcode.com/problems/longest-common-prefix/)

## 问题描述
在一组字符串中找出最长的公共前缀。


## 解决办法
```
public class Solution {
    public String longestCommonPrefix(String[] strs) {
        if(strs.length < 2){
            if(strs.length < 1){
                return "";
            }
            return strs[0];
        }
        String prefix = strs[0];
        for(int i = 1;i<strs.length;i++){
            String s = strs[i];
            if(s.indexOf(prefix) != 0){
                do{
                    prefix = prefix.substring(0,prefix.length() - 1);
                }while(s.indexOf(prefix) != 0 && prefix.length() > 0);
            }
        }
        
        return prefix;
    }
}
```