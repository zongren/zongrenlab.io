---
title: 使用chmod修改文件权限
category: android
tags: [linux,serial_port]
excerpt: 使用chmod修改文件权限
date: 2018-12-19 10:52:29
---

最近调试串口的时候，需要获取串口权限，不知道是不是跟Android系统版本（Android 7.x）有关

第一步，执行```su```命令
```
Process su = Runtime.getRuntime().exec("su");
```

第二步，获取outputStream
```
DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());
```

第三步，写入命令
```
outputStream.writeBytes("chmod 0777 /dev/ttyUSB0\n");
outputStream.writeBytes("exit\n");
```

第四步，关闭stream
```
outputStream.flush();
outputStream.close();
```

第五步，等待执行结果
```
su.waitFor();
```