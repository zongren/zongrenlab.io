---
title: 观察者模式
date: 2016-08-30 14:32:50
category: development
tags: [design-pattern]
---
观察者模式属于一种软件开发模式（Software Design Pattern），更准确的说，它属于软件开发模式中的行为性模式（Behavioral Patterns）。有一个对象，叫做目标对象或可观察对象，还有一些其他对象，叫附属对象或者观察者，对目标对象保持观察，当目标对象的某些状态发生变化时，目标对象会逐一通知这些附属对象，通常通过调用附属对象的方法来完成通知。一般来说，目标对象会持有所有附属对象的引用并保存在列表中，这样很容易造成内存泄漏，解决办法通常为保持弱引用（如果支持的话）。观察者模式通常用来实现事件处理系统，并且在MVC模式中是非常重要的一部分。下面用Java语言演示观察者模式
```
import java.util.Observable;
import java.util.Scanner;

class EventSource extends Observable implements Runnable {
    public void run() {
        while (true) {
            String response = new Scanner(System.in).next();
            setChanged();
            notifyObservers(response);
        }
    }
}
```

```
import java.util.Observable;
import java.util.Observer;

public class MyApp {
    public static void main(String[] args) {
        System.out.println("Enter Text: ");
        EventSource eventSource = new EventSource();

        eventSource.addObserver(new Observer() {
            public void update(Observable obj, Object arg) {
                System.out.println("Received response: " + arg);
            }
        });

        new Thread(eventSource).start();
    }
}
/*
interface Observer{
    public void update(Observable obj, Object arg);
}*/
```
