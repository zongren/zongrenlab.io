---
title: Java中的不定变量
date: 2016-05-19 13:26:45
category: android
tags: [java,parameter]
---
查看以下代码，检测输入的多个字符串是否都为空。如果全部为空，则返回`true`，如果有至少一个字符串不为空，则返回`false`。
```
public static bool isNull(String... strings){
	for(String string:strings){
		if(string != null){
			return false;
		}
	}
	return true;
}
```
