---
title: 将AFNetworking返回的对象转换为字符串
date: 2016-05-24 21:19
category: ios
tags: [afnetworking,tip,note]
---
如何将`NSData`转换为`NSString`类型，代码如下
```
NSLog(@"%@ %@", response, [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding]);
```