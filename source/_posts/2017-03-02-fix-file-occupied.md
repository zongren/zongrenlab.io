---
title: 解决文件被进程占用的问题。
category: miscellaneous
tags: [tip,pid]
date: 2017-03-02 10:45:57
excerpt: 解决后台进程占用文件，导致该文件不能重命名、移动和删除的问题。
---

解决这个问题很简单，只要关闭占用该文件的进程，找到这些进程最简单的方法是使用*资源监视器*

1. Win + R
2. 输入resmon并运行

打开CPU标签页，输入文件或文件夹名称，等待搜索结果，如图所示
{% raw %}
<div style="overflow-x:auto;overflow-y:hidden;width:100%;"><a href="/images/resource-monitor.PNG" target="_blank"><img src="/images/resource-monitor.PNG" alt="windows-resource-monitor" style="max-width:unset;width:500px;"/></a></div>
{% endraw %}