---
title: 我的Android应用需要改进的地方
date: 2016-10-14 15:13:21
category: android
tags: [best-practice]
---
最近项目快要完事了，总结一下需要改进的地方

## 引入第三方库后要立刻测试
不管是直接导入`jar`文件，还是通过`gradle`添加的依赖，一定要打包`release`版本进行测试。另外建议每隔一段时间就打包release版本进行测试，否则最后项目完成了才发现混淆代码导致的各种问题就太晚了。

## 采用更安全的方式打开Activity
Android官方文档介绍道
{% blockquote Android Developers https://developer.android.com/training/basics/intents/sending.html#StartActivity Sending the User to Another App %}
Here's a complete example that shows how to create an intent to view a map, verify that an app exists to handle the intent, then start it:
{% endblockquote %}
代码如下
```
// Build the intent
Uri location = Uri.parse("geo:0,0?q=1600+Amphitheatre+Parkway,+Mountain+View,+California");
Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

// Verify it resolves
PackageManager packageManager = getPackageManager();
List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
boolean isIntentSafe = activities.size() > 0;

// Start an activity if it's safe
if (isIntentSafe) {
    startActivity(mapIntent);
}
```

## 采用更安全的方式执行fragment transaction
如果在`onSaveInstanceState()`之后调用了`FragmentTransaction#commit()`，Android应用会抛出`java.lang.IllegalStateException`异常。`onSaveInstanceState()`的调用时机如下表所示：

|  | Honeycomb之前 | Honeycomb之后 |
| -- | -------------- | -------------- |
|`onSaveInstanceState()`调用时机 | `onPause()`之前 | `onStop()`之前 |

而对于`android support library`来说，`FragmentTransaction#commit()`的调用时机的影响如下表所示

|  | Honeycomb之前 | Honeycomb之后 |
| -- | -------------- | -------------- |
| 在`onPause()`之前调用`commit()` | 正常 | 正常 |
| 在`onPause()`之后，`onStop()`之前调用`commit()` | 状态丢失 | 正常 |
| 在`onStop()`之后调用`commit()` | 抛出异常 | 抛出异常 |

执行`commit()`的最好时机分别为`onCreate()`，`onResumeFragments()`，`onPostResume()`方法。