---
title: Guess Number Higher or Lower
date: 2016-08-31 16:18:58
category: leetcode
tags: [algorithm]
---
## 问题链接
[Guess Number Higher or Lower | LeetCode OJ](https://leetcode.com/problems/guess-number-higher-or-lower/)

## 问题描述
我们进行猜数字的游戏. 游戏规则如下：
我从1到n里选择一个数字，你来猜我选择的哪个数字。
如果你猜错了的话，我会告诉你我选择的数字比你猜的数字更大还是更小。
使用预先定义好的方法`guess(int num)`，这个方法返回三个数字-1, 1, 或者 0，分别表示：
```
-1 : 我选择的数字比你猜的数字更小
 1 : 我选择的数字比你猜的数字更大
 0 : 恭喜你，你猜中了！
```
举例说明:
```
n = 10，我选择了6，
那么你的方法应该返回6。
```


## 解决方法
给出我的解决方法，语言为Java
```
/* The guess API is defined in the parent class GuessGame.
   @param num, your guess
   @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
      int guess(int num); */

public class Solution extends GuessGame {
    public int guessNumber(int n) {
        if(n == 1){
            return 1;
        }
        else if(n == 2){
            if(guess(1) == 0){
                return 1;
            }
            else{
                return 2;
            }
        }
        else{
            int start = 1;
            int middle = middleNumber(start,n);
            int result;
            while((result = guess(middle)) != 0){
                if(result == 1){
                    start = middle + 1;
                }
                else{
                    n = middle - 1;
                }
                middle = middleNumber(start,n);
            }
            return middle;
        }
    }
    public int middleNumber(int start,int end){
        return (end-start)/2 + start;
    }
}
```