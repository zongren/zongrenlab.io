---
title: 设置输入框光标颜色
date: 2016-05-21 17:18:18
category: android
tags: [edittext,hint,tip,note]
---
EditText有一个属性：android:textCursorDrawable，这个属性是用来控制光标颜色的

```
android:textCursorDrawable="@null"，//"@null"作用是让光标颜色和text color一样
```