---
title: css中的居中显示
date: 2016-09-14 11:48:12
category: miscellaneous
tags: [css,center]
---

css中的居中一向让我头疼，因为看起来我有很多种方式来实现它，但是每一种都不确定是否可行，现在总结一下每种情况下应该使用哪种方式实现

## 水平居中
### 内联元素
如果想要一个内联元素水平居中，并且该内联元素位于块级元素内，可以使用以下方法
```
.center-children {
  text-align: center;
}
```
举例说明
```
<header>
  水平居中的内容。
</header>
<nav role='navigation'>
  <a href="javascript:void(0);">菜单一</a>
  <a href="javascript:void(0);">菜单二</a>
  <a href="javascript:void(0);">菜单三</a>
  <a href="javascript:void(0);">菜单四</a>
</nav>  
```

```
header, nav {
  text-align: center;
  background-color: #666;
  color: #fff;
}
```
{% raw %}
<div class="highlight" style="margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222">
<header style="background-color:#666;color:#fff;text-align:center;">
  水平居中的内容。
</header>
<nav style="margin-top:1em;background-color:#666;color:#fff;text-align:center;">
  <a style="color:#fff;" href="javascript:void(0);">菜单一</a>
  <a style="color:#fff;" href="javascript:void(0);">菜单二</a>
  <a style="color:#fff;" href="javascript:void(0);">菜单三</a>
  <a style="color:#fff;" href="javascript:void(0);">菜单四</a>
</nav>
</div>
{% endraw %}
以上方法同样对inline-block、inline-table、inline-flex等有效。

### 块级元素
如果想把一个块级元素设置为水平居中，首先需要设定宽度（不设定宽度的话，宽度为父级元素的宽度，也就没必要水平居中了），然后将`margin-left`和`margin-right`属性设置为`auto`即可实现水平居中
```
<div>
  <div class="center">
    这是一个块级元素，并且水平居中
  </div>
</div>
```

```
.center {
  width: 300px;
  margin-left: auto;
  margin-right: auto;
  background: black;
  color: #fff;
  background-color: #666;
  text-align: center;
}
```
{% raw %}
<div class="highlight" style="margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222">
  <div style="width: 300px;margin-left: auto;margin-right: auto;background: black;color: #fff;
background-color: #666;text-align:center;">
    这是一个块级元素，并且水平居中
  </div>
</div>
{% endraw %}

### 多个块级元素
如果想要多个块级元素在同一行内水平居中，那么就必须更改`display`属性为`inline-block`，或者将父元素的`display`属性设置为`flex`
```
<div class="inline-block-center">
  <div>
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
  <div>
    I'm an element that is block-like with my siblings and we're centered in a row. I have more content in me than my siblings do.
  </div>
  <div>
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
</div>

<div class="flex-center">
  <div>
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
  <div>
    I'm an element that is block-like with my siblings and we're centered in a row. I have more content in me than my siblings do.
  </div>
  <div>
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
</div>
```

```
div div {
  background-color:#666;
  color:#fff;
  max-width:125px;
}
.inline-block-center {
  text-align: center;
}
.inline-block-center div {
  display: inline-block;
  margin-top: 5px;
}
.flex-center {
  display: flex;
  justify-content: center;
}
.flex-center div{
  margin-left: 5px;
}
```

{% raw %}
<div class="highlight" style="margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;text-align:center;">
  <div style="background-color:#666;color:#fff;max-width:125px; display: inline-block;margin-top: 5px;">
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
  <div style="background-color:#666;color:#fff;max-width:125px; display: inline-block;margin-top: 5px;">
    I'm an element that is block-like with my siblings and we're centered in a row. I have more content in me than my siblings do.
  </div>
  <div style="background-color:#666;color:#fff;max-width:125px; display: inline-block;margin-top: 5px;">
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
</div>
{% endraw %}

{% raw %}
<div class="highlight" style="margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;display: flex;justify-content: center;">
  <div style="background-color:#666;color:#fff;max-width:125px; display: inline-block;margin-left: 5px;">
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
  <div style="background-color:#666;color:#fff;max-width:125px; display: inline-block;margin-left: 5px;">
    I'm an element that is block-like with my siblings and we're centered in a row. I have more content in me than my siblings do.
  </div>
  <div style="background-color:#666;color:#fff;max-width:125px; display: inline-block;margin-left: 5px;">
    I'm an element that is block-like with my siblings and we're centered in a row.
  </div>
</div>
{% endraw %}
如果多个块级元素不需要在同一行水平居中，那么设置宽度和`margin-left`和`margin-right`属性即可，就像这样
{% raw %}
<div class="highlight" style="margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;">
  <div style="background-color:#666;color:#fff;max-width:100px;margin-left: auto;margin-right:auto;margin-top: 5px;">
    I'm an element that is block-level.
  </div>
  <div style="background-color:#666;color:#fff;max-width:200px;margin-left: auto;margin-right:auto;margin-top: 5px;">
    I'm an element that is block-level.
  </div>
  <div style="background-color:#666;color:#fff;max-width:150px;margin-left: auto;margin-right:auto;margin-top: 5px;">
    I'm an element that is block-level.
  </div>
</div>
{% endraw %}
## 垂直居中
### 内联元素
如果一个内联元素没有设置高度，那么它的高度应该是内容的高度，不存在居中不居中的问题，如果想增加它的高度，可以使用`padding-top`和`padding-bottom`属性，正好可以满足垂直居中的要求。
如果一个内联元素设置了高度，那么需要同时设置`line-height`来实现垂直居中
```
.center-text-trick {
  height: 100px;
  line-height: 100px;
  white-space: nowrap;
}
```

### 块级元素
#### 确定高度的块级元素
如果子元素（需要垂直居中的元素）都已经确定了它的高度，那么可以使用下面的方法实现垂直居中，注意父元素是确定高度而且高度大于子元素（如果不大于子元素，也就没有居中这个说法了）。
```
.parent {
  position: relative;
  height: 200px;
}
.child {
  position: absolute;
  top: 50%;
  height: 100px;
  margin-top: -50px;
  background-color:#666;
  color:#fff;
}
```

{% raw %}
<div class="highlight" style="position:relative;margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;height:200px">
   <div style="position: absolute;top: 50%;height: 100px;margin-top: -50px;background-color:#666;color:#fff;">
     I'm a block-level element with a fixed height, centered vertically within my parent.
  </div>
</div>
{% endraw %}
#### 不确定高度的块级元素
如果该元素的高度不确定，就使用下面的方法
```
.parent {
  position: relative;
  height: 200px;
}
.child {
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  background-color:#666;
  color:#fff;
}
```

{% raw %}
<div class="highlight" style="position:relative;margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;height:200px">
   <div style="position: absolute;top: 50%;transform: translateY(-50%);background-color:#666;color:#fff;">
     I'm a block-level element without a fixed height, centered vertically within my parent.
  </div>
</div>
{% endraw %}

#### 使用flexbox
无论是否确定了高度，如果可以使用flexbox的话，就可以使用以下方法实现垂直居中。
```
.parent {
  display: flex;
  flex-direction: column;
  justify-content: center;
}
```

{% raw %}
<div class="highlight" style="display:flex;flex-direction: column;justify-content: center;margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;height:200px">
   <div style="background-color:#666;color:#fff;">
     I'm a block-level element without a fixed height, centered vertically within my parent.
  </div>
</div>
{% endraw %}

{% raw %}
<div class="highlight" style="display:flex;flex-direction: column;justify-content: center;margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;height:200px">
   <div style="background-color:#666;color:#fff;height:100px;">
     I'm a block-level element with a fixed height, centered vertically within my parent.
  </div>
</div>
{% endraw %}
## 水平和垂直居中
组合使用以上的方法，基本能解决大部分需要同时水平和垂直居中的需求。
### 确定高度和宽度的
如果元素的高度和宽度已经确定了，使用以下方法即可。
```
.parent {
  position: relative;
  height: 200px;
}
.child {
  position: absolute;
  width: 150px;
  height: 150px;
  top: 50%;
  left: 50%;
  margin-top: -75px;
  margin-left: auto;
  margin-right: auto;
  background-color:#666;
  color:#fff;
}
```

{% raw %}
<div class="highlight" style="position:relative;margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;height:200px">
   <div style="position: absolute;
  width: 150px;
  height: 150px;
  top: 50%;
  left: 50%;
  margin-top: -75px;
  margin-left: -75px;
  background-color:#666;
  color:#fff;">
     I'm a block-level element with a fixed height and a fixed width, centered vertically within my parent.
  </div>
</div>
{% endraw %}
### 不确定高度和宽度的
如果该元素的高度不确定，就使用下面的方法
```
.parent {
  position: relative;
  height: 200px;
}
.child {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color:#666;
  color:#fff;
}
```

{% raw %}
<div class="highlight" style="position:relative;margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;height:200px">
   <div style="position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color:#666;
  color:#fff;">
     I'm a block-level element without a fixed height and width, centered vertically and horizontally within my parent.
  </div>
</div>
{% endraw %}
### 使用flexbox
如果可以使用flexbox，参考以下代码即可
```
.parent {
  display: flex;
  justify-content: center;
  align-items: center;
}
.child{
  width: 50%;
}
```

{% raw %}
<div class="highlight" style="display:flex;align-items: center;justify-content: center;margin-top:1em;padding-top:1em;padding-bottom:1em;background-color:#222;height:200px">
   <div style="background-color:#666;color:#fff;width:50%">
     I'm a block-level element without a fixed height, centered vertically within my parent.
  </div>
</div>

{% endraw %}


