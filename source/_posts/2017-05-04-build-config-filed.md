---
title: 在build.gradle定义变量
category: android
tags: [tip]
excerpt: 在build.gradle中定义变量
date: 2017-05-04 14:38:55
---

如果你的项目具有不同的版本，例如测试版、稳定版、免费版、收费版，而这些版本的代码是相同的，只不过有些变量不同，这时一般都是通过`buildConfigField`方法实现相同的代码打包出不同的apk，详细说明可以参考官方文档：[配置构建](https://developer.android.com/studio/build/index.html?hl=zh-cn)

例如你的项目对应两个服务器，那么只需要按照以下内容配置
```
buildTypes {
    debug {
        buildConfigField "String", "ENDPOINT", "\"http://exampleDebug.com\""
    }
    release {
        minifyEnabled false
        proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        signingConfig signingConfigs.demo
    }
    releaseA {
        initWith release
        buildConfigField "String", "ENDPOINT", "\"http://exampleA.com\""
    }
    releaseB {
        initWith release
        buildConfigField "String", "ENDPOINT", "\"http://exampleB.com\""
    }
}
```

打包的时候选择`assembleReleaseA`和`assembleReleaseB`即可
{% img /images/build-config.png build-config %}

A simple demo:[BuildConfigDemo](https://github.com/zongren/BuildConfigDemo)