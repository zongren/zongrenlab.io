---
title: 菜单显示完整名称
category: android
tags: [menu,compatible]
date: 2016-11-08 09:18:14
excerpt: 菜单显示完整名称
---
使用`Android support library`后，如果要显示菜单的名称，需要使用以下方式
```
<?xml version="1.0" encoding="utf-8"?>
<menu
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    >
    <item
        android:id="@+id/business_hour_save"
        android:title="@string/menuSave"
        app:showAsAction="always"
        tools:ignore="AppCompatResource"/>
</menu>
```