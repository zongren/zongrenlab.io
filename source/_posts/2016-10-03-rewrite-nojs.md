---
title: 重写nojs主题
date: 2016-10-03 13:37:43
category: miscellaneous
tags: [nojs,css]
---
最近花时间重写了nojs主题，从整体布局和页面细节都做了优化，对css代码也进行了一些优化，并且还使用了[icomoon](https://icomoon.io/)制作了图标。新版主题看起来棱角更加明显，风格更加突出，同时对于小屏设备做了处理，也算是响应式设计了吧。新版主题可以在[hexo-theme-nojs/modern](https://github.com/zongren/hexo-theme-nojs/tree/modern)上找到，同时旧版主题仍然保留。新版主题不再支持IE9以下的浏览器，后期还会加入更多的CSS3动画效果。