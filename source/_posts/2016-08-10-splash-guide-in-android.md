---
title: Android下启动图和引导图思路
date: 2016-08-10 16:20:12
category: android
tags: [note]
---
## 引导图说明
所谓启动图是指，在启动APP的时候经常会有几秒（根据手机和应用而不同）的间隔，这个时候屏幕显示空白，比较难看，如果能用这段时间显示自己公司的品牌图或者推广图，是非常有用的，一是避免了尴尬的空白画面，二是让这几秒钟产生价值。
## 引导图实现
### 创建drawable文件`activity_splash_baground.xml`
```
<layer-list xmlns:android="http://schemas.android.com/apk/res/android"
            android:opacity="opaque">
    <item android:drawable="@android:color/white"/>
    <item>
        <bitmap
            android:gravity="center"
            android:src="@mipmap/ic_launcher"/>
    </item>
</layer-list>
```

如果要把服务器端的图片作为启动图，需要预先下载好这些文件，保证断网或者网速较低的时候App能正常显示。具体实现方法和本文类似（主题采用默认主题，即没有背景图的主题，然后在Acitivty中调用setContentView方法），不再赘述。
### 创建引导图专用的主题`themeSplash`
```
<style name="themeSplash" parent="Theme.AppCompat.NoActionBar">
    <item name="android:windowBackground">@drawable/activity_splash_baground</item>
</style>
```
可以根据自己的需求，更改父主题。
### 创建SplashActivity并声明为启动Acitivty
```
<activity
    android:name=".Activity.SplashActivity"
    android:theme="@style/themeSplash">
    <intent-filter>
        <action android:name="android.intent.action.MAIN"/>

        <category android:name="android.intent.category.LAUNCHER"/>
    </intent-filter>
</activity>
```
### 判断是否为第一次打开App
在SplashActivity中判断是否为第一次打开App，如果是，则打开引导图，否则直接打开主界面。代码如下
```
public class SplashActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean isUsed = Utils.getBoolFromSharedPreferences(getApplicationContext(), Key.IS_USED.toString());
        if (isUsed) {
            openRootActivity();
        } else {
            RequestQueue requestQueue = NoHttp.newRequestQueue(1);
            StringRequest request = new StringRequest(Api.API_APP_GUIDE, RequestMethod.GET);
            request.setUserAgent("szyapp/ios");
            requestQueue.add(HttpWhat.GUIDE.getValue(), request, this);
        }
    }
}
```
## 引导图实现
引导图一般使用ViewPager实现，最后打开RootActivity，也就是你的应用的主界面即可。