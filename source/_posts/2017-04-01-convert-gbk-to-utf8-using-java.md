---
title: 将文件从GBK格式批量转换为UTF-8格式
category: miscellaneous
tags: [java,encode]
date: 2017-04-01 10:07:53
excerpt: 使用java运行环境和第三方jar文件，将项目中所有的java文件批量转为UTF-8格式。
---

我所用的jar文件是GitHub上的[gbk2utf8](https://github.com/downgoon/gbk2utf8)，使用方法非常简单：
```
C:\Users\zongren\AndroidStudioProjects>java -jar C:\Users\zongren\Downloads\gbk2utf8-0.1.1-SNAPSHOT-all.jar  Android C:\Users\zongren\Downloads\
```

转换之后的文件会按照原文件目录保存在`C:\Users\zongren\Downloads\`目录下，手动覆盖源文件即可。