---
title: JUnit运行出错
category: miscellaneous
tags: [junit,test]
excerpt: junit-class-not-found
date: 2019-02-23 09:21:52
---
在MacBook上成功运行的一个项目的JUnit测试代码，结果在Windows系统上
运行JUnit测试时，提示以下错误
```
java.lang.NoClassDefFoundError: org/apache/http/entity/mime/content/ContentBody
```

但是这个库肯定是已经依赖了的，解决方法是将implementation换成api

```
dependencies {
    api 'org.apache.httpcomponents:httpclient:4.0-beta1'
    api 'org.apache.httpcomponents:httpcore:4.0-beta2'
    api 'org.apache.httpcomponents:httpmime:4.0-beta2'
}
```