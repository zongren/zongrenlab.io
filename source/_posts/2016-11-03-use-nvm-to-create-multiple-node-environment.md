---
title: 使用nvm创建多版本运行环境
category: miscellaneous
tags: [node,tutorial]
date: 2016-11-03 08:43:48
excerpt: 简单记录一下使用nvm创建多版本node运行环境，和一些注意事项。
---
我经常在电脑上运行一些`nodejs`项目，例如hexo博客系统和一些插件，而这些插件的兼容性参差不齐，一个单一的运行环境无法满足我的需求，这时就难免需要多个nodejs版本，而对于这种情况，[nvm](https://github.com/creationix/nvm)是最好的选择，nvm的全称就是`node version manager`。首先介绍一下如何使用nvm
第一步，下载nvm
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
```
也可以使用`wget`命令下载
```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
```
第二步，然后添加环境变量，我使用的是macOS系统，在`~/.bash_profile`文件中添加以下代码
```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
```
然后执行`source ~/.bash_profile`就可以了。
第三步，安装多版本nodejs，如果你不清楚都有哪些版本，可以使用`nvm ls-remote`查看所有版本
```
zongrendeMacbookPro15:zongren.gitlab.io zongren$ nvm ls-remote
         ...
         v4.0.0
         v4.1.0
         v4.1.1
         v4.1.2
         v4.2.0   (LTS: Argon)
         v4.2.1   (LTS: Argon)
         v4.2.2   (LTS: Argon)
         v4.2.3   (LTS: Argon)
         v4.2.4   (LTS: Argon)
         v4.2.5   (LTS: Argon)
         v4.2.6   (LTS: Argon)
         v4.3.0   (LTS: Argon)
         v4.3.1   (LTS: Argon)
         v4.3.2   (LTS: Argon)
         v4.4.0   (LTS: Argon)
         v4.4.1   (LTS: Argon)
         v4.4.2   (LTS: Argon)
         v4.4.3   (LTS: Argon)
         v4.4.4   (LTS: Argon)
         v4.4.5   (LTS: Argon)
         v4.4.6   (LTS: Argon)
         v4.4.7   (LTS: Argon)
         v4.5.0   (LTS: Argon)
         v4.6.0   (LTS: Argon)
->       v4.6.1   (Latest LTS: Argon)
         v5.0.0
         v5.1.0
         v5.1.1
         v5.2.0
         v5.3.0
         v5.4.0
         v5.4.1
         v5.5.0
         v5.6.0
         v5.7.0
         v5.7.1
         v5.8.0
         v5.9.0
         v5.9.1
        v5.10.0
        v5.10.1
        v5.11.0
        v5.11.1
        v5.12.0
         v6.0.0
         v6.1.0
         v6.2.0
         v6.2.1
         v6.2.2
         v6.3.0
         v6.3.1
         v6.4.0
         v6.5.0
         v6.6.0
         v6.7.0
         v6.8.0
         v6.8.1
         v6.9.0   (LTS: Boron)
         v6.9.1   (Latest LTS: Boron)
         v7.0.0

```
安装指定的版本，安装完成之后，查看`~/.nvm/versions/node/`文件夹，发现里面多了一个`v7.0.0`的文件夹。
```
nvm install v7.0.0
nvm ls //列出本地所有的版本
```
第四步，切换nodejs版本
```
nvm use v4.6.1
node -v
nvm use v7.0.0
node -v
```

在这个过程中我发现一些需要注意的地方，首先是nvm不会识别之前安装的node，一定要通过`nvm install`命令安装才可以。另外一点就是国内的下载速度非常差，`nvm install`命令经常执行失败，这时你需要到[nodejs官网](https://nodejs.org/dist/)下载你想安装的版本，解压缩，然后复制到`~/.nvm/versions/node/`文件夹中，同样可以使用nvm切换。我下载的是`node-v4.6.1-darwin-x64.tar.gz`。