---
title: 使用npm-check-updates插件更新package.json文件
category: miscellaneous
tags: [npm]
date: 2017-04-06 09:39:56
excerpt: 升级package.json中的依赖时，官方提供的npm update和npm outdated命令不够方便，使用npm-check-updates能够快速检测需要升级的依赖并直接覆盖package.json文件。
---
使用方法非常简单
```
npm i -g npm-check-updates
ncu -u
```

升级之后会提示哪些依赖发生了变化，例如
```
 bluebird         3.4.1  →   3.5.0
 ms               0.7.1  →   1.0.0
 request-promise  4.0.0  →   4.2.0
 yargs            4.8.1  →   7.0.2
 eslint           3.1.0  →  3.19.0
```