---
title: git拉取指定的文件夹     
category: miscellaneous
tags: [git,github]
date: 2016-10-20 14:42:09
---
如果只要拉取git仓库的指定`文件夹`（注意不是文件），请参考以下代码，以下代码执行完毕后，只会拉取`/languages/`文件夹，亲测可用，`windows`下的用户请酌情修改代码（主要是echo）。
```
mkdir hexo-theme-nojs
cd hexo-theme-nojs
git init
git remote add origin https://github.com/zongren/hexo-theme-nojs.git
git config core.sparsecheckout true
echo /languages/ >> .git/info/sparse-checkout
git pull --depth=1 origin master
```
P.S. github不支持`git archive`命令