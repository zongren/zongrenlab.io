---
title: 使用android module灵活打包
category: android
tags: [module,gradle]
excerpt: 单应用多业务场景下的灵活打包方法，使用android module和gradle脚本
date: 2018-01-16 22:47:45
---

最近开发企业应用的时候遇到了一个问题，企业A需要A功能，企业B和企业C等不需要A功能，这种时候有两种解决办法

1、创建分支
在主版本的基础上创建分支，这样可以针对企业A进行定制开发，而企业B和C继续使用主版本不受影响。
这么做带来的麻烦就是一旦主业务发生更改，需要同步到企业A的分支上

2、使用module分离A功能
将A功能放在一个独立的module中，然后使用不同的gradle脚本（其实只是一句话的区别）给A、B、C企业打包
这么做的好处是更新主业务不会影响A功能的打包，A功能也可以独立更新，同时把A功能的相关文件（jar、so等）独立出来，减少主项目的复杂程度

demo on GitHub：[android-module](https://github.com/zongren/androidmodule)