---
title: 无法使用gradle管理的第三方库文件
category: miscellaneous
tags: [library,android]
date: 2016-12-03 13:36:32
excerpt: nothing important.
---
## 腾讯信鸽
### 下载地址
[官网](http://xg.qq.com/xg/ctr_index/download)

### jar文件
* Xg_sdk_vx.xx_xxxxxxxx_xxxx.jar
* jg_filter_sdk_x.x.jar
* wup-x.x.x.E-SNAPSHOT.jar

### so文件
* libtpnsSecurity.so 
* libxguardian.so

### 代码混淆：
```
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep class com.tencent.android.tpush.**  {* ;}
-keep class com.tencent.mid.**  {* ;}
```

## 高德地图

### 下载地址
[官网](http://lbs.amap.com/api/android-sdk/download/)

### jar文件：
* AMap_Location_V3.1.0_20161027.jar
* AMap_Search_V3.6.1_20161122.jar
* Amap_2DMap_V2.9.2_20161026.jar
* Android_Map3D_SDK_V4.1.2_20161104.jar
* AMapNavi_1.9.2_20160830.jar
* Msc.jar

### so文件：
* libgdinamapv4sdk752.so
* libgdinamapv4sdk752ex.so
* libmsc.so
* libtbt3631.so
* libwtbt145.so

## 新浪微博

### 下载地址
[GitHub](https://github.com/sinaweibosdk/weibo_android_sdk)

### jar文件
* weiboSDKCore_3.1.4.jar

### so文件
* libweibosdkcore.so

## 银联

### 下载地址：
[官网](https://open.unionpay.com/ajweb/help/file/toDetailPage?id=403&flag=2)

### jar文件
* UPPayPluginExPro.jar
* UPPayAssistEx.jar

### so文件
* libentryexpro.so
* libuptsmaddon.so

### bin文件
data.bin需要放置在`src/main/assets/`

## 支付宝

### 下载地址
[官网](https://doc.open.alipay.com/doc2/detail.htm?treeId=54&articleId=104509&docType=1)

### jar文件
* alipaySingle-20161129.jar（不包含UTDID）
* alipaySdk-20161129.jar（包含UTDID）

## QQ

### 下载地址
[官网](http://wiki.open.qq.com/wiki/mobile/SDK%E4%B8%8B%E8%BD%BD)

### jar文件
* mta-sdk-1.6.2.jar
* open_sdk_r5756.jar

## 微信

### 下载地址
[官网](https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1419319167&token=&lang=zh_CN)

### jar文件
* libammsdk.jar
