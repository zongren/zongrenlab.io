---
title: 使用ProgressBar实现下载动画
category: android
tags: [progressbar]
date: 2017-03-21 14:05:34
excerpt: 使用ProgressBar实现下载（或上传）动画
---
想要实现类似这种<img src="/images/download.gif" alt="download.gif" title="download animation" />效果的下载动画（即最后实现的效果）。
解决方法来自[StackOverflow](http://stackoverflow.com/a/8277231)
将以下内容复制到`style.xml`文件中
```
<style name="Widget"></style>

<style name="Widget.ProgressBar">
    <item name="android:indeterminateOnly">true</item>
    <item name="android:indeterminateBehavior">repeat</item>
    <item name="android:indeterminateDuration">3500</item>
    <item name="android:minWidth">40dp</item>
    <item name="android:maxWidth">40dp</item>
    <item name="android:minHeight">40dp</item>
    <item name="android:maxHeight">40dp</item>
</style>

<style name="Widget.ProgressBar.Vertical">
    <item name="android:indeterminateOnly">false</item>
    <item name="android:progressDrawable">@drawable/background_download</item>
    <item name="android:indeterminateDrawable">
        @android:drawable/progress_indeterminate_horizontal
    </item>
    <item name="android:minWidth">1dip</item>
    <item name="android:maxWidth">12dip</item>
</style>
```

`background_download`文件内容如下所示
```
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:id="@android:id/background">
        <bitmap
            android:gravity="center"
            android:src="@mipmap/icon_download_1"
            android:tileMode="disabled"
            />
    </item>
    <item android:id="@android:id/secondaryProgress">
        <clip
            android:clipOrientation="vertical"
            android:gravity="top">
            <bitmap android:src="@mipmap/icon_download_9" />
        </clip>
    </item>
    <item android:id="@android:id/progress">
        <clip
            android:clipOrientation="vertical"
            android:gravity="top">
            <bitmap android:src="@mipmap/icon_download_9" />
        </clip>
    </item>
</layer-list>
```

使用方法如下
```
<ProgressBar
    style="@style/Widget.ProgressBar.Vertical"
    android:layout_width="40dp"
    android:layout_height="40dp"
     />
```

`icon_download_1`的分辨如果太低，会出现平铺的情况，并且在`xml`中设置`tileMode`无效，另外可以通过给ProgressBar设置background属性实现
```
<ProgressBar
    style="@style/Widget.ProgressBar.Vertical"
    android:layout_width="40dp"
    android:background="@mipmap/icon_download_1"
    android:layout_height="40dp"
     />
```