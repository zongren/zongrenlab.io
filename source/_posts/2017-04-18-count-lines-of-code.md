---
title: 统计代码量
category: miscellaneous
tags: [android-studio]
excerpt: 在Android Studio中统计代码量
date: 2017-04-18 17:09:43
---

打开插件搜索界面（Settings->Plugins->Browse repositories），搜索`Statistic`（[官网](https://plugins.jetbrains.com/plugin/4509-statistic)）

安装之后需要重启Android Studio

点击左下角的Statistic，然后点击Refresh即可统计当前项目的代码量

{% img /images/statistic.png statistic-in-android-studio %}