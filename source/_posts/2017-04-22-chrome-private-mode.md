---
title: Chrome隐私模式
category: miscellaneous
tags: [chrome]
excerpt: Chrome隐私模式
date: 2017-04-22 16:03:55
---
右键打开属性
{% img /images/chrome.png chrome %}

在目标一栏添加` -incognito`，这样每次打开Chrome都自动进入隐私模式