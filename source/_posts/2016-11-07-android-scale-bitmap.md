---
title: 如何更改Bitmap的尺寸
category: android
tags: [bitmap]
date: 2016-11-07 11:13:41
excerpt: 如何更改Bitmap的尺寸
---

```
/**
 * Shrink image.
 *
 * @param bitmap    The bitmap need to be scaled.
 * @param maxWidth  The max width.
 * @param maxHeight The max height.
 * @return new Bitmap.
 */
public static Bitmap shrinkImage(Bitmap bitmap, int maxWidth, int maxHeight) {
    int photoWidth = bitmap.getWidth();
    int photoHeight = bitmap.getHeight();
    if (photoHeight <= maxHeight && photoWidth <= maxWidth) {
        return bitmap;
    }

    float scaleWidth = ((float) maxWidth) / photoWidth;
    float scaleHeight = ((float) maxHeight) / photoHeight;
    Matrix matrix = new Matrix();
    matrix.postScale(scaleWidth, scaleHeight);
    Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, photoWidth, photoHeight, matrix,
            false);
    bitmap.recycle();
    return resizedBitmap;
}
```