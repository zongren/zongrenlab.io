---
title: 修复“Conflict with dependency”的错误
category: miscellaneous
tags: [android,android-studio,gradle,dependency]
excerpt: 修复“Conflict with dependency”的错误
date: 2017-04-19 15:14:55
---
在build.gradle文件中添加若干个依赖后出现以下问题，字面意思是测试app和正式app使用了不同版本的依赖
```
Error:Conflict with dependency 'com.google.code.findbugs:jsr305' in project ':app'. Resolved versions for app (3.0.1) and test app (2.0.1) differ. See http://g.co/androidstudio/app-test-app-conflict for details.
```

解决办法有以下几种

### 统一依赖版本
可以通过两种办法实现
```
android{
    configurations.all {
        resolutionStrategy.force 'com.google.code.findbugs:jsr305:3.0.1'
    }
}
```

或者
```
dependencies {
    compile('com.google.code.findbugs:jsr305:3.0.1')
    testCompile('com.google.code.findbugs:jsr305:3.0.1')
    androidTestCompile('com.google.code.findbugs:jsr305:3.0.1')
}
```

### 阻止（exclude）

```
dependencies {
    compile 'com.google.dagger:dagger-android-support:2.10',{
        exclude group:'com.google.code.findbugs'
    }
}
```