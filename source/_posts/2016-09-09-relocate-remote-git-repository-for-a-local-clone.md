---
title: 更改Git库的地址
date: 2016-09-09 14:28:02
category: miscellaneous
tags: [git,version-control]
---
如果有个git库更改了它的地址，而你在本地有一个克隆，又不想另外新建一个克隆，或者还有`commit`没有`push`，这时就需要更改本地克隆的origin。
首先查看一下当前的origin地址，如果地址已经不存在了会有提示
```
$ git remote show origin
* remote origin
URL: https://gitlab.com/zongren.blog.git
Remote branch(es) merged with 'git pull' while on branch master
master
Tracked remote branches
master
```

接下来就是删除旧的origin地址，添加新地址
```
$ git remote rm origin
$ git remote add origin git@newserver:project.git
$ git remote show origin
* remote origin
URL: git@newserver:project.git
Remote branch(es) merged with 'git pull' while on branch master
master
error: refs/remotes/origin/HEAD points nowhere!
New remote branches (next fetch will store in remotes/origin)
master
```

然后测试一下，是否更换成功
```
git pull                                                 
Already up-to-date.
```
有时会出现以下错误
```
There is no tracking information for the current branch.
    Please specify which branch you want to merge with.
    See git-pull(1) for details

    git pull <remote> <branch>

If you wish to set tracking information for this branch you can do so with:

    git branch --set-upstream develop origin/<branch>
```
解决办法很简单
```
git branch --set-upstream-to=origin/master master
```
    