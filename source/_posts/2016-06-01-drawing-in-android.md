---
title: 安卓绘图
date: 2016-05-31 23:40:59
category: android
tags: [draw,view,layout]
---

## 函数调用顺序

首先调用onMeasure函数，用来获取当前View的宽和高，之后调用onLayout函数对Children进行布局，最后调用onDraw函数，进行绘制。