---
title: 生成系统keys
category: android
tags: [aosp,build,system,platform]
excerpt: 开发系统镜像，发布系统镜像，如何创建自定义系统keys
date: 2019-07-23 16:37:22
---
生成Android系统镜像所需的若干证书文件，参考

[Signing Builds for Release](https://source.android.com/devices/tech/ota/sign_builds)
[Downloading the Source](https://source.android.com/setup/build/downloading)
[清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/help/AOSP/)
[make_key](https://android.googlesource.com/platform/development/+/master/tools/make_key)
[Keystores](http://jmlinnik.blogspot.com/2011/12/keystores.html)

经测试macOS在openssl调用上出了点问题，在Linux发行版上成功执行，make_key脚本执行完毕后，会生成8个文件，
分别是```releasekey```,```platform```,```shared```,```media```，替换aosp对应的文件并打包