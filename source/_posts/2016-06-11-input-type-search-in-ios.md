---
title: iOS下input的type为search时键盘样式不变
date: 2016-06-11 20:37:01
category: miscellaneous
tags: [web,ios,tip,note]
---

一般来说将`input`标签的`type`属性设置为`search`的话，在手机上弹出的键盘，会自动将Enter键显示为`搜索`，如果在iOS下还需要加入`form`表单中才会显示搜索按钮。