---
title: 使用SikulixIDE和百度网盘PC版实现批量下载功能
category: miscellaneous 
tags: [pc,tool,sikulix] 
excerpt: 使用SikulixIDE和百度网盘PC版实现批量下载功能 
date: 2017-10-13 18:18:19 
---

{% img /images/sikulix.png sikulix_screen_capture %}

从[官方网站](https://launchpad.net/sikuli)下载`Sikulix`安装文件，需要`JRE`进行安装

下载脚本文件：[PanDownload.sikuli](/PanDownload.sikuli.zip)

解压缩脚本文件到任意目录，使用SikulixIDE打开脚本

打开百度云盘，并跳转到想要下载的目录，保持百度云盘显示在桌面中（也就是不能最小化）

将需要下载的链接保存在seed.txt文件中，使用换行隔开，例如
```
ed2k://|file|凶鬼恶灵.Supernatural.S12E01.中英字幕.WEB-HR.AAC.1024X576.x264.mp4|418957200|888e0a8131012406c461bee893c7a853|h=qphq5fh4pjsshn4lcj3hcnozgiassha5|/
ed2k://|file|凶鬼恶灵.Supernatural.S12E02.中英字幕.WEB-HR.AAC.1024X576.x264.mp4|421619283|dc43884c868f8caf49683cfd0435bf79|h=mglilwan3f2ee5mmy2i36h4e5hh6llcv|/
ed2k://|file|凶鬼恶灵.Supernatural.S12E03.中英字幕.WEB-HR.AAC.1024X576.x264.mp4|422560845|40dae4eb442262fffc5be82828d3a6f0|h=bt4wjsuywdrh7n2tq6cazrpn7ohp2zyx|/
ed2k://|file|凶鬼恶灵.Supernatural.S12E04.中英字幕.WEB-HR.AAC.1024X576.x264.mp4|422797358|95465e8cc4980d2a6e5fc4f29b2c09bb|h=itup4ba7s4pvl27c5jbjhedmraiak3ve|/
```
将seed.txt放置在SikulixIDE目录下，然后运行脚本即可