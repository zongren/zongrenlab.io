---
title: ConstraintLayout示例
category: android
tags: [layout]
excerpt: ConstraintLayout示例
date: 2017-04-25 17:15:16
---

### 配置build.gradle文件
```
dependencies {
    compile 'com.android.support.constraint:constraint-layout:1.0.1'
}
```

### 官方文档
官方文档：[Build a Responsive UI with ConstraintLayout](https://developer.android.com/training/constraint-layout/index.html)，[Reference](https://developer.android.com/reference/android/support/constraint/ConstraintLayout.html)

### Demo

[GitHub](https://github.com/zongren/ConstraintLayoutDemo)