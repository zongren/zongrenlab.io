---
title: Android项目依赖管理
category: android
tags: [denpendency]
date: 2016-12-08 00:32:49
excerpt: 如何导入另外一个项目作为项目依赖。
---

只需要在根目录下的setting.gradle添加以下内容即可，其中`./AnotherProject/library/`为另一个项目的路径，建议将其放在主项目中，建议使用`git submodule`或`svn external`管理，`Library`为自定义名称。
```
include ':app'
include ':Library'
project(':Library').projectDir = new File(settingsDir, './AnotherProject/library/')

```