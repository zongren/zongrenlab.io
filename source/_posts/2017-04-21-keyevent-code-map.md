---
title: Android键值表
category: miscellaneous
tags: [android]
excerpt: Android键值表
date: 2017-04-21 17:37:47
---
### 表格
根据[官方文档](https://developer.android.com/reference/android/view/KeyEvent.html)整理出的表格

| code | api level | description | value |
| ---   | ---   | ---         | --- |
| KEYCODE_0 | 1 | '0' key. | 7 | 
| KEYCODE_1 | 1 | '1' key. | 8 | 
| KEYCODE_11 | 21 | '11' key. | 227 | 
| KEYCODE_12 | 21 | '12' key. | 228 | 
| KEYCODE_2 | 1 | '2' key. | 9 | 
| KEYCODE_3 | 1 | '3' key. | 10 | 
| KEYCODE_3D_MODE | 14 | 3D Mode key. Toggles the display between 2D and 3D mode. | 206 | 
| KEYCODE_4 | 1 | '4' key. | 11 | 
| KEYCODE_5 | 1 | '5' key. | 12 | 
| KEYCODE_6 | 1 | '6' key. | 13 | 
| KEYCODE_7 | 1 | '7' key. | 14 | 
| KEYCODE_8 | 1 | '8' key. | 15 | 
| KEYCODE_9 | 1 | '9' key. | 16 | 
| KEYCODE_A | 1 | 'A' key. | 29 | 
| KEYCODE_ALT_LEFT | 1 | Left Alt modifier key. | 57 | 
| KEYCODE_ALT_RIGHT | 1 | Right Alt modifier key. | 58 | 
| KEYCODE_APOSTROPHE | 1 | ''' (apostrophe) key. | 75 | 
| KEYCODE_APP_SWITCH | 11 | App switch key. Should bring up the application switcher dialog. | 187 | 
| KEYCODE_ASSIST | 16 | Assist key. Launches the global assist activity. Not delivered to applications. | 219 | 
| KEYCODE_AT | 1 | '@' key. | 77 | 
| KEYCODE_AVR_INPUT | 11 | A/V Receiver input key. On TV remotes, switches the input mode on an external A/V Receiver. | 182 | 
| KEYCODE_AVR_POWER | 11 | A/V Receiver power key. On TV remotes, toggles the power on an external A/V Receiver. | 181 | 
| KEYCODE_B | 1 | 'B' key. | 30 | 
| KEYCODE_BACK | 1 | Back key. | 4 | 
| KEYCODE_BACKSLASH | 1 | '\' key. | 73 | 
| KEYCODE_BOOKMARK | 11 | Bookmark key. On some TV remotes, bookmarks content or web pages. | 174 | 
| KEYCODE_BREAK | 11 | Break / Pause key. | 121 | 
| KEYCODE_BRIGHTNESS_DOWN | 18 | Brightness Down key. Adjusts the screen brightness down. | 220 | 
| KEYCODE_BRIGHTNESS_UP | 18 | Brightness Up key. Adjusts the screen brightness up. | 221 | 
| KEYCODE_BUTTON_1 | 12 | Generic Game Pad Button #1. | 188 | 
| KEYCODE_BUTTON_10 | 12 | Generic Game Pad Button #10. | 197 | 
| KEYCODE_BUTTON_11 | 12 | Generic Game Pad Button #11. | 198 | 
| KEYCODE_BUTTON_12 | 12 | Generic Game Pad Button #12. | 199 | 
| KEYCODE_BUTTON_13 | 12 | Generic Game Pad Button #13. | 200 | 
| KEYCODE_BUTTON_14 | 12 | Generic Game Pad Button #14. | 201 | 
| KEYCODE_BUTTON_15 | 12 | Generic Game Pad Button #15. | 202 | 
| KEYCODE_BUTTON_16 | 12 | Generic Game Pad Button #16. | 203 | 
| KEYCODE_BUTTON_2 | 12 | Generic Game Pad Button #2. | 189 | 
| KEYCODE_BUTTON_3 | 12 | Generic Game Pad Button #3. | 190 | 
| KEYCODE_BUTTON_4 | 12 | Generic Game Pad Button #4. | 191 | 
| KEYCODE_BUTTON_5 | 12 | Generic Game Pad Button #5. | 192 | 
| KEYCODE_BUTTON_6 | 12 | Generic Game Pad Button #6. | 193 | 
| KEYCODE_BUTTON_7 | 12 | Generic Game Pad Button #7. | 194 | 
| KEYCODE_BUTTON_8 | 12 | Generic Game Pad Button #8. | 195 | 
| KEYCODE_BUTTON_9 | 12 | Generic Game Pad Button #9. | 196 | 
| KEYCODE_BUTTON_A | 9 | A Button key. On a game controller, the A button should be either the button labeled A or the first button on the bottom row of controller buttons. | 96 | 
| KEYCODE_BUTTON_B | 9 | B Button key. On a game controller, the B button should be either the button labeled B or the second button on the bottom row of controller buttons. | 97 | 
| KEYCODE_BUTTON_C | 9 | C Button key. On a game controller, the C button should be either the button labeled C or the third button on the bottom row of controller buttons. | 98 | 
| KEYCODE_BUTTON_L1 | 9 | L1 Button key. On a game controller, the L1 button should be either the button labeled L1 (or L) or the top left trigger button. | 102 | 
| KEYCODE_BUTTON_L2 | 9 | L2 Button key. On a game controller, the L2 button should be either the button labeled L2 or the bottom left trigger button. | 104 | 
| KEYCODE_BUTTON_MODE | 9 | Mode Button key. On a game controller, the button labeled Mode. | 110 | 
| KEYCODE_BUTTON_R1 | 9 | R1 Button key. On a game controller, the R1 button should be either the button labeled R1 (or R) or the top right trigger button. | 103 | 
| KEYCODE_BUTTON_R2 | 9 | R2 Button key. On a game controller, the R2 button should be either the button labeled R2 or the bottom right trigger button. | 105 | 
| KEYCODE_BUTTON_SELECT | 9 | Select Button key. On a game controller, the button labeled Select. | 109 | 
| KEYCODE_BUTTON_START | 9 | Start Button key. On a game controller, the button labeled Start. | 108 | 
| KEYCODE_BUTTON_THUMBL | 9 | Left Thumb Button key. On a game controller, the left thumb button indicates that the left (or only) joystick is pressed. | 106 | 
| KEYCODE_BUTTON_THUMBR | 9 | Right Thumb Button key. On a game controller, the right thumb button indicates that the right joystick is pressed. | 107 | 
| KEYCODE_BUTTON_X | 9 | X Button key. On a game controller, the X button should be either the button labeled X or the first button on the upper row of controller buttons. | 99 | 
| KEYCODE_BUTTON_Y | 9 | Y Button key. On a game controller, the Y button should be either the button labeled Y or the second button on the upper row of controller buttons. | 100 | 
| KEYCODE_BUTTON_Z | 9 | Z Button key. On a game controller, the Z button should be either the button labeled Z or the third button on the upper row of controller buttons. | 101 | 
| KEYCODE_C | 1 | 'C' key. | 31 | 
| KEYCODE_CALCULATOR | 15 | Calculator special function key. Used to launch a calculator application. | 210 | 
| KEYCODE_CALENDAR | 15 | Calendar special function key. Used to launch a calendar application. | 208 | 
| KEYCODE_CALL | 1 | Call key. | 5 | 
| KEYCODE_CAMERA | 1 | Camera key. Used to launch a camera application or take pictures. | 27 | 
| KEYCODE_CAPS_LOCK | 11 | Caps Lock key. | 115 | 
| KEYCODE_CAPTIONS | 11 | Toggle captions key. Switches the mode for closed-captioning text, for example during television shows. | 175 | 
| KEYCODE_CHANNEL_DOWN | 11 | Channel down key. On TV remotes, decrements the television channel. | 167 | 
| KEYCODE_CHANNEL_UP | 11 | Channel up key. On TV remotes, increments the television channel. | 166 | 
| KEYCODE_CLEAR | 1 | Clear key. | 28 | 
| KEYCODE_COMMA | 1 | ',' key. | 55 | 
| KEYCODE_CONTACTS | 15 | Contacts special function key. Used to launch an address book application. | 207 | 
| KEYCODE_COPY | 24 | Copy key. | 278 | 
| KEYCODE_CTRL_LEFT | 11 | Left Control modifier key. | 113 | 
| KEYCODE_CTRL_RIGHT | 11 | Right Control modifier key. | 114 | 
| KEYCODE_CUT | 24 | Cut key. | 277 | 
| KEYCODE_D | 1 | 'D' key. | 32 | 
| KEYCODE_DEL | 1 | Backspace key. Deletes characters before the insertion point, unlike KEYCODE_FORWARD_DEL. | 67 | 
| KEYCODE_DPAD_CENTER | 1 | Directional Pad Center key. May also be synthesized from trackball motions. | 23 | 
| KEYCODE_DPAD_DOWN | 1 | Directional Pad Down key. May also be synthesized from trackball motions. | 20 | 
| KEYCODE_DPAD_DOWN_LEFT | 24 | Directional Pad Down-Left | 269 | 
| KEYCODE_DPAD_DOWN_RIGHT | 24 | Directional Pad Down-Right | 271 | 
| KEYCODE_DPAD_LEFT | 1 | Directional Pad Left key. May also be synthesized from trackball motions. | 21 | 
| KEYCODE_DPAD_RIGHT | 1 | Directional Pad Right key. May also be synthesized from trackball motions. | 22 | 
| KEYCODE_DPAD_UP | 1 | Directional Pad Up key. May also be synthesized from trackball motions. | 19 | 
| KEYCODE_DPAD_UP_LEFT | 24 | Directional Pad Up-Left | 268 | 
| KEYCODE_DPAD_UP_RIGHT | 24 | Directional Pad Up-Right | 270 | 
| KEYCODE_DVR | 11 | DVR key. On some TV remotes, switches to a DVR mode for recorded shows. | 173 | 
| KEYCODE_E | 1 | 'E' key. | 33 | 
| KEYCODE_EISU | 16 | Japanese alphanumeric key. | 212 | 
| KEYCODE_ENDCALL | 1 | End Call key. | 6 | 
| KEYCODE_ENTER | 1 | Enter key. | 66 | 
| KEYCODE_ENVELOPE | 1 | Envelope special function key. Used to launch a mail application. | 65 | 
| KEYCODE_EQUALS | 1 | '=' key. | 70 | 
| KEYCODE_ESCAPE | 11 | Escape key. | 111 | 
| KEYCODE_EXPLORER | 1 | Explorer special function key. Used to launch a browser application. | 64 | 
| KEYCODE_F | 1 | 'F' key. | 34 | 
| KEYCODE_F1 | 11 | F1 key. | 131 | 
| KEYCODE_F10 | 11 | F10 key. | 140 | 
| KEYCODE_F11 | 11 | F11 key. | 141 | 
| KEYCODE_F12 | 11 | F12 key. | 142 | 
| KEYCODE_F2 | 11 | F2 key. | 132 | 
| KEYCODE_F3 | 11 | F3 key. | 133 | 
| KEYCODE_F4 | 11 | F4 key. | 134 | 
| KEYCODE_F5 | 11 | F5 key. | 135 | 
| KEYCODE_F6 | 11 | F6 key. | 136 | 
| KEYCODE_F7 | 11 | F7 key. | 137 | 
| KEYCODE_F8 | 11 | F8 key. | 138 | 
| KEYCODE_F9 | 11 | F9 key. | 139 | 
| KEYCODE_FOCUS | 1 | Camera Focus key. Used to focus the camera. | 80 | 
| KEYCODE_FORWARD | 11 | Forward key. Navigates forward in the history stack. Complement of KEYCODE_BACK. | 125 | 
| KEYCODE_FORWARD_DEL | 11 | Forward Delete key. Deletes characters ahead of the insertion point, unlike KEYCODE_DEL. | 112 | 
| KEYCODE_FUNCTION | 11 | Function modifier key. | 119 | 
| KEYCODE_G | 1 | 'G' key. | 35 | 
| KEYCODE_GRAVE | 1 | '`' (backtick) key. | 68 | 
| KEYCODE_GUIDE | 11 | Guide key. On TV remotes, shows a programming guide. | 172 | 
| KEYCODE_H | 1 | 'H' key. | 36 | 
| KEYCODE_HEADSETHOOK | 1 | Headset Hook key. Used to hang up calls and stop media. | 79 | 
| KEYCODE_HELP | 21 | Help key. | 259 | 
| KEYCODE_HENKAN | 16 | Japanese conversion key. | 214 | 
| KEYCODE_HOME | 1 | Home key. This key is handled by the framework and is never delivered to applications. | 3 | 
| KEYCODE_I | 1 | 'I' key. | 37 | 
| KEYCODE_INFO | 11 | Info key. Common on TV remotes to show additional information related to what is currently being viewed. | 165 | 
| KEYCODE_INSERT | 11 | Insert key. Toggles insert / overwrite edit mode. | 124 | 
| KEYCODE_J | 1 | 'J' key. | 38 | 
| KEYCODE_K | 1 | 'K' key. | 39 | 
| KEYCODE_KANA | 16 | Japanese kana key. | 218 | 
| KEYCODE_KATAKANA_HIRAGANA | 16 | Japanese katakana / hiragana key. | 215 | 
| KEYCODE_L | 1 | 'L' key. | 40 | 
| KEYCODE_LANGUAGE_SWITCH | 14 | Language Switch key. Toggles the current input language such as switching between English and Japanese on a QWERTY keyboard. On some devices, the same function may be performed by pressing Shift+Spacebar. | 204 | 
| KEYCODE_LAST_CHANNEL | 21 | Last Channel key. Goes to the last viewed channel. | 229 | 
| KEYCODE_LEFT_BRACKET | 1 | '[' key. | 71 | 
| KEYCODE_M | 1 | 'M' key. | 41 | 
| KEYCODE_MANNER_MODE | 14 | Manner Mode key. Toggles silent or vibrate mode on and off to make the device behave more politely in certain settings such as on a crowded train. On some devices, the key may only operate when long-pressed. | 205 | 
| KEYCODE_MEDIA_AUDIO_TRACK | 19 | Audio Track key. Switches the audio tracks. | 222 | 
| KEYCODE_MEDIA_CLOSE | 11 | Close media key. May be used to close a CD tray, for example. | 128 | 
| KEYCODE_MEDIA_EJECT | 11 | Eject media key. May be used to eject a CD tray, for example. | 129 | 
| KEYCODE_MEDIA_FAST_FORWARD | 3 | Fast Forward media key. | 90 | 
| KEYCODE_MEDIA_NEXT | 3 | Play Next media key. | 87 | 
| KEYCODE_MEDIA_PAUSE | 11 | Pause media key. | 127 | 
| KEYCODE_MEDIA_PLAY | 11 | Play media key. | 126 | 
| KEYCODE_MEDIA_PLAY_PAUSE | 3 | Play/Pause media key. | 85 | 
| KEYCODE_MEDIA_PREVIOUS | 3 | Play Previous media key. | 88 | 
| KEYCODE_MEDIA_RECORD | 11 | Record media key. | 130 | 
| KEYCODE_MEDIA_REWIND | 3 | Rewind media key. | 89 | 
| KEYCODE_MEDIA_SKIP_BACKWARD | 23 | Skip backward media key. | 273 | 
| KEYCODE_MEDIA_SKIP_FORWARD | 23 | Skip forward media key. | 272 | 
| KEYCODE_MEDIA_STEP_BACKWARD | 23 | Step backward media key. Steps media backward, one frame at a time. | 275 | 
| KEYCODE_MEDIA_STEP_FORWARD | 23 | Step forward media key. Steps media forward, one frame at a time. | 274 | 
| KEYCODE_MEDIA_STOP | 3 | Stop media key. | 86 | 
| KEYCODE_MEDIA_TOP_MENU | 21 | Media Top Menu key. Goes to the top of media menu. | 226 | 
| KEYCODE_MENU | 1 | Menu key. | 82 | 
| KEYCODE_META_LEFT | 11 | Left Meta modifier key. | 117 | 
| KEYCODE_META_RIGHT | 11 | Right Meta modifier key. | 118 | 
| KEYCODE_MINUS | 1 | '-'. | 69 | 
| KEYCODE_MOVE_END | 11 | End Movement key. Used for scrolling or moving the cursor around to the end of a line or to the bottom of a list. | 123 | 
| KEYCODE_MOVE_HOME | 11 | Home Movement key. Used for scrolling or moving the cursor around to the start of a line or to the top of a list. | 122 | 
| KEYCODE_MUHENKAN | 16 | Japanese non-conversion key. | 213 | 
| KEYCODE_MUSIC | 15 | Music special function key. Used to launch a music player application. | 209 | 
| KEYCODE_MUTE | 3 | Mute key. Mutes the microphone, unlike KEYCODE_VOLUME_MUTE. | 91 | 
| KEYCODE_N | 1 | 'N' key. | 42 | 
| KEYCODE_NAVIGATE_IN | 23 | Navigate in key. Activates the item that currently has focus or expands to the next level of a navigation hierarchy. | 262 | 
| KEYCODE_NAVIGATE_NEXT | 23 | Navigate to next key. Advances to the next item in an ordered collection of items. | 261 | 
| KEYCODE_NAVIGATE_OUT | 23 | Navigate out key. Backs out one level of a navigation hierarchy or collapses the item that currently has focus. | 263 | 
| KEYCODE_NAVIGATE_PREVIOUS | 23 | Navigate to previous key. Goes backward by one item in an ordered collection of items. | 260 | 
| KEYCODE_NOTIFICATION | 1 | Notification key. | 83 | 
| KEYCODE_NUM | 1 | Number modifier key. Used to enter numeric symbols. This key is not Num Lock; it is more like KEYCODE_ALT_LEFT and is interpreted as an ALT key by MetaKeyKeyListener. | 78 | 
| KEYCODE_NUMPAD_0 | 11 | Numeric keypad '0' key. | 144 | 
| KEYCODE_NUMPAD_1 | 11 | Numeric keypad '1' key. | 145 | 
| KEYCODE_NUMPAD_2 | 11 | Numeric keypad '2' key. | 146 | 
| KEYCODE_NUMPAD_3 | 11 | Numeric keypad '3' key. | 147 | 
| KEYCODE_NUMPAD_4 | 11 | Numeric keypad '4' key. | 148 | 
| KEYCODE_NUMPAD_5 | 11 | Numeric keypad '5' key. | 149 | 
| KEYCODE_NUMPAD_6 | 11 | Numeric keypad '6' key. | 150 | 
| KEYCODE_NUMPAD_7 | 11 | Numeric keypad '7' key. | 151 | 
| KEYCODE_NUMPAD_8 | 11 | Numeric keypad '8' key. | 152 | 
| KEYCODE_NUMPAD_9 | 11 | Numeric keypad '9' key. | 153 | 
| KEYCODE_NUMPAD_ADD | 11 | Numeric keypad '+' key (for addition). | 157 | 
| KEYCODE_NUMPAD_COMMA | 11 | Numeric keypad ',' key (for decimals or digit grouping). | 159 | 
| KEYCODE_NUMPAD_DIVIDE | 11 | Numeric keypad '/' key (for division). | 154 | 
| KEYCODE_NUMPAD_DOT | 11 | Numeric keypad '.' key (for decimals or digit grouping). | 158 | 
| KEYCODE_NUMPAD_ENTER | 11 | Numeric keypad Enter key. | 160 | 
| KEYCODE_NUMPAD_EQUALS | 11 | Numeric keypad '=' key. | 161 | 
| KEYCODE_NUMPAD_LEFT_PAREN | 11 | Numeric keypad '(' key. | 162 | 
| KEYCODE_NUMPAD_MULTIPLY | 11 | Numeric keypad '*' key (for multiplication). | 155 | 
| KEYCODE_NUMPAD_RIGHT_PAREN | 11 | Numeric keypad ')' key. | 163 | 
| KEYCODE_NUMPAD_SUBTRACT | 11 | Numeric keypad '-' key (for subtraction). | 156 | 
| KEYCODE_NUM_LOCK | 11 | Num Lock key. This is the Num Lock key; it is different from KEYCODE_NUM. This key alters the behavior of other keys on the numeric keypad. | 143 | 
| KEYCODE_O | 1 | 'O' key. | 43 | 
| KEYCODE_P | 1 | 'P' key. | 44 | 
| KEYCODE_PAGE_DOWN | 9 | Page Down key. | 93 | 
| KEYCODE_PAGE_UP | 9 | Page Up key. | 92 | 
| KEYCODE_PAIRING | 21 | Pairing key. Initiates peripheral pairing mode. Useful for pairing remote control devices or game controllers, especially if no other input mode is available. | 225 | 
| KEYCODE_PASTE | 24 | Paste key. | 279 | 
| KEYCODE_PERIOD | 1 | '.' key. | 56 | 
| KEYCODE_PICTSYMBOLS | 9 | Picture Symbols modifier key. Used to switch symbol sets (Emoji, Kao-moji). | 94 | 
| KEYCODE_PLUS | 1 | '+' key. | 81 | 
| KEYCODE_POUND | 1 | '#' key. | 18 | 
| KEYCODE_POWER | 1 | Power key. | 26 | 
| KEYCODE_PROG_BLUE | 11 | Blue "programmable" key. On TV remotes, acts as a contextual/programmable key. | 186 | 
| KEYCODE_PROG_GREEN | 11 | Green "programmable" key. On TV remotes, actsas a contextual/programmable key. | 184 | 
| KEYCODE_PROG_RED | 11 | Red "programmable" key. On TV remotes, acts as a contextual/programmable key. | 183 | 
| KEYCODE_PROG_YELLOW | 11 | Yellow "programmable" key. On TV remotes, acts as a contextual/programmable key. | 185 | 
| KEYCODE_Q | 1 | 'Q' key. | 45 | 
| KEYCODE_R | 1 | 'R' key. | 46 | 
| KEYCODE_RIGHT_BRACKET | 1 | ']' key. | 72 | 
| KEYCODE_RO | 16 | Japanese Ro key. | 217 | 
| KEYCODE_S | 1 | 'S' key. | 47 | 
| KEYCODE_SCROLL_LOCK | 11 | Scroll Lock key. | 116 | 
| KEYCODE_SEARCH | 1 | Search key. | 84 | 
| KEYCODE_SEMICOLON | 1 | ';' key. | 74 | 
| KEYCODE_SETTINGS | 11 | Settings key. Starts the system settings activity. | 176 | 
| KEYCODE_SHIFT_LEFT | 1 | Left Shift modifier key. | 59 | 
| KEYCODE_SHIFT_RIGHT | 1 | Right Shift modifier key. | 60 | 
| KEYCODE_SLASH | 1 | '/' key. | 76 | 
| KEYCODE_SLEEP | 20 | Sleep key. Puts the device to sleep. Behaves somewhat like KEYCODE_POWER but it has no effect if the device is already asleep. | 223 | 
| KEYCODE_SOFT_LEFT | 1 | Soft Left key. Usually situated below the display on phones and used as a multi-function feature key for selecting a software defined function shown on the bottom left of the display. | 1 | 
| KEYCODE_SOFT_RIGHT | 1 | Soft Right key. Usually situated below the display on phones and used as a multi-function feature key for selecting a software defined function shown on the bottom right of the display. | 2 | 
| KEYCODE_SOFT_SLEEP | 24 | put device to sleep unless a wakelock is held. | 276 | 
| KEYCODE_SPACE | 1 | Space key. | 62 | 
| KEYCODE_STAR | 1 | '*' key. | 17 | 
| KEYCODE_STB_INPUT | 11 | Set-top-box input key. On TV remotes, switches the input mode on an external Set-top-box. | 180 | 
| KEYCODE_STB_POWER | 11 | Set-top-box power key. On TV remotes, toggles the power on an external Set-top-box. | 179 | 
| KEYCODE_STEM_1 | 24 | Generic stem key 1 for Wear | 265 | 
| KEYCODE_STEM_2 | 24 | Generic stem key 2 for Wear | 266 | 
| KEYCODE_STEM_3 | 24 | Generic stem key 3 for Wear | 267 | 
| KEYCODE_STEM_PRIMARY | 24 | Primary stem key for Wear Main power/reset button on watch. | 264 | 
| KEYCODE_SWITCH_CHARSET | 9 | Switch Charset modifier key. Used to switch character sets (Kanji, Katakana). | 95 | 
| KEYCODE_SYM | 1 | Symbol modifier key. Used to enter alternate symbols. | 63 | 
| KEYCODE_SYSRQ | 11 | System Request / Print Screen key. | 120 | 
| KEYCODE_SYSTEM_NAVIGATION_DOWN | 25 | Consumed by the system for navigation down | 281 | 
| KEYCODE_SYSTEM_NAVIGATION_LEFT | 25 | Consumed by the system for navigation left | 282 | 
| KEYCODE_SYSTEM_NAVIGATION_RIGHT | 25 | Consumed by the system for navigation right | 283 | 
| KEYCODE_SYSTEM_NAVIGATION_UP | 25 | Consumed by the system for navigation up | 280 | 
| KEYCODE_T | 1 | 'T' key. | 48 | 
| KEYCODE_TAB | 1 | Tab key. | 61 | 
| KEYCODE_TV | 11 | TV key. On TV remotes, switches to viewing live TV. | 170 | 
| KEYCODE_TV_ANTENNA_CABLE | 21 | Antenna/Cable key. Toggles broadcast input source between antenna and cable. | 242 | 
| KEYCODE_TV_AUDIO_DESCRIPTION | 21 | Audio description key. Toggles audio description off / on. | 252 | 
| KEYCODE_TV_AUDIO_DESCRIPTION_MIX_DOWN | 21 | Audio description mixing volume down key. Lessen audio description volume as compared with normal audio volume. | 254 | 
| KEYCODE_TV_AUDIO_DESCRIPTION_MIX_UP | 21 | Audio description mixing volume up key. Louden audio description volume as compared with normal audio volume. | 253 | 
| KEYCODE_TV_CONTENTS_MENU | 21 | Contents menu key. Goes to the title list. Corresponds to Contents Menu  of CEC User Control Code | 256 | 
| KEYCODE_TV_DATA_SERVICE | 21 | TV data service key. Displays data services like weather, sports. | 230 | 
| KEYCODE_TV_INPUT | 11 | TV input key. On TV remotes, switches the input on a television screen. | 178 | 
| KEYCODE_TV_INPUT_COMPONENT_1 | 21 | Component #1 key. Switches to component video input #1. | 249 | 
| KEYCODE_TV_INPUT_COMPONENT_2 | 21 | Component #2 key. Switches to component video input #2. | 250 | 
| KEYCODE_TV_INPUT_COMPOSITE_1 | 21 | Composite #1 key. Switches to composite video input #1. | 247 | 
| KEYCODE_TV_INPUT_COMPOSITE_2 | 21 | Composite #2 key. Switches to composite video input #2. | 248 | 
| KEYCODE_TV_INPUT_HDMI_1 | 21 | HDMI #1 key. Switches to HDMI input #1. | 243 | 
| KEYCODE_TV_INPUT_HDMI_2 | 21 | HDMI #2 key. Switches to HDMI input #2. | 244 | 
| KEYCODE_TV_INPUT_HDMI_3 | 21 | HDMI #3 key. Switches to HDMI input #3. | 245 | 
| KEYCODE_TV_INPUT_HDMI_4 | 21 | HDMI #4 key. Switches to HDMI input #4. | 246 | 
| KEYCODE_TV_INPUT_VGA_1 | 21 | VGA #1 key. Switches to VGA (analog RGB) input #1. | 251 | 
| KEYCODE_TV_MEDIA_CONTEXT_MENU | 21 | Media context menu key. Goes to the context menu of media contents. Corresponds to Media Context-sensitive Menu  of CEC User Control Code. | 257 | 
| KEYCODE_TV_NETWORK | 21 | Toggle Network key. Toggles selecting broacast services. | 241 | 
| KEYCODE_TV_NUMBER_ENTRY | 21 | Number entry key. Initiates to enter multi-digit channel nubmber when each digit key is assigned for selecting separate channel. Corresponds to Number Entry Mode  of CEC User Control Code. | 234 | 
| KEYCODE_TV_POWER | 11 | TV power key. On TV remotes, toggles the power on a television screen. | 177 | 
| KEYCODE_TV_RADIO_SERVICE | 21 | Radio key. Toggles TV service / Radio service. | 232 | 
| KEYCODE_TV_SATELLITE | 21 | Satellite key. Switches to digital satellite broadcast service. | 237 | 
| KEYCODE_TV_SATELLITE_BS | 21 | BS key. Switches to BS digital satellite broadcasting service available in Japan. | 238 | 
| KEYCODE_TV_SATELLITE_CS | 21 | CS key. Switches to CS digital satellite broadcasting service available in Japan. | 239 | 
| KEYCODE_TV_SATELLITE_SERVICE | 21 | BS/CS key. Toggles between BS and CS digital satellite services. | 240 | 
| KEYCODE_TV_TELETEXT | 21 | Teletext key. Displays Teletext service. | 233 | 
| KEYCODE_TV_TERRESTRIAL_ANALOG | 21 | Analog Terrestrial key. Switches to analog terrestrial broadcast service. | 235 | 
| KEYCODE_TV_TERRESTRIAL_DIGITAL | 21 | Digital Terrestrial key. Switches to digital terrestrial broadcast service. | 236 | 
| KEYCODE_TV_TIMER_PROGRAMMING | 21 | Timer programming key. Goes to the timer recording menu. Corresponds to Timer Programming  of CEC User Control Code. | 258 | 
| KEYCODE_TV_ZOOM_MODE | 21 | Zoom mode key. Changes Zoom mode (Normal, Full, Zoom, Wide-zoom, etc.) | 255 | 
| KEYCODE_U | 1 | 'U' key. | 49 | 
| KEYCODE_UNKNOWN | 1 | Unknown key code. | 0 | 
| KEYCODE_V | 1 | 'V' key. | 50 | 
| KEYCODE_VOICE_ASSIST | 21 | Voice Assist key. Launches the global voice assist activity. Not delivered to applications. | 231 | 
| KEYCODE_VOLUME_DOWN | 1 | Volume Down key. Adjusts the speaker volume down. | 25 | 
| KEYCODE_VOLUME_MUTE | 11 | Volume Mute key. Mutes the speaker, unlike KEYCODE_MUTE. This key should normally be implemented as a toggle such that the first press mutes the speaker and the second press restores the original volume. | 164 | 
| KEYCODE_VOLUME_UP | 1 | Volume Up key. Adjusts the speaker volume up. | 24 | 
| KEYCODE_W | 1 | 'W' key. | 51 | 
| KEYCODE_WAKEUP | 20 | Wakeup key. Wakes up the device. Behaves somewhat like KEYCODE_POWER but it has no effect if the device is already awake. | 224 | 
| KEYCODE_WINDOW | 11 | Window key. On TV remotes, toggles picture-in-picture mode or other windowing functions. | 171 | 
| KEYCODE_X | 1 | 'X' key. | 52 | 
| KEYCODE_Y | 1 | 'Y' key. | 53 | 
| KEYCODE_YEN | 16 | Japanese Yen key. | 216 | 
| KEYCODE_Z | 1 | 'Z' key. | 54 | 
| KEYCODE_ZENKAKU_HANKAKU | 16 | Japanese full-width / half-width key. | 211 | 
| KEYCODE_ZOOM_IN | 11 | Zoom in key. | 168 | 
| KEYCODE_ZOOM_OUT | 11 | Zoom out key. | 169 

### 命令
使用`adb shell input keyevent KEYCODE`输入指定的按键，例如输入回车键
```
adb shell input keyevent 66
```