---
title: Android模拟nfc卡
category: android
tags: [nfc]
excerpt: 简单记录android手机如何模拟nfc卡
date: 2018-03-12 11:27:33
---

android模拟nfc卡大体分为两种类型，安全元件模拟，Host-Cpu模拟，也就是纯软件模拟

## 安全元件
例如sim卡，优点是不用启动任何应用，卡号固定

而使用另一台自带nfc模块的Android设备读卡时，需要使用以下方法
```
nfcAdapter.enableReaderMode(this, new NfcAdapter.ReaderCallback() {
    @Override
    public void onTagDiscovered(Tag tag) {
        Bundle data = new Bundle();
        data.putParcelable("tag", tag);
        Message message = new Message();
        message.setData(data);
        discoverTagHandler.sendMessage(message);
    }
}, FLAG_READER_NFC_A | FLAG_READER_SKIP_NDEF_CHECK, null);
```

而不能使用以下方法
```
PendingIntent mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()), 0);
nfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
```

如果是普通ic卡读卡器，手机可以当作普通nfc卡直接刷卡

## Host CPU模拟
纯软件模拟，缺点是每次刷卡都要打开对应的app，否则卡号会一直变化