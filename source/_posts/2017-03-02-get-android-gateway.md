---
title: 获取Android网关信息
category: miscellaneous
tags: [terminal]
date: 2017-03-02 16:06:26
excerpt: 获取Android网关信息
---

一句话获取Android网关信息
```
getprop | grep gw
```
或者使用以下命令获取dns、网关等信息
```
getprop | grep eth0
```