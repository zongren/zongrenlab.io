---
title: 发送短信
date: 2016-08-12 16:24:47
category: ios
tags: [sms,note]
---
本文介绍如何调用系统短信应用，发送短信
首先引入系统库`MessageUI.framework`,然后在控制器中引入头文件
```
#import <MessageUI/MessageUI.h>
```
接下来实现代理方法
```
- (void)messageComposeViewController:(MFMessageComposeViewController*)controller
                 didFinishWithResult:(MessageComposeResult)result {
    switch (result) {
        case MessageComposeResultCancelled: {
            [self.view makeToast:@"取消"];
            break;
        }
        case MessageComposeResultSent: {
            [self.view makeToast:@"发送成功"];
            break;
        }
        case MessageComposeResultFailed: {
            [self.view makeToast:@"发送失败"];
            break;
        }
    }
}
```
最后打开ViewController即可
```
MFMessageComposeViewController* viewController = [[MFMessageComposeViewController alloc] init];
        viewController.messageComposeDelegate = self;
        viewController.body = @"测试";
        [self.navigationController pushViewController:viewController animated:YES];
```


