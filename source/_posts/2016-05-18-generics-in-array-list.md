---
title: 在List中使用Generics变量
date: 2016-05-18 10:25:00
category: android
tags: [generic,java,list]
---

## 什么是Generics Type
最近研究了一下`java`里的`generics`类型，发现非常好用，特别适合封装一些组件，之前发表的几篇文章都用到这这个（{% post_link a-simple-yet-useful-way-to-manage-list %},{% post_link a-simple-yet-reusable-adpater-for-android %}）。generics是类似于
```
CommonAdapter<V,T>
```
中的`V`和`T`，表示声明CommonAdapter的对象时，可以传入某种类型作为参数，可以是自定义的类或是原生的类。如果是
```
CommonAdapter<V extends CommonModel,T extends CommonViewHolder>
```
这种写法，则表示必须传入继承了CommonModel类的类型（或者实现了CommonModel接口的类型，取决于CommonModel是普通类还是接口）和继承了CommonViewHolder类的类型作为参数。其中`V`和`T`的字母的选择（即什么时候用V，什么时候用T）有自己的规则

| 字母 | 意义 |
| ---- | ---- |
| E | Element (used extensively by the Java Collections Framework) |
| K | Key |
| N | Number |
| T | Type |
| V | Value |
| S,U,V etc. | 2nd, 3rd, 4th types |

以上命名规范从[java generics](https://docs.oracle.com/javase/tutorial/java/generics/types.html)复制而来，其中最后一行`S,U,V etc.`是指可以使用连续的字母表示多个类型，如
```
CommonAdapter<T,S,U,V>
```
另外还可以使用长单词而不是字母来表示generics

## 在ArrayList中使用Generics Type
介绍完了generics，下面说一下在数组中使用generics，即
```
class MyActivity extends Activity{
    ...
    ArrayList<T extends MyClass> list;
    ...
}
```
其中MyClass是一个自定义类。看了以上代码，你可能觉得list是一个包含继承了MyClass的类的对象的数组（list contains elements which are instance of subclass of MyClass)，但是当你使用以下代码的时候，会提示错误
```
class MyOtherClass extens MyClass{
    ...
}
list.add(new MyOthreClass());
```
建议采用这种写法
```
class MyActivity<T extends MyClass> extends Activity{
    ...
    ArrayList<T> list;
    ...
}
```