---
title: Android下滚动隐藏ActionBar
date: 2016-08-22 22:45:51
category: android
tags: [actionbar,coordinator-layout,design]
---
现在添加滚动隐藏ActionBar的效果已经非常简单了，只需要使用`CoordinatorLayout`即可，代码如下
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.design.widget.CoordinatorLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:id="@+id/coordinatorLayout"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    >

    <android.support.design.widget.AppBarLayout
        android:id="@+id/appBarLayout"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        >

        <android.support.v7.widget.Toolbar 
            android:id="@+id/activity_common_toolbar"
            android:layout_width="match_parent"
            android:layout_height="?attr/actionBarSize"
            android:background="@color/colorActionBarBackground"
            android:theme="@style/ThemeOverlay.AppCompat.ActionBar"
            app:layout_scrollFlags="scroll|enterAlways"
            app:navigationIcon="@mipmap/btn_back_dark"
            app:popupTheme="@style/ThemeOverlay.AppCompat.Light"
            android:focusableInTouchMode="true"
            >

            <EditText
                android:id="@+id/activity_goods_list_editText"
                android:layout_width="match_parent"
                android:layout_height="30dp"
                android:layout_marginRight="10dp"
                android:background="@drawable/activity_goods_list_edit_text"
                android:drawableRight="@mipmap/ic_search"
                android:hint="@string/hintEnterGoodsName"
                android:paddingLeft="5dp"
                android:textColor="@color/colorThree"
                android:textSize="14dp" 
                />

        </android.support.v7.widget.Toolbar>
    </android.support.design.widget.AppBarLayout>

    <FrameLayout
        android:id="@+id/activity_common_fragment_container"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_below="@+id/activity_common_toolbar"
        app:layout_behavior="@string/appbar_scrolling_view_behavior" />

</android.support.design.widget.CoordinatorLayout>
```
