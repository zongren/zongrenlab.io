---
title: 给nojs主题增加计数功能
category: development
tags: [nojs,counter,leancloud]
date: 2017-02-28 08:26:09
excerpt: 如何利用LeanCloud免费存储功能，给静态网站增加计数功能。
---

静态网站想要添加计数功能，一般是通过前端Javascript和一个后端实现的，简单来说，它的流程为：

1. Javascript将当前页面的url，标题等信息发到后端服务器上
2. 服务器判断该ip是否访问了该url，如果没有则创建一条记录，并返回总的访问数量
3. Javascript获取访问数量并修改页面对应的元素

虽然整体逻辑都很简单，但是我的博客是搭建在gitlab免费托管平台上的，没有单独的服务器，又不想为了一个计数功能单独买一个虚拟机，最后发现最好的方法是使用[LeanCloud](https://leancloud.cn/)免费存储服务，这样完全不用担心后端的问题了，实现的过程参考了[NexT主题](https://github.com/iissnan/hexo-theme-next/)，移除几个warning，增加了ip判断
