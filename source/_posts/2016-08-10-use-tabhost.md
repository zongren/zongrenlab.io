---
title: 使用FragmentTabHost
date: 2016-08-10 15:47:59
category: android
tags: [note]
---

本文主要介绍如何使用FragmentTabHost实现底部导航栏，以及切换Fragment
## Activity布局
Activity的布局文件代码如下
```
<?xml version="1.0" encoding="utf-8"?>
<android.support.v4.app.FragmentTabHost
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@android:id/tabhost"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    >

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        >

        <TabWidget
            android:id="@android:id/tabs"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_alignParentBottom="true"
            android:background="@color/colorTabBackground"
            android:orientation="horizontal"
            />

        <FrameLayout
            android:id="@android:id/tabcontent"
            android:layout_width="0dp"
            android:layout_height="0dp"
            />

        <FrameLayout
            android:id="@+id/activity_root_fragmentContainer"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_above="@android:id/tabs"
            />

    </RelativeLayout>
</android.support.v4.app.FragmentTabHost>
```

需要注意的是，除了```realtabcontent```，其它id都不可以更改。
## 标签布局
每个标签的布局是不同的，如下是其中一个标签的布局
```
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:padding="5dp"
    >

    <ImageView
        android:id="@+id/activity_root_tab_home_imageView"
        android:layout_width="wrap_content"
        android:layout_height="22dp"
        android:layout_centerHorizontal="true"
        android:scaleType="fitCenter"
        android:src="@drawable/activity_root_tab_home_image_selector"
        />

    <TextView
        android:id="@+id/activity_root_tab_home_textView"
        android:layout_width="wrap_content"
        android:layout_height="15dp"
        android:layout_below="@+id/activity_root_tab_home_imageView"
        android:layout_centerHorizontal="true"
        android:layout_marginTop="5dp"
        android:text="@string/tabHome"
        android:textColor="@drawable/activity_root_tab_text_selector"
        android:textSize="13dp"
        />
</RelativeLayout>
```
## 添加标签
最后一步需要在Activity中添加标签，代码如下
```
public class RootActivity extends AppCompatActivity {
    private FragmentTabHost mTabHost;

    @Override
    @SuppressLint("InflateParams")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabWidget = (TabWidget) findViewById(android.R.id.tabs);
        mTabWidget.setStripEnabled(false);
        mTabWidget.setDividerDrawable(null);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.activity_root_fragmentContainer);

        TabHost.TabSpec homeTabSpec = mTabHost.newTabSpec(INDEX_FRAGMENT_TAG);
        View homeIndicatorView = getLayoutInflater().inflate(R.layout.activity_root_tab_home, null);
        homeTabSpec.setIndicator(homeIndicatorView);
        mTabHost.addTab(homeTabSpec, IndexFragment.class, null);

        TabHost.TabSpec categoryTabSpec = mTabHost.newTabSpec(CATEGORY_FRAGMENT_TAG);
        categoryTabSpec.setIndicator(getLayoutInflater().inflate(R.layout.activity_root_tab_category, null));
        mTabHost.addTab(categoryTabSpec, CategoryFragment.class, null);

        TabHost.TabSpec inventoryTabSpec = mTabHost.newTabSpec(INVENTORY_FRAGMENT_TAG);
        inventoryTabSpec.setIndicator(getLayoutInflater().inflate(R.layout.activity_root_tab_inventory, null));
        mTabHost.addTab(inventoryTabSpec, InventoryFragment.class, null);

        TabHost.TabSpec cartTabSpec = mTabHost.newTabSpec(CART_FRAGMENT_TAG);
        View cartIndicatorView = getLayoutInflater().inflate(R.layout.activity_root_tab_cart, null);
        cartTabSpec.setIndicator(cartIndicatorView);
        mTabHost.addTab(cartTabSpec, CartFragment.class, null);

        TabHost.TabSpec userTabSpec = mTabHost.newTabSpec(USER_FRAGMENT_TAG);
        userTabSpec.setIndicator(getLayoutInflater().inflate(R.layout.activity_root_tab_user, null));
        mTabHost.addTab(userTabSpec, UserFragment.class, null);
    }
}
```

