---
title: 移除闪烁效果
category: android
tags: [blink]
date: 2017-01-12 13:22:50
excerpt: 移除notifyItemChanged导致的闪烁效果
---

```
recyclerView.getItemAnimator().setChangeDuration(0);
//or
recyclerView.setSupportsChangeAnimations(false);
```
第二种方法不兼容老版SDK