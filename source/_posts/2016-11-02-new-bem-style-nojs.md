---
title: nojs应用bem命名风格  
category: miscellaneous
tags: [nojs,bem]
date: 2016-11-02 16:24:31
excerpt: 简单介绍了bem命名法和nojs主题的更改。
---
`bem`不是什么开发工具，它只是一个命名方法。bem的三个字母分别代表`block`，`element`，`modifier`，即`块`，`元素`和`修饰词`。bem规定使用一个单词表示完整的信息。用一个很形象的例子来说明（非原创）bem的优点，首先看下面的4个单词：

1. `person__hand`
2. `person--female__hand`
3. `person__hand-left`
4. `people--female__hand--left()`

即使是完全不了解bem的人，也可以一眼看出来，第1个单词描述了一个人的手，第2个单词描述了一个女性的手，第3个单词描述了一个人的左手，第4个单词描述了一个女性的左手。而没有采用bem的css代码好像下面这样：
```
.person {}
.hand {}
.female {}
.female-hand {}
.left-hand {}
```
它的可读性就大大降低，这样的代码也非常容易造成混乱，让其他开发人员不知所措。

简单来说，bem采用了一下命名规范：
1. `--`符号的后面表示修饰词，如`person--female`，`hand--left`
2. `__`符号的后面表示子元素，如`person__hand`
3. 块，元素和修饰词采用小写加连字符（`-`）链接的写法，如`apple-juice`

我花了两天时间将`nojs`主题的代码改为bem风格，并且把整体样式再次恢复到以前的精简的风格，还删除了可有可无的`katex`插件，图标，代码高亮等特性。

想知道更多关于bem的介绍，可以参考[官方网站️A](http://getbem.com/)和[官方网站B](https://en.bem.info/)和[这篇文章](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)。官方网站B上还是有很多有用的工具的。