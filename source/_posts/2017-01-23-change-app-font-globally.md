---
title: 全局替换应用字体
category: development
tags: [ios,android]
date: 2017-01-23 11:38:17
excerpt: 如果对系统自带的字体不满意，那么怎么才能全局替换默认字体。
---

## Android解决方法
Android下推荐使用[Calligraphy](https://github.com/chrisjenx/Calligraphy)

### 创建文件夹
在assets文件夹下创建fonts文件夹，然后放入字体文件，例如default.ttf

### 初始化
在Application中
```
@Override
public void onCreate() {
    super.onCreate();
    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
            );
    //....
}
```

### 绑定Actiity
在Activity中
```
@Override
protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
}
```

通过以上方法就可以全局修改默认字体了，如果要通过主题修改字体，请参考官方文档

## iOS下的解决方法

iOS下使用原生字体更改方法，然后使用Category或者Method Swizzling解决全局替换。首先看一下如何添加自定义字体

### 导入字体文件
将下载好的ttf文件或者otf文件拖入项目中，确保`Build Phases`->`Copy Bundle Resources`包含该文件

### 修改Info.plist
添加`Fonts provided by application`字段，并输入所有字体，Value填写文件名称，如下所示
```
Fonts provided by application
-- item01            default.ttf
```

### 查找字体名称
代码中使用的字体名称不一定是文件名称，我们需要其它方法查看family name和对应的font name，例如使用Objective-C
```
for (NSString* family in [UIFont familyNames])
{
    NSLog(@"%@\n", family);
    
    for (NSString* name in [UIFont fontNamesForFamilyName: family])
    {
        NSLog(@"==%@\n", name);
    }
}
```
打印结果为
```
...
Noteworthy
==Noteworthy-Light
==Noteworthy-Bold
...
```
代码调用如下
```
UIFont*font = [UIFont fontWithName:@"Noteworthy-Bold" size:14.0f];
```

### 全局替换
建议创建UIFont的Category类，然后创建默认字体方法，例如
```
//UIFont+Default.h
+ (UIFont*)defaultFont;

//UIFont+Default.m
+ (UIFont*)defaultFont{
    return [UIFont fontWithName:@"Noteworthy-Bold" size:14.0f];
}
```

如果要强制所有字体都修改为自定义字体，可以通过`Method Swizzling`实现，具体代码请查看[github gist](https://gist.github.com/zongren/a0443cd7bf87b4b3dbd3aef46560fbd1)，有关method swizzling的详细资料还可以查看[这篇文章](http://nshipster.cn/method-swizzling/)