---
title: 安卓触摸事件
date: 2016-05-31 23:40:25
category: android
tags: [touch]
---

## 函数调用顺序

当`Activity`接收事件后，首先调用`dispatchTouchEvent`，对事件进行分发（判断谁来处理这个事件，有可能是自己或下级），如果返回true，说明有人负责处理这个事件，事件被消耗，不再继续分发。需要注意的是`View`和`ViewGroup`的`dispatchTouchEvent`有点不一样。道理很简单，`View`是没有下级的，只要考虑自己是否需要处理这个事件就行了，而ViewGroup有可能有下级，所以ViewGroup在对事件进行分发的时候，不仅仅要判断自己对这个事件的看法（通过调用onInteceptTouchEvent来判断自己是不是要截获这次事件，如果截获了就不再继续分发），还要考虑下级对这个事件的看法（即谁来处理这个事件）。最终负责这个事件的View或ViewGroup会先调用OnTouchListener（如果有的话），如果返回true，则在当前View或ViewGroup里处理事件，不再继续执行，否则调用onTouchEvent，对事件进行处理。