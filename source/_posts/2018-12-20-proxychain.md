---
title: 使用proxychain给所有终端命令设置代理
category: miscellaneous
tags: [proxychain,proxy]
excerpt: 指定一个命令走代理
date: 2018-12-20 09:26:17
---
安装proxychain，详情可查看[官方主页](https://github.com/rofl0r/proxychains-ng)，我使用的brew 安装方式
```
# git安装
git clone https://github.com/rofl0r/proxychains-ng.git
cd proxychains-ng
./configure
make && make install
cp ./src/proxychains.conf /etc/proxychains.conf
cd .. && rm -rf proxychains-ng

# brew安装
brew install proxychains-ng
```
按照实际使用的代理方式，修改配置文件，我使用的socks5，如下所示
```
# git安装
vim /etc/proxychains.conf

# brew安装
vim ~/proxychains.conf
```
将```socks5```改为```127.0.0.1 1080```（或新增）
```
[ProxyList]
socks5 127.0.0.1 1080
```

调用proxychains4
```
proxychains4 [-f ~/proxychains.conf] pod install
```