---
title: 制作拉动加载组件
date: 2016-06-18 13:41:43
category: android
tags: [library,github]
---

由于项目中需要下拉刷新和上拉加载的功能，所以动手做了一个`拉动加载`的组件，这个组件有以下几个特点
1. 自动添加刷新组件，所以在布局文件中只需添加一个组件
2. 支持在上下左右四个方向添加刷新组件，并且可以分别添加事件
3. 支持开始加载、取消加载、组建大小改变等三个事件

以上几点是根据项目的需求所设计完成的，如果有其它建议和Bug修复，请前往[zongren/PullableLayout](https://github.com/zongren/PullableLayout)反馈