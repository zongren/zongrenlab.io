---
title: android快速点击实现
category: android
tags: [fastclick,test]
excerpt: 使用shell命令实现快速点击
date: 2019-03-06 22:36:11
---
通过for循环执行input tap的方式实现连续点击，速率比较慢，如果想要快速点击指定位置的话，可以按照以下方式进行

## 记录点击事件

```
adb shell
dd if=/dev/input/event1 of=/sdcard/recordtap
```

## 点击屏幕
点击屏幕对应的位置，多次

## 停止记录
CTRL + C

## 执行自动点击

```
while [ $(($i)) -le 5 ]; do i=$(($i + 1)); dd if=/sdcard/recordtap of=/dev/input/event1; sleep 0.009;done;
```

注意sleep 0.009的参数不能设置的太小