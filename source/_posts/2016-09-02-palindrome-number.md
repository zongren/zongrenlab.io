---
title: Palindrome Number
date: 2016-09-02 17:47:05
category: leetcode
tags: [algorithm]
---
## 问题链接
[Palindrome Number | LeetCode OJ](https://leetcode.com/problems/palindrome-number/)

## 问题描述
判断一个数字是否为回文数。


## 解决办法
```
public class Solution {
    public boolean isPalindrome(int x) {
        if (x >= 0 && x < 10) {
            return true;
        }
        if(x<0){
            return false;
        }
        boolean stop = false;
        int position = 0;
        int digitCount = digitCount(x);
        int middlePosition = digitCount / 2;
        while (!stop) {
            if (position < middlePosition) {
                int left = (x / (int) Math.pow(10, digitCount - position - 1)) % 10;
                int right = (x / (int) Math.pow(10, position)) % 10;
                if (left != right) {
                    return false;
                }
            } else {
                stop = true;
            }
            position++;
        }
        return true;
    }

    public static Integer digitCount(Integer numberInput) {
        if(numberInput == Integer.MIN_VALUE){
            return 10;
        }
        if(numberInput < 0){
            numberInput = -numberInput;
        }
        int i = 0;
        while (numberInput > 0) {
            i++;
            numberInput = numberInput / 10;
        }
        return i;
    }
}
```