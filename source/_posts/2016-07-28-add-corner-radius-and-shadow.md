---
title: 同时添加圆角和阴影效果
date: 2016-07-28 11:37:55
category: ios
tags: [tip]
---
如果想要实现视图有圆角，有阴影，并且阴影有圆角的效果，应该怎么做，请参考以下代码
```
CGFloat radius = 20;
CGFloat dimension = 200;
CGFloat offset = 8;
CGFloat opacity = 0.5;

// The front view
UIImageView* frontImageView =
    [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"landscape"]];
frontImageView.frame = CGRectMake (0, 0, dimension, dimension);
frontImageView.contentMode = UIViewContentModeScaleAspectFill;
frontImageView.layer.cornerRadius = radius;
frontImageView.layer.masksToBounds = YES;

// The bakc view with shadow
UIView* backView = [[UIView alloc] initWithFrame:CGRectMake (100, 100, dimension, dimension)];
backView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
backView.layer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:CGRectMake (0, 0, dimension, dimension)
                                                        cornerRadius:radius] CGPath];
backView.layer.shadowOffset = CGSizeMake (offset, offset);
backView.layer.shadowOpacity = opacity;
backView.layer.shadowRadius = 2;

[backView addSubview:frontImageView];
[self.view addSubview:backView];

// The front view
UILabel* frontLabel = [[UILabel alloc] initWithFrame:CGRectMake (0, 0, dimension, dimension)];
frontLabel.textAlignment = NSTextAlignmentCenter;
frontLabel.text = @"This is good.";
frontLabel.textColor = [UIColor whiteColor];
frontLabel.backgroundColor = [UIColor colorWithRed:0.949 green:0.322 blue:0.318 alpha:1.00];
frontLabel.layer.cornerRadius = radius;
frontLabel.layer.masksToBounds = YES;

// Another back view with shadow and corner radius
UIView* anotherBackView = [[UIView alloc] initWithFrame:CGRectMake (100, 350, dimension, dimension)];
anotherBackView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
anotherBackView.layer.shadowPath =
    [[UIBezierPath bezierPathWithRoundedRect:CGRectMake (0, 0, dimension, dimension)
                                cornerRadius:radius] CGPath];
anotherBackView.layer.shadowOffset = CGSizeMake (offset, offset);
anotherBackView.layer.shadowOpacity = opacity;
anotherBackView.layer.shadowRadius = 2;

[anotherBackView addSubview:frontLabel];
[self.view addSubview:anotherBackView];
```