---
title: 反码
tags:
  - development
  - binary
  - computer-science
category: development
date: 2016-09-19 11:51:24
---

维基百科链接：[Ones' complement - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Ones%27_complement)
反码也叫一补数、Ones' complement，是计算机系统用来表示有符号数的一种方式。