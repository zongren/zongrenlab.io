---
title: Computer Networking读书笔记（1）
category: reading
tags:
  - book
  - network
  - computer
date: 2016-09-19 16:58:13
---
## 打卡日期
2016-09-19，以下内容为`Computer Networking`的读书笔记之第`1`篇。

## 书籍链接
附上书籍链接：[Computer Networking](https://book.douban.com/subject/10573157/)

## 书籍章节
第1.3.1章，22页-26页

## 读书笔记

### 分组交换（Packet Switching）
信息在传递时，会被拆分成`数据包`（`packet`），然后穿过通信链接和分组交换机（例如路由器，链路层交换机等），从一端到另一端。数据包的传输速率为线路的最大传输速率。

### 存储转发机制（Store-and-Forward Transmission）
分组交换机只有接受了一个数据包全部的数据，才会把这个数据包放到出站链接上。假设从A端到B端有N条链接和N-1个交换机，所有链接的传输速率都为R bit/sec，现在传输P个数据包，数据包的大小全部为L bit，那么总的传输时间为：
![computer-networking-one](/images/computer-networking-one.png)
通常情况下，一个分组交换机包括了多条链接，每条链接都有一个输出`缓冲区`（`output buffer`），或者叫做输出`队列`（`output queue`），如果一条链接已经在传输数据包，另外一个数据包就必须在缓冲区等待，这就造成了`队列延迟`（`queuing delays`），延迟的程度取决于线路拥堵情况。由于缓冲区的大小是有限的，势必会造成`丢包`（`packet loss`）现象，既有可能是丢弃即将进入缓冲区的数据包，也有可能丢弃正在排队的数据包。

### 转发表和路由协议（Forwarding Tables and Routing Protocols）
因为路由器通常有很多线路，所以路由器需要一种方法来决定传入的数据包传输到哪条线路上。每个终端都有一个`IP`地址，当一个终端向目标终端发送数据的时候，数据包的头部就包含了目标终端的IP地址，并且是层级结构的地址。每个路由器都有一个`转发表`（`forwarding table`），转发表的内容是IP地址和线路的对应关系。那么问题来了，怎么设置转发表呢，答案就是`路由协议`（`routing protocol`），路由协议能够决定端到端的最短线路，并据此设置路由器的转发表。