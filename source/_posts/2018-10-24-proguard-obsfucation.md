---
title: ProGuard混淆
category: android
tags: [security]
excerpt: proguard-obsfucation
date: 2018-10-24 13:55:49
---

我们知道通过在gradle中配置minifyenable，即可开启ProGuard混淆，本篇只讨论几个特殊配制

## 我不想删除“没有用到的”类

有时候ProGuard判断某些类没有用，实际上代码里通过ClassLoader和类名用到了这个类，例如
```
public interface ClassWithName {
  String getClassName();
}

public class ClassB implements ClassWithName{
  public String filedB = "class b";

  public String getClassName() {
    return filedB;
  }
}

Class<ClassWithName> classOfClassB = (Class<ClassWithName>) getClassLoader().loadClass("me.zongren.proguarddemo.ClassB");
ClassWithName classB = classOfClassB.newInstance();
System.out.println("class b name is " + classB.getClassName());
```
如果想保留这种类，一般是通过在proguard-rules.pro中使用以下配置
```
--keep class me.zongren.proguarddemo.ClassB
# 或者
-dontobfuscate
```

## 我只想混淆指定的类

在proguard-rules.pro中使用以下配置
```
-keep class !me.zongren.**,!me.zongren.**{*;}
```