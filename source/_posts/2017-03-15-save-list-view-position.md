---
title: 保存ListView滚动位置
category: android
tags:
  - code
  - tip
  - fragment
excerpt: 保存ListView滚动位置
date: 2017-03-15 10:12:31
---
来自StackOverflow上的解决方法
```
// save index and top position
int index = mList.getFirstVisiblePosition();
View item = mList.getChildAt(0);
int top = (item == null) ? 0 : (item.getTop() - mList.getPaddingTop());

// restore index and top position
mList.setSelectionFromTop(index, top);

```
