---
title: VPS日志
category: miscellaneous
tags: [vps,journal]
date: 2016-11-26 14:58:17
excerpt: 记录购买主机后执行的步骤
---

心血来潮购买了一台独立IP的主机，准备用来折腾，记录一下购买后进行的操作

## 首先需要选择操作系统
对于没有预先安装操作系统的主机，需要先安装操作系统，安装的方式由主机提供商决定，我安装了`Arch Linux`

## 创建普通用户
创建用户分为以下几步

### 创建用户
```
useradd –d /usr/zongren -m zongren
```

### 更改用户密码
```
passwd zongren
New password: 
Retype new password: 
passwd: password updated successfully
```

### 添加root权限（可选）
Arch Linux使用`visudo`命令添加权限
```
visudo
```
然后在`root ALL=(ALL) ALL`之后添加
```
zongren ALL=(ALL) ALL
```
`:q`保存退出

### 更改ssh配置
新添加的用户还不能通过ssh登录，需要修改ssh相关配置
```
vi /etc/ssh/sshd_config
```
添加以下代码
```
AllowUsers root zongren
```

### 注销并使用新用户登录
```
logout
```