---
title: 设置GridView元素高度
date: 2016-05-21 09:00:59
category: android
tags: [gridview,item-height]
---
有时候需要设置gridview item的高度，参考以下代码
```
@Override
 public View getView(final int position, View convertView, ViewGroup container) {
     View view = super.getView(position,convertView,container);

     ViewGroup.LayoutParams params = view.getLayoutParams();
     params.height = Utils.getPixelsFromDPs(mContext,200);
     view.setLayoutParams(params);
     return view;
}
 public static int getPixelsFromDPs(Context context, int dps){
        Resources r = context.getResources();
        int  px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }
```

