---
title: Data Binding示例
category: android
tags: [data-binding]
excerpt: Data Binding示例
date: 2017-04-25 15:42:30
---
### 配置build.gradle文件
```
dataBinding{
    enable = true
}
```
根据文档中所说的
{% blockquote %}
If you have an app module that depends on a library which uses data binding, your app module must configure data binding in its build.gradle file as well.
{% endblockquote %}
如果app模块依赖了使用`Data Binding`的库文件，同样需要配置`build.gradle`文件

### 创建对象
例如创建一个Text对象，包括2个字符串
```
package me.zongren.databindingdemo;

/**
 * Created by zongren on 2017/4/26.
 */

public class Text {
    public String text1;
    public String text2;

    public Text(String text1, String text2) {
        this.text1 = text1;
        this.text2 = text2;
    }
}

```

也可以使用getter，例如
```
public class Text {
    private String text1;
    private String text2;

    public String getText1(){
        return this.text1;
    }

    public String getText2(){
        return this.text2;
    }
}
```
或者
```
public class Text {
    private String text1;
    private String text2;

    public String text1(){
        return this.text1;
    }

    public String text2(){
        return this.text2;
    }
}
```
### 创建布局文件
和普通布局文件不同，它的根元素是`layout`
```
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android">

    <data>

        <variable
            name="text"
            type="me.zongren.databindingdemo.Text" />
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">

        <TextView

            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@{text.text1}" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@{text.text2}" />

        <android.support.v7.widget.RecyclerView
            android:id="@+id/recyclerView"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:layout_weight="1" />
    </LinearLayout>
</layout>

```

### Data Binding
在`onCreate`方法中实现绑定过程，生成的绑定工具类的命名规则是布局文件名称的PascalCase写法，加上Binding后缀，例如`activity_main.xml`生成的工具类为`ActivityMainBinding`
```
package me.zongren.databindingdemo;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.zongren.databindingdemo.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding =  DataBindingUtil.setContentView(this,R.layout.activity_main);
        Text text = new Text("text1 is poo","text2 is foo");
        binding.setText(text);
    }
}

```

### 在RecyclerView中使用Data Binding
流程和Activity绑定类似，不在赘述，具体可以查看下面的demo

### 绑定事件
处理事件有两种方式：`Method Reference`和`Listener Bindings`
根据官方文档描述，两者的主要区别在于
{% blockquote %}
The major difference between Method References and Listener Bindings is that the actual listener implementation is created when the data is bound, not when the event is triggered. If you prefer to evaluate the expression when the event happens, you should use listener binding.
{% endblockquote %}

也就是说Method Reference会在绑定数据（setHandler）时，创建好View的onClickListener，而`Listener Bindings`在事件发生时才会创建。

#### Method Reference
首先声明一个接口
```
package me.zongren.databindingdemo;

import android.view.View;

/**
 * Created by zongren on 2017/4/26.
 */

public interface ItemEventHandler {
    void clickTitle(View view);
    void clickCheckbox(View view);
}

```
在布局文件中使用这个接口
```
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    >

    <data>

        <variable
            name="item"
            type="me.zongren.databindingdemo.Item" />

        <variable
            name="handler"
            type="me.zongren.databindingdemo.ItemEventHandler"/>
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="40dp"
        android:orientation="horizontal">

        <TextView
            android:id="@+id/textView"
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="1"
            android:gravity="start|center_vertical"
            android:paddingLeft="10dp"
            android:paddingStart="10dp"
            android:text="@{item.title}"
            android:onClick="@{handler::clickTitle}"
            />

        <CheckBox
            android:id="@+id/checkbox"
            android:layout_width="40dp"
            android:layout_height="match_parent"
            android:checked="@{item.checked}"
            android:onClick="@{handler::clickCheckbox}"
            />
    </LinearLayout>
</layout>
```
最后绑定即可
```
@Override
public void onBindViewHolder(ItemViewHolder holder, int position) {
    ActivityMainItemBinding binding = holder.binding;
    binding.setItem(list.get(position));
    binding.setHandler(MainAdapter.this);
    binding.textView.setTag(R.id.tag_position,position);
    binding.checkbox.setTag(R.id.tag_position,position);
}
```

#### Listener Binding
Listener Bindings的优点是可以使用复杂的参数
例如接口改为下面这样
```
public interface ItemEventHandler {
    void clickTitle(View view,Item item);
}
```

布局文件改为下面这样
```
<TextView
    android:id="@+id/textView"
    android:layout_width="0dp"
    android:layout_height="match_parent"
    android:layout_weight="1"
    android:gravity="start|center_vertical"
    android:paddingLeft="10dp"
    android:paddingStart="10dp"
    android:text="@{item.title}"
    android:onClick="@{(view)->handler.clickTitle(view,item)}"
    />
```

### Demo
A simple [demo](https://github.com/zongren/DataBindingDemo) of data binding

### 官方文档
更多内容参考[官方文档](https://developer.android.com/topic/libraries/data-binding/index.html)
