---
title: 自动验证letsencrypt（二）
category: miscellaneous
tags: [gitlab-pages,letsencrypt]
excerpt: gitlab-letsencrypt插件已经支持git lab page domain api，可以全自动更新https 证书了，下面介绍一下使用方法。
date: 2018-04-05 19:22:56
---
## 更新
此插件经常报错，使用{% post_link letsencrypt-online-request %}代替

## 运行环境
```
操作系统: Windows 10 1709
node: 8.11.1 32位
python: 2.7.14
visual c++ build tools: 2015
gitlab letsencrypt: 3.1.1
```

## 操作步骤

### 安装node、python
```
省略
```

### 安装Visual C++ Build Tools 2015

前往[官网](https://www.visualstudio.com/zh-hans/vs/older-downloads/)下载安装工具
{% img /images/visual-cpp-build-tools-download-2015.png visual-cpp-build-tools-download-2015 %}

安装时注意勾选`Windows Phone 8.1 SDK`，不然等下会报错

### 下载libeay32.lib文件
从GitHub上的一个仓库下载[libeay32.lib](https://github.com/ReadyTalk/win32/blob/master/msvc/lib/libeay32.lib)文件，将其放置到`C:\OpenSSL-Win64\lib`目录下

### 安装gitlab letsencrypt
在`Visual C++ 2015 MSBuild Command Prompt`中执行以下命令
```
npm install -g gitlab-letsencrypt --msvs_version=2015
```
### 执行gitlab-le命令

```
gitlab-le --email {email} --domain {domain} --token {token}  --path /source/_static/.well-known/acme-challenge --production true --repository {your repository}
```
稍等片刻，就可以自动更新https证书

